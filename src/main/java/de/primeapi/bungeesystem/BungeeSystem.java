package de.primeapi.bungeesystem;

import de.primeapi.bungeesystem.bstats.Metrics;
import de.primeapi.bungeesystem.commands.*;
import de.primeapi.bungeesystem.license.AdvancedLicense;
import de.primeapi.bungeesystem.listeners.*;
import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.CommandManager;
import de.primeapi.bungeesystem.managers.ReportManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.managers.messages.MessageManager;
import de.primeapi.bungeesystem.mysql.*;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BungeeSystem extends Plugin {

    private static BungeeSystem instance;
    @Getter
    private static ThreadPoolExecutor threadPoolExecutor;
    public boolean clansystem;
    @Getter
    public static MessageManager messageManager;

    public static BungeeSystem getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {

        ProxyServer.getInstance().getConsole().sendMessage("§8§m----------[§r §6§lBungeeSystem §8§m]----------");
        ProxyServer.getInstance().getConsole().sendMessage("§7> Author:§e PrimeAPI");
        ProxyServer.getInstance().getConsole().sendMessage("§7> Version:§e " + getDescription().getVersion());
        ProxyServer.getInstance().getConsole().sendMessage("§7> Shop:§e primeshopmc.de");
        ProxyServer.getInstance().getConsole().sendMessage("§8§m----------[§r §6§lBungeeSystem §8§m]----------");

        instance = this;
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
        Conf.createConf();
        autoBC();
        onMins();
        try {
            messageManager = new MessageManager();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Data.pr = Conf.getSettingsString("Prefix");
        Data.wpr = Conf.getSettingsString("Prefix");
        Data.noperm = Message.NO_PERMS.getContent();

        Metrics metrics = new Metrics(this);

        metrics.addCustomChart(new Metrics.SingleLineChart("active_bans", new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return BanStats.getBannedUUIDs().size();
            }
        }));

        try {
            boolean license = new AdvancedLicense(Conf.getSettingsString("LizenzKey"), "http://primeapi.de/shop/licence/verify.php").register();
            Data.License = license;
        }catch (Exception exception){
            Data.License = false;
        }

        MySQL.connect();
        registerDatabases();
        OnlineStats.flush();

        regiserEvents();
        registerCommands();
        CommandManager.init();

        clansystem = ProxyServer.getInstance().getPluginManager().getPlugin("ClanSystem") != null;
        System.out.println("[BungeeSystem] " + (clansystem ? "Clan-System wurde gefunden" : "Clan-System wurde nicht gefunden!"));

    }

    @Override
    public void onDisable() {
        if(!Conf.getSettingsBoolean("report.keepReportsOnRestart")){
            for(ReportManager.Report report : ReportStats.getReports()){
                ReportStats.removeReport(report.getReported(), report.getReporter());
            }
        }
        OnlineStats.flush();
        MySQL.disconnect();
    }

    public void regiserEvents() {
        PluginManager pm = ProxyServer.getInstance().getPluginManager();
        pm.registerListener(this, new JoinListener());
        pm.registerListener(this, new ChatListener());
        pm.registerListener(this, new PostLoginListener());
        if (Conf.getSettingsBoolean("toggle.motd")){ pm.registerListener(this, new PingListener()); }
        pm.registerListener(this, new QuitListener());
        pm.registerListener(this, new ServerSwitchListener());
    }

    public void registerCommands() {
        PluginManager pm = ProxyServer.getInstance().getPluginManager();
        pm.registerCommand(this, new ConnectCommand("connect"));
        pm.registerCommand(this, new BungeeSystemCommand("bungeesystem"));
        pm.registerCommand(this, new BungeeSystemCommand("bungeesys"));

    }

    public void registerDatabases() {
        MySQL.Update("CREATE TABLE IF NOT EXISTS Users(name varchar(100),uuid varchar(100),ip varchar(100),banned varchar(100),lang varchar(100), reason varchar(100), admin varchar(100), onmins varchar(100), coins varchar(100), FirstJoin varchar(100), LastJoin varchar(100), ipban varchar(100))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS History(uuid varchar(100),reason varchar(200),admin varchar(200),longs varchar(200),times varchar(200),type varchar(200))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Mutes(uuid varchar(100),muted varchar(200),admin varchar(200),lang varchar(200),reason varchar(200))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Warns(uuid varchar(100),reason varchar(200),admin varchar(200),longs varchar(200))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Notify(uuid varchar(100),notify varchar(200),tc varchar(200),report varchar(200))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Reports(reported varchar(100),reason varchar(200),reporter varchar(200),timestamp varchar(200))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Info(infotype varchar(100),info1 varchar(200),info2 varchar(200),info3 varchar(200))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Friends(uuid varchar(100),uuidfriend varchar(100),uuidanfrage varchar(100),status varchar(100))");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Toggle(uuid varchar(100),friends int,msg int,online int,jump int)");
        MySQL.Update("CREATE TABLE IF NOT EXISTS Online(uuid varchar(100),server varchar(100),afk int,party varchar(100),frozen boolean)");
        try {
            MySQL.c.prepareStatement("CREATE TABLE IF NOT EXISTS `auth`(uuid varchar(100),active boolean,secret varchar(255))").execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    private void onMins() {
        ProxyServer.getInstance().getScheduler().schedule(this, () -> {

            for (ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()) {
                if(!OnlineStats.getAFK(pp.getUniqueId())) {
                    int i = Integer.valueOf(PlayerStats.getOnMins(pp.getUniqueId().toString()));
                    i++;
                    PlayerStats.setOnMins(pp.getUniqueId().toString(), String.valueOf(i));
                }
            }


        }, 1, 1, TimeUnit.MINUTES);
    }


    private void autoBC() {
        if(!Conf.getSettingsBoolean("toggle.autobc")) return;
        ProxyServer.getInstance().getScheduler().schedule(this, () -> {

            List<String> list = Conf.getSettingsStringList("autobc.list");

            Random rdm = new Random();
            int i = rdm.nextInt((list.size() - 1));

            for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                pp.getPp().sendMessage(list.get(i));
            }

        }, Conf.getSettingsInteger("autobc.timeInMinutes"), Conf.getSettingsInteger("autobc.timeInMinutes"), TimeUnit.MINUTES);
    }


}
