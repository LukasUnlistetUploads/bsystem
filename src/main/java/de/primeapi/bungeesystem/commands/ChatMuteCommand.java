package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ChatMuteCommand extends Command {

    public ChatMuteCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.chatmute")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sendMessage(Message.CHATMUTE_USAGE);
            return;
        }


        if (args[0].equalsIgnoreCase("local") || args[0].equalsIgnoreCase("l")) {
            if (!Data.chatmutedServers.contains(p.getServerInfo())) {
                for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers(p.getPp().getServer().getInfo())) {
                    all.sendMessage(Message.CHATMUTE_SUCESS_LOCAL_DEACTIVATE);
                }
                Data.chatmutedServers.add(p.getServerInfo());
            } else {
                for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers(p.getPp().getServer().getInfo())) {
                    all.sendMessage(Message.CHATMUTE_SUCESS_LOCAL_ACTIVATE);
                }
                Data.chatmutedServers.remove(p.getServerInfo());
            }
        } else if (args[0].equalsIgnoreCase("global") || args[0].equalsIgnoreCase("g")) {
            if (!Data.globalchatmute) {
                for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
                    all.sendMessage(Message.CHATMUTE_SUCESS_GLOBAL_DEACTIVATE);
                }
                Data.globalchatmute = true;
            } else {
                for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
                    all.sendMessage(Message.CHATMUTE_SUCESS_GLOBAL_ACTIVATE);
                }
                Data.globalchatmute = false;
            }
        }

    }

}
