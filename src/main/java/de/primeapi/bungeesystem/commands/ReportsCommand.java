package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.ReportManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.ReportStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.List;

public class ReportsCommand extends Command {

    public ReportsCommand() {
        super("reports");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.reports")) {
            p.sendNoPerm();
        }

        if (!NotifyStats.isReport(p.getUniqeID())) {
            TextComponent login = new TextComponent(" §8[§aLogin§8]");
            login.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cLogge dich in das Report-System ein!").create()));
            login.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/login in Report "));
            p.getPp().sendMessage(new TextComponent(Message.REPORTS_LOGIN.getContent()), login);
        }

        if (args.length == 0) {
            if (ReportStats.getReports().size() == 0) {
                p.sendMessage(Message.REPORTS_NOREPORTS);
            } else {
                p.sendMessage(Message.REPORTS_LIST_CAPTION);
                for (ReportManager.Report report : ReportStats.getReports()) {
                    TextComponent tp = new TextComponent(" §8[§aAccept§8]");
                    tp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cTeleportiere dich zu diesem Spieler").create()));
                    tp.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reports accept " + report.getReported()));

                    p.getPp().sendMessage(new TextComponent(Message.REPORTS_LIST_CONTENT.replace("player", report.getReported()).replace("reason", report.getReason()).replace("reporter", report.getReporter()).replace("time", TimeUtils.getDate(report.getTime().toString())).getContent()), tp);


                }
            }
        } else {
            if (args[0].equalsIgnoreCase("accept")) {
                if (PrimeAPI.isOnline(args[1])) {
                    PrimePlayer t = PrimeAPI.getPrimePlayer(args[1]);
                    List<ReportManager.Report> reports = ReportStats.getReportsByReported(t.getName());
                    if (reports.size() >= 1) {


                        p.getPp().connect(t.getServerInfo());

                        p.sendMessage(Message.REPORTS_ACCEPT_SUCCESS);
                        p.sendMessage(Message.REPORTS_ACCEPT_COUNT.replace("count", String.valueOf(reports.size())));
                        p.sendMessage(Message.REPORTS_ACCEPT_LIST_CAPTION);
                        for(ReportManager.Report report : reports){
                            p.sendMessage(Message.REPORTS_ACCEPT_LIST_CONTENT.replace("reason", report.getReason()).replace("reporter", report.getReporter()));
                            ReportStats.removeReport(report.getReported(), report.getReporter());
                        }


                    } else {
                        p.sendMessage(Message.REPORTS_NOTREPORTED);
                    }
                }else {
                    p.sendMessage(Message.REPORTS_OFFLINE);
                }
            }
        }


    }
}
