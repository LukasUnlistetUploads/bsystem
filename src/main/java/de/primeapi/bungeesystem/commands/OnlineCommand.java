package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class OnlineCommand extends Command {
    public OnlineCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);
        int team = 0;
        int on = PrimeAPI.getOnlinePrimePlayers().size();
        int onServer = 0;
        for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
            if (pp.hasPermission("bungee.team")) {
                team++;
            }
            if (pp.getServerInfo().equals(p.getServerInfo())) {
                onServer++;
            }
        }

        p.sendMessage(Message.ONLINE_ALL.replace("count", String.valueOf(on)));
        p.sendMessage(Message.ONLINE_CURRENT.replace("count", String.valueOf(team)));
        p.sendMessage(Message.ONLINE_TEAM.replace("count", String.valueOf(onServer)));
    }
}
