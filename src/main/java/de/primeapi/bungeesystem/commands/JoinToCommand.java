package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class JoinToCommand extends Command {

    public JoinToCommand(String name) {
        super("jointo");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.jointo")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sendMessage(Message.JOINTO_USAGE);
            return;
        }

        if (!PrimeAPI.isOnline(args[0])) {
            p.sendMessage(Message.JOINTO_NOTFOUND);
            return;
        }

        PrimePlayer t = PrimeAPI.getPrimePlayer(args[0]);
        if (t.getPp().getServer().getInfo().equals(p.getPp().getServer().getInfo())) {
            p.sendMessage(Message.JOINTO_ALREADY.replace("player", t.getName()));

            return;
        }

        p.getPp().connect(t.getPp().getServer().getInfo());
        p.sendMessage(Message.JOINTO_SUCCESS);

    }

}
