package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class LoginCommand extends Command {

    public LoginCommand() {
        super("Login");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = new PrimePlayer((ProxiedPlayer) sender);

        if (!(p.hasPermission("bungee.team"))) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            if (NotifyStats.isReport(p.getUniqeID())) {
                p.sendMessage(Message.LOGIN_REPORT_ON);
            } else {
                p.sendMessage(Message.LOGIN_REPORT_OFF);
            }
            if (NotifyStats.isTC(p.getUniqeID())) {
                p.sendMessage(Message.LOGIN_TEAMCHAT_ON);
            } else {
                p.sendMessage(Message.LOGIN_TEAMCHAT_OFF);
            }
            if (NotifyStats.isNotify(p.getUniqeID())) {
                p.sendMessage(Message.LOGIN_NOTIFY_ON);
            } else {
                p.sendMessage(Message.LOGIN_NOTIFY_OFF);
            }
            p.sendMessage(Message.LOGIN_USAGE);
            return;
        }

        if(args.length != 2){
            p.sendMessage(Message.LOGIN_USAGE);
            return;
        }

        String uuid = p.getUniqeID();

        if (args[0].equalsIgnoreCase("in")) {
            if (args[1].equalsIgnoreCase("all")) {
                NotifyStats.setNotify(uuid, true);
                NotifyStats.setReport(uuid, true);
                NotifyStats.setTC(uuid, true);
                p.sendMessage(Message.LOGIN_LOGIN_ALL_SUCCESS);
                for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                    if (NotifyStats.isReport(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGIN_REPORT_NOTIFY.replace("player", p.getName()));
                    }
                    if (NotifyStats.isTC(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGIN_TEAMCHATT_NOTIFY.replace("player", p.getName()));
                    }
                }
            } else if (args[1].equalsIgnoreCase("tc")) {
                NotifyStats.setTC(uuid, true);
                p.sendMessage(Message.LOGIN_LOGIN_TEAMCHAT_SUCCESS);
                for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                    if (NotifyStats.isTC(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGIN_REPORT_NOTIFY.replace("player", p.getName()));
                    }
                }
            } else if (args[1].equalsIgnoreCase("notify")) {
                NotifyStats.setNotify(uuid, true);
                p.sendMessage(Message.LOGIN_LOGIN_NOTIFY_SUCCESS);
            } else if (args[1].equalsIgnoreCase("report")) {
                NotifyStats.setReport(uuid, true);
                p.sendMessage(Message.LOGIN_LOGIN_REPORT_SUCCESS);
                for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                    if (NotifyStats.isReport(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGIN_REPORT_NOTIFY.replace("player", p.getName()));
                    }
                }
            } else {
                p.sendMessage(Message.LOGIN_USAGE);
            }

        } else if (args[0].equalsIgnoreCase("out")) {
            if (args[1].equalsIgnoreCase("all")) {
                NotifyStats.setNotify(uuid, false);
                NotifyStats.setReport(uuid, false);
                NotifyStats.setTC(uuid, false);
                p.sendMessage(Message.LOGIN_LOGOUT_ALL_SUCCESS);
                for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                    if (NotifyStats.isReport(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGOUT_REPORT_NOTIFY.replace("player", p.getName()));
                    }
                    if (NotifyStats.isTC(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGOUT_TEAMCHATT_NOTIFY.replace("player", p.getName()));
                    }
                }
            } else if (args[1].equalsIgnoreCase("tc")) {
                NotifyStats.setTC(uuid, false);
                p.sendMessage(Message.LOGIN_LOGOUT_ALL_SUCCESS);
                for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                    if (NotifyStats.isTC(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGOUT_TEAMCHATT_NOTIFY.replace("player", p.getName()));
                    }
                }
            } else if (args[1].equalsIgnoreCase("notify")) {
                NotifyStats.setNotify(uuid, false);
                p.sendMessage(Message.LOGIN_LOGOUT_NOTIFY_SUCCESS);
            } else if (args[1].equalsIgnoreCase("report")) {
                NotifyStats.setReport(uuid, false);
                p.sendMessage(Message.LOGIN_LOGOUT_REPORT_SUCCESS);
                for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                    if (NotifyStats.isReport(pp.getUniqeID())) {
                        p.sendMessage(Message.LOGIN_LOGOUT_REPORT_NOTIFY.replace("player", p.getName()));
                    }
                }
            } else {
                p.sendMessage(Message.LOGIN_USAGE);
            }
        } else {
            p.sendMessage(Message.LOGIN_USAGE);
            return;
        }

    }


}
