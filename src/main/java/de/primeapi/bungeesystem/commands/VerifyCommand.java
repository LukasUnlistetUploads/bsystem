package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.AuthManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.AuthStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class VerifyCommand extends Command {
    public VerifyCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(!sender.hasPermission(Conf.getSettingsStringRaw("settings.auth.permission"))){
            sender.sendMessage(Message.NO_PERMS.getContent());
            return;
        }

        if(args.length == 0){
            sender.sendMessage(Message.VERIFY_USAGE1.getContent());
            sender.sendMessage(Message.VERIFY_USAGE2.getContent());
            return;
        }

        if(args[0].equalsIgnoreCase("reset")){
            if(args.length == 1){
                if(!(sender instanceof ProxiedPlayer)){
                    sender.sendMessage("This command can only be used by an Player!");
                    return;
                }
                AuthStats.setActive(((ProxiedPlayer) sender).getUniqueId(), false);
                AuthManager.activatePlayer(PrimeAPI.getPrimePlayer(sender));
            }else {
                if (!PlayerStats.exist(args[1])) {
                    sender.sendMessage(Message.VERIFY_NOTFOUND.getContent());
                    return;
                }

                UUID uuid = UUID.fromString(PlayerStats.getUUID(args[1]));
                AuthStats.setActive(uuid, false);
                sender.sendMessage(Message.VERIFY_SUCCESS.getContent());
            }

        }
    }
}
