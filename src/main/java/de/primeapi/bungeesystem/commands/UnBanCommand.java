package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.LogStats;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class UnBanCommand extends Command {

    public UnBanCommand() {
        super("unban");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            if (args.length < 2) {
                System.out.println(Data.pr + "§7 Benutze: §e/unban [SpielerName] [Grund...]");
                return;
            }

            if (!PlayerStats.exist(args[0])) {
                System.out.println(Data.pr + "§c Dieser Spieler war noch nie auf dem Netzwerk");
                return;
            }

            String uuid = PlayerStats.getUUID(args[0]);

            if (!BanStats.isBanned(uuid)) {
                System.out.println(Data.pr + "§7 Dieser Spieler ist nicht gebannt!");
                return;
            }
            String reason = "";
            for (int argCo = 1; argCo <= args.length - 1; argCo++) {
                reason += args[argCo] + " ";
            }

            BanStats.setBanned(uuid, false);
            LogStats.addLogReason(uuid, reason, "Console", "0", String.valueOf(System.currentTimeMillis()), "Unban");

            System.out.println(Data.pr + "§7 Du hast erfolgreich §e" + PlayerStats.getName(uuid) + "§a entbannt!");


            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);


        if (!p.hasPermission("bungee.unban")) {
            p.sendNoPerm();
            return;
        }

        if (args.length < 2) {
            p.sendMessage(Message.UNBAN_USAGE);
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(Message.UNBAN_NOTFOUND);
            return;
        }

        String UUID = PlayerStats.getUUID(args[0]);

        if (!BanStats.isBanned(UUID)) {
            p.sendMessage(Message.UNBAN_NOTBANNED);
            return;
        }
        String reason = "";
        for (int argCo = 1; argCo <= args.length - 1; argCo++) {
            reason += args[argCo] + " ";
        }


        BanStats.setBanned(UUID, false);
        LogStats.addLogReason(UUID, reason, p.getUUID().toString(), "0", String.valueOf(System.currentTimeMillis()), "Unban");

        p.sendMessage(Message.UNBAN_SUCCESS.replace("player", PlayerStats.getName(UUID)));

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungee.notify")) {
                if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                    all.sendMessage(Message.UNBAN_NOTIFY.replace("player", PlayerStats.getName(UUID)).replace("admin", p.getName()).getContent());
                }
            }
        }


    }
}