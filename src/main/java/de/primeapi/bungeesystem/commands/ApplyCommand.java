package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ApplyCommand extends Command {

    public ApplyCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (args.length == 0) {
            for (String msg : Conf.getSettingsStringList("ApplyMSG.overview")) {
                p.getProxiedPlayer().sendMessage(msg.replace("&", "§").replace("[>]", "➤"));
            }
        } else {
            if (args[0].equalsIgnoreCase("Developer")) {
                for (String msg : Conf.getSettingsStringList("ApplyMSG.dev")) {
                    p.getProxiedPlayer().sendMessage(msg.replace("&", "§").replace("[>]", "➤"));
                }
            } else if (args[0].equalsIgnoreCase("Builder")) {
                for (String msg : Conf.getSettingsStringList("ApplyMSG.build")) {
                    p.getProxiedPlayer().sendMessage(msg.replace("&", "§").replace("[>]", "➤"));
                }
            } else if (args[0].equalsIgnoreCase("Supporter")) {
                for (String msg : Conf.getSettingsStringList("ApplyMSG.sup")) {
                    p.getProxiedPlayer().sendMessage(msg.replace("&", "§").replace("[>]", "➤"));
                }
            } else {
                for (String msg : Conf.getSettingsStringList("ApplyMSG.overview")) {
                    p.getProxiedPlayer().sendMessage(msg.replace("&", "§").replace("[>]", "➤"));
                }
            }
        }

    }
}
