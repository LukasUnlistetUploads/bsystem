package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class OnTimeCommand extends Command {
    public OnTimeCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        PrimePlayer p = PrimeAPI.getPrimePlayer(commandSender);

        p.sM(Message.ONTIME.replace("count", TimeUtils.MinutesToString(Long.valueOf(PlayerStats.getOnMins(p.getUniqeID())))));
    }
}
