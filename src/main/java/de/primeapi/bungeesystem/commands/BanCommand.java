package de.primeapi.bungeesystem.commands;

import com.sun.org.apache.bcel.internal.generic.NOP;
import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.*;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BanCommand extends Command implements TabExecutor {

    String pr = Data.pr;

    public BanCommand() {
        super("ban");
    }

    @SuppressWarnings("deprecation")
    public void usage(PrimePlayer p) {
        p.sendMessage(Message.BAN_USAGE1);

        for (String s : Conf.getBanList("reasons")) {
            if (Conf.getBanString("Reason." + s + ".Type").equalsIgnoreCase("Ban")) {
                p.getProxiedPlayer().sendMessage("§8× §a" + Conf.getBanString("Reason." + s + ".Reason") + "§7 - §d" + s + " §7| §4NETWORK-BAN");
            } else {
                p.getProxiedPlayer().sendMessage("§8× §a" + Conf.getBanString("Reason." + s + ".Reason") + "§7 - §d" + s + " §7| §cCHAT-MUTE");
            }
        }

        p.sendMessage(Message.BAN_USAGE2);
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            System.out.println("nope");
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);


        if (!p.hasPermission("Bungee.ban")) {
            p.sendMessage(Message.NO_PERMS);
            return;
        }
        if (args.length < 2) {
            usage(p);
            return;
        }
        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(Message.BAN_NOTFOUND);
            return;
        }

        String uuid = PlayerStats.getUUID(args[0]);

        String type;
        String ltype;
        int l;
        String reason;

        if (Conf.getBanString("Reason." + args[1] + ".reason") == null) {
            usage(p);
            return;
        }

        String s = args[1];
        if (!p.hasPermission("Bungee.ban." + s)) {
            p.sendMessage(Message.BAN_NOPERMS);
            return;
        }

        type = Conf.getBanString("Reason." + s + ".Type");
        reason = Conf.getBanString("Reason." + s + ".Reason");
        boolean before = false;
        for (String reasons : LogStats.getBanReasons(uuid)) {
            if (reasons.contains(reason)) {
                before = true;
            }
        }

        for (String reasons : LogStats.getMuteReasons(uuid)) {
            if (reasons.contains(reason)) {
                before = true;
            }
        }

        if (!before) {
            ltype = Conf.getBanString("Reason." + s + ".FirstBan.s");
            l = Conf.getBanInt("Reason." + s + ".FirstBan.int");
        } else {
            ltype = Conf.getBanString("Reason." + s + ".SecondBan.s");
            l = Conf.getBanInt("Reason." + s + ".SecondBan.int");
        }


        String msg = " | ";

        for (int argCo = 2; argCo <= args.length - 1; argCo++) {
            msg += args[argCo] + " ";
        }

        if (!msg.equalsIgnoreCase(" | ")) {
            reason = reason + msg;
        }


        long time = 0;
        String timename = "";
        if (ltype.equalsIgnoreCase("P")) {
            time = -1;
            timename = "Permanent";
        } else if (ltype.equalsIgnoreCase("d")) {
            time = TimeUtils.toMillies(l, 0, 0);
            timename = l + " Tage";
        } else if (ltype.equalsIgnoreCase("h")) {
            time = TimeUtils.toMillies(0, l, 0);
            timename = l + " Stunden";
        } else if (ltype.equalsIgnoreCase("m")) {
            time = TimeUtils.toMillies(0, 0, l);
            timename = l + " Minuten";
        } else {
            p.getProxiedPlayer().sendMessage(Data.pr + "§c Fehler: 1");
            return;
        }
        long end = 0;
        if (time != -1) {
            end = System.currentTimeMillis() + time;
        } else {
            end = -1;
        }


        if (type.equalsIgnoreCase("Ban")) {

            BanStats.addBan(uuid, reason, p.getUniqeID(), end);
            LogStats.addLogReason(uuid, reason, p.getUniqeID(), timename, String.valueOf(System.currentTimeMillis()), "Ban");


            if (ProxyServer.getInstance().getPlayer(args[0]) != null) {
                ProxyServer.getInstance().getPlayer(args[0]).disconnect(Conf.getSettingsString("messages.ban")
                        .replaceAll("%reason%", reason)
                        .replaceAll("%time%", timename));
            }

            p.sendMessage(Message.BAN_BAN_SUCCESS.replace("%player%", PlayerStats.getName(uuid)).replace("%reason%", reason));


            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                if (all.hasPermission("bungee.notify")) {
                    if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                        all.sendMessage(Message.BAN_BAN_SUCCESS_NOTIFY.replace("%admin%", p.getName()).replace("%player%", PlayerStats.getName(uuid)).replace("%reason%", reason).getContent());
                    }
                }
            }


        } else {
            MuteStats.addMute(uuid, reason, p.getUUID().toString(), end);
            LogStats.addLogReason(uuid, reason, p.getUUID().toString(), timename, String.valueOf(System.currentTimeMillis()), "Mute");
            p.sendMessage(Message.BAN_MUTE_SUCCESS.replace("%player%", PlayerStats.getName(uuid)).replace("%reason%", reason));

            if (ProxyServer.getInstance().getPlayer(args[0]) != null) {
                ProxiedPlayer t = ProxyServer.getInstance().getPlayer(args[0]);
                t.sendMessage(Message.BAN_MUTE_SUCCESS_NOTIFYPLAYER.replace("%time%", timename).replace("%reason%", reason).getContent());
            }
        }


    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        List<String> set = new ArrayList<>();

        if(args.length == 1){
            for(ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()){
                if(pp.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                    set.add(pp.getName());
                }
            }
        }

        if(args.length == 2){
            for(String s : Conf.getBanList("reasons")){
                if(s.toLowerCase().startsWith(args[1].toLowerCase())){
                    set.add(s);
                }
            }
        }



        return set;
    }
}
