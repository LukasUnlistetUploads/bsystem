package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SlotsCommand extends Command {
    public SlotsCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.slots")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sendMessage(Message.SLOTS_COUNT);
            p.sendMessage(Message.SLOTS_USAGE);
            return;
        }

        int i = 0;
        try {
            i = Integer.valueOf(args[0]);
        } catch (Exception ex) {
            p.sendMessage(Message.SLOTS_NONUMBER.replace("number", args[0]));
            return;
        }


        Conf.setSettingsInt("Slots", i);
        Data.slots = i;

        p.sendMessage(Message.SLOTS_SUCCESS);
        Methods.updateTabList();
    }
}
