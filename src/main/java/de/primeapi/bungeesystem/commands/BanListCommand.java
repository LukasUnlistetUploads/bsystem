package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BanListCommand extends Command {

    public BanListCommand() {
        super("BanList");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = new PrimePlayer((ProxiedPlayer) sender);

        if (!p.hasPermission("ban.banlist")) {
            p.sendNoPerm();
            return;
        }


        if (BanStats.getBannedUUIDs().size() == 0) {
            p.sendMessage(Message.BANLIST_NOBANS);
            return;
        }

        String s = "";

        for (String uuid : BanStats.getBannedUUIDs()) {
            if (BanStats.getTime(uuid).equalsIgnoreCase("-1")) {
                s = s + " §4" + PlayerStats.getName(uuid) + "§7,";
            } else {
                s = s + " §c" + PlayerStats.getName(uuid) + "§7,";
            }
        }
        p.sendMessage(Message.BANLIST_MESSAGE);
        p.getProxiedPlayer().sendMessage(" " + s);


    }


}
