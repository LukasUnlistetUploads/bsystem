package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class WhereIsCommand extends Command {

    public WhereIsCommand(String name) {
        super("whereis");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.whereis")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sM(Message.WHEREIS_USAGE);
            return;
        }

        if (!PrimeAPI.isOnline(args[0])) {
            p.sM(Message.WHEREIS_NOTFOUND);
            return;
        }

        PrimePlayer t = PrimeAPI.getPrimePlayer(args[0]);
        p.sendMessage(Message.WHEREIS_MESSAGE.replace("server", p.getPp().getServer().getInfo().getName()).replace("player", t.getName()));

    }

}
