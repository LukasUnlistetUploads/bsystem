package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SetIpBanCommand extends Command {

    public SetIpBanCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage("Spieler Command");
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.setipban")) {
            p.sendNoPerm();
            return;
        }

        if (args.length != 2) {
            p.sendMessage(Message.SETIPBAN_USAGE);
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(Message.SETIPBAN_NOTFOUND);
            return;
        }

        String uuid = PlayerStats.getUUID(args[0]);

        if (!BanStats.isBanned(uuid)) {
            p.sendMessage(Message.SETIPBAN_NOTBANNED);
            return;
        }

        if (args[1].equalsIgnoreCase("Ja")) {
            BanStats.setIPBanned(uuid, true);
            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                if (all.hasPermission("bungee.notify")) {
                    if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                        all.sendMessage(Message.SETIPBAN_ON_NOTIFY.replace("admin", p.getName()).replace("player", PlayerStats.getName(uuid)).getContent());
                    }
                }
            }
            p.sendMessage(Message.SETIPBAN_ON_SUCCESS);
        } else if (args[1].equalsIgnoreCase("Nein")) {
            BanStats.setIPBanned(uuid, false);
            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                if (all.hasPermission("bungee.notify")) {
                    if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                        all.sendMessage(Message.SETIPBAN_OFF_NOTIFY.replace("admin", p.getName()).replace("player", PlayerStats.getName(uuid)).getContent());
                    }
                }
            }
            p.sendMessage(Message.SETIPBAN_OFF_SUCCESS);
        } else {
            p.sendMessage(Message.SETIPBAN_USAGE2);
        }

    }
}
