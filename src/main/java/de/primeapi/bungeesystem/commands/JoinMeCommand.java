package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class JoinMeCommand extends Command {
    public JoinMeCommand(String name) {
        super(name);
    }


    public static HashMap<UUID, Long> waiting = new HashMap<>();

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.joinme")) {
            p.sendNoPerm();
            return;
        }

        if(waiting.containsKey(p.getUUID())){
            Long l = waiting.get(p.getUUID());
            if(System.currentTimeMillis() < l){
                p.sendMessage(Message.JOINME_WAIT.replace("time", TimeUtils.getEnd(l)));
                return;
            }
        }

        if (p.getServerInfo().getName().startsWith("Lobby") || p.getServerInfo().getName().startsWith("build")) {
            p.sendMessage(Message.JOINME_LOBBY);
            return;
        }

        Methods.createJoinMe(p);
        waiting.put(p.getUUID(), System.currentTimeMillis() + TimeUtils.toMillies(0, 0, getPlayersWaitTime(p)));

    }


    public Integer getPlayersWaitTime(PrimePlayer p){
        List<String> list = Conf.getSettingsStringList("joinme.cooldown");
        int i = 0;
        for (String s : Objects.requireNonNull(list)) {
            String permission = s.split(":")[0];
            String mins = s.split(":")[1];

            if(p.hasPermission(permission)) return Integer.parseInt(mins);
            i++;

            if(i == list.size()){
                return Integer.parseInt(mins);
            }
        }

        return 0;
    }
}
