package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.ReportManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.ReportStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReportCommand extends Command implements TabExecutor {

    public ReportCommand() {
        super("report");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (args.length == 0 || args.length == 1) {
            sendUsage(p);
            return;
        }

        if (!PrimeAPI.isOnline(args[0])) {
            p.sendMessage(Message.REPORT_NOT_FOUND);
            return;
        }


        PrimePlayer t = PrimeAPI.getPrimePlayer(args[0]);

        if (t.getProxiedPlayer() == p.getProxiedPlayer()) {
            p.sendMessage(Message.REPORT_SELF);
            return;
        }

        String msg = null;
        if(Conf.getSettingsBoolean("reports.usetemplates")){
            for(String s : Conf.getSettingsStringList("reports.reasons")){
                if(args[1].toLowerCase().equalsIgnoreCase(s)){
                    msg = s;
                    break;
                }
            }
            if(Objects.isNull(msg)){
                sendUsage(p);
                return;
            }
        }else {
            msg = "";
            for (int i = 1; i < args.length; i++) {
                msg += args[i] + " ";
            }
        }


        if(ReportStats.hasReported(p.getName(), t.getName())){
            p.sendMessage(Message.REPORT_ALREADY);
            return;
        }

        for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
            if (NotifyStats.isReport(all.getUniqeID())) {
                TextComponent tp = new TextComponent(" §8[§aAccept§8]");
                tp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cTeleportiere dich zu diesem Spieler").create()));
                tp.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reports accept " + t.getName()));
                all.getPp().sendMessage(new TextComponent(Message.REPORT_NOTIFY.replace("player", t.getName()).replace("reason", msg).getContent()), tp);
            }
        }

        ReportStats.insertReport(new ReportManager.Report(p.getName(), t.getName(), msg));
        p.sendMessage(Message.REPORT_SUCCESS.replace("player", t.getName()).replace("reason", msg));


    }

    public void sendUsage(PrimePlayer p){
        if(Conf.getSettingsBoolean("reports.usetemplates")){
            p.sendMessage(Message.REPORT_USAGE);

            StringBuilder builder = new StringBuilder("");
            List<String> list = Conf.getSettingsStringList("reports.reasons");
            int i = 0;
            for(String s : list){
                i++;
                if(i == list.size()){
                    builder.append("§e").append(s);
                }else {
                    builder.append("§e").append(s).append("§7, ");
                }
            }

            p.sendMessage(Message.REPORT_REASONS.replace("reasons", builder.toString()));
        }else {
            p.sendMessage(Message.REPORT_USAGE);
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        List<String> list = new ArrayList<>();

        if(args.length == 1){
            for(ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()){
                if(pp.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                    list.add(pp.getName());
                }
            }
        }

        if(args.length == 2){
            for(String s : Conf.getSettingsStringList("reports.reasons")){
                if(s.toLowerCase().startsWith(args[1].toLowerCase())) list.add(s);
            }
        }

        return list;
    }
}
