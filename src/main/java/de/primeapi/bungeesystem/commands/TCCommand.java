package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TCCommand extends Command {

    public TCCommand() {
        super("TC");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = new PrimePlayer((ProxiedPlayer) sender);

        if (!p.hasPermission("bungee.tc")) {
            p.sendNoPerm();
            return;
        }
        if (!NotifyStats.isTC(p.getUniqeID())) {
            TextComponent login = new TextComponent(" §8[§aLogin§8]");
            login.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cLogge dich in das Report-System ein!").create()));
            login.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/login in Tc"));
            p.getPp().sendMessage(new TextComponent(Message.TC_LOGIN.getContent()), login);
        }
        if (args.length == 0) {
            p.sM(Message.TC_USAGE);
            return;
        }

        String msg = "";
        for (int argCo = 0; argCo <= args.length - 1; argCo++) {
            msg += args[argCo] + " ";
        }

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungee.tc")) {
                if (NotifyStats.isTC(all.getUniqueId().toString())) {
                    all.sendMessage(Message.TC_MESSAGE.replace("name", p.getName()).replace("server", p.getServerInfo().getName()).replace("message", msg).getContent());
                }
            }
        }


    }

}
