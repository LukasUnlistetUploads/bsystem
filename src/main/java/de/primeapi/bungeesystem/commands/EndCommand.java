package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.TimeUnit;

public class EndCommand extends Command {
    public EndCommand(String name) {
        super(name);
    }

    public void setMI() {
        Data.stoppingInt--;
    }

    public Integer getI() {
        return Data.stoppingInt;
    }

    public void setI(int i) {
        Data.stoppingInt = i;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission("bungee.end")) {
            sender.sendMessage(Data.noperm);
            return;
        }


        if (args.length != 1) {
            sender.sendMessage(Message.END_USAGE.getContent());
            return;
        }

        if (args[0].equalsIgnoreCase("now")) {
            for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                pp.getPp().disconnect(Message.END_KICKMESSAGE.getContent());
            }

            ProxyServer.getInstance().getScheduler().cancel(BungeeSystem.getInstance());
            ProxyServer.getInstance().stop("§c Starte Neu....");
            return;
        }

        if (args[0].equalsIgnoreCase("stop")) {
            sender.sendMessage(Message.END_ABORD.getContent());
            Data.stopping = false;
            return;
        }

        int i = 0;
        try {
            i = Integer.valueOf(args[0]);
        } catch (Exception ex) {
            sender.sendMessage(Message.END_USAGE.getContent());
            return;
        }

        setI(i);
        Data.stopping = true;
        ProxyServer.getInstance().broadcast(Message.END_COUNT.replace("count", String.valueOf(getI())).getContent());


        ProxyServer.getInstance().getScheduler().schedule(BungeeSystem.getInstance(), () -> {

            if (Data.stopping) {
                setMI();

                if (getI() == 60 || getI() == 30 || getI() == 15 || getI() == 10 || getI() == 5 || getI() == 4 || getI() == 3 || getI() == 2 || getI() == 1) {
                    ProxyServer.getInstance().broadcast(Message.END_COUNT.replace("count", String.valueOf(getI())).getContent());
                }

                if (getI() == 0) {
                    for (PrimePlayer pp : PrimeAPI.getOnlinePrimePlayers()) {
                        pp.getPp().disconnect(Message.END_KICKMESSAGE.getContent());
                    }

                    ProxyServer.getInstance().getScheduler().cancel(BungeeSystem.getInstance());
                    ProxyServer.getInstance().stop("§c Starte Neu....");
                    return;
                }


            }
        }, 1, 1, TimeUnit.SECONDS);


    }
}
