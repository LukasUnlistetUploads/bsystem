package de.primeapi.bungeesystem.commands;


import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.OnlineStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class AFKCommand extends Command {
    public AFKCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        PrimePlayer p = PrimeAPI.getPrimePlayer(commandSender);

        if(OnlineStats.getAFK(p.getPp().getUniqueId())){
            OnlineStats.updateAFK(p.getPp().getUniqueId(), false);
            p.sM(Message.AFK_NONAFK);
        }else {
            OnlineStats.updateAFK(p.getPp().getUniqueId(), true);
            p.sM(Message.AFK_AFK);
        }
    }
}
