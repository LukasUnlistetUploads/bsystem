package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.LogStats;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class KickCommand extends Command {

    public KickCommand() {
        super("kick");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = new PrimePlayer((ProxiedPlayer) sender);

        if (!p.hasPermission("bungee.kick")) {
            p.sendNoPerm();
            return;
        }

        if (!(args.length >= 2)) {
            p.sendMessage(Message.KICK_USAGE);
            return;
        }

        if (ProxyServer.getInstance().getPlayer(args[0]) == null) {
            p.sendMessage(Message.KICK_NOTFOUND);
            return;
        }
        PrimePlayer t = new PrimePlayer(ProxyServer.getInstance().getPlayer(args[0]));


        String reason = "";
        for (int argCo = 1; argCo <= args.length - 1; argCo++) {
            reason += args[argCo] + " ";
        }


        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungee.notify")) {
                if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                    all.sendMessage(Message.KICK_NOTIFY.replace("player", t.getName()).replace("admin", p.getName()).replace("reason", reason).getContent());
                }
            }
        }

        t.getPp().disconnect("§8» §6BanSystem §8« "
                + "\n §cDu wurdest vom §cNetzwerk §6Gekickt§8!"
                + "\n §7Grund§8: §e" + reason
                + "\n "
                + "\n "
                + "\n §cSolltest du zu unrecht gekickt worden sein,"
                + "\n kannst du im §eForum §coder im §eTeamspeak §cBeschwerde einreichen!");

        p.sendMessage(Message.KICK_SUCCESS.replace("player", t.getName()).replace("admin", p.getName()).replace("reason", reason));


        LogStats.addLogReason(t.getUniqeID(), reason, p.getUniqeID(), "0", String.valueOf(System.currentTimeMillis()), "Kick");

    }
}