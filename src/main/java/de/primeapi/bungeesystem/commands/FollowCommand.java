package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class FollowCommand extends Command {
    public FollowCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!(p.hasPermission("bungee.follow"))) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sM(Message.FOLLOW_USAGE);
            return;
        }
        //NOTE: Data.follow = <Admin,Spieler>
        //NOTE: Data.follow = <Spieler,Admin>

        if (Data.follow.containsKey(p.getPp())) {
            PrimePlayer t = PrimeAPI.getPrimePlayer(Data.follow.get(p.getPp()));
            p.sM(Message.FOLLOW_DEACTIVATE);
            Data.follow.remove(p.getPp());
            Data.follow2.remove(t.getPp());
        } else {
            if (!PrimeAPI.isOnline(args[0])) {
                p.sM(Message.FOLLOW_NOTFOUND);
                return;
            }

            PrimePlayer t = PrimeAPI.getPrimePlayer(args[0]);

            Data.follow.put(p.getPp(), t.getPp());
            Data.follow2.put(t.getPp(), p.getPp());
            p.sM(Message.FOLLOW_ACTIVATE);

        }
    }
}
