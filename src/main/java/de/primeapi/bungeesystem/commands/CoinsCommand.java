package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.CoinsAPI;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CoinsCommand extends Command implements TabExecutor {
    public CoinsCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage("Du hast unendlich coins!");
            return;
        }
        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (args.length == 0) {
            p.sM(Message.COINS_BALANCE.replace("%amount%", String.valueOf(CoinsAPI.getCoins(p.getUniqeID()))));
            if (Conf.getSettingsBoolean("Coins.pay")) {
                p.sM(Message.COINS_USAGE1);
            }
            if (p.hasPermission("bungee.coins.manage")) {
                p.sM(Message.COINS_USAGE2);
                p.sM(Message.COINS_USAGE3);
                p.sM(Message.COINS_USAGE4);
                p.sM(Message.COINS_USAGE5);
            }
        } else {
            if (args[0].equalsIgnoreCase("pay") && Conf.getSettingsBoolean("Coins.pay")) {
                int coins = 0;
                try {
                    coins = Integer.valueOf(args[2]);
                } catch (Exception ex) {
                    p.sM(Message.COINS_NONUMBER.replace("value", args[2]));
                    return;
                }

                if (coins < 1) {
                    p.sM(Message.COINS_MINVALUE);
                    return;
                }

                if (!PlayerStats.exist(args[1])) {
                    p.sM(Message.COINS_NOTFOUND);
                    return;
                }

                String uuid = PlayerStats.getUUID(args[1]);
                if (CoinsAPI.getCoins(p.getUniqeID()) >= coins) {
                    CoinsAPI.removecoins(p.getUniqeID(), coins);
                    CoinsAPI.addcoins(uuid, coins);
                    p.sM(Message.COINS_SUCCESS.replace("player", PlayerStats.getName(uuid)).replace("coins", String.valueOf(coins)));
                    if (PrimeAPI.isOnline(args[1])) {
                        PrimeAPI.getPrimePlayer(args[1]).sM(Message.COINS_NOTIFY.replace("name", p.getName()).replace("amount", String.valueOf(coins)));
                        return;
                    }
                }
                return;
            }


            if (!p.hasPermission("bungee.coins.manage")) {
                p.sendNoPerm();
                return;
            }

            if (args[0].equalsIgnoreCase("help")) {
                p.sM(Message.COINS_USAGE2);
                p.sM(Message.COINS_USAGE3);
                p.sM(Message.COINS_USAGE4);
                p.sM(Message.COINS_USAGE5);

                return;
            } else {
                if (args[0].equalsIgnoreCase("see") && args.length == 2) {
                    if (!PlayerStats.exist(args[1])) {
                        p.sM(Message.COINS_NOTFOUND);
                        return;
                    }

                    p.sM(Message.COINS_INFO.replace("player", PlayerStats.getName(PlayerStats.getUUID(args[1]))).replace("coins", String.valueOf(CoinsAPI.getCoins(PlayerStats.getUUID(args[1])))));
                    return;
                }

                if (args.length != 3) {
                    p.sM(Message.COINS_USAGE2);
                    p.sM(Message.COINS_USAGE3);
                    p.sM(Message.COINS_USAGE4);
                    p.sM(Message.COINS_USAGE5);
                    return;
                }
                int coins = 0;
                try {
                    coins = Integer.valueOf(args[2]);
                } catch (Exception ex) {
                    p.sM(Message.COINS_NONUMBER.replace("value", args[2]));
                    return;
                }

                if (!PlayerStats.exist(args[1])) {
                    p.sM(Message.COINS_NOTFOUND);
                    return;
                }

                String uuid = PlayerStats.getUUID(args[1]);

                if (args[0].equalsIgnoreCase("add")) {
                    CoinsAPI.addcoins(uuid, coins);
                } else if (args[0].equalsIgnoreCase("remove")) {
                    CoinsAPI.removecoins(uuid, coins);
                } else if (args[0].equalsIgnoreCase("set")) {
                    CoinsAPI.setCoins(uuid, coins);
                } else {
                    p.sM(Message.COINS_USAGE2);
                    p.sM(Message.COINS_USAGE3);
                    p.sM(Message.COINS_USAGE4);
                    p.sM(Message.COINS_USAGE5);
                    return;
                }
                p.sM(Message.COINS_SUCCESS1);

            }

        }

    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        List<String> set = new ArrayList<>();

        if(args.length == 1){
            if(!commandSender.hasPermission("bungee.coins.manage")){
                set.add("pay");
                return set;
            }
            String[] strings = {"pay", "see", "add", "remove", "set"};
            for(String s : strings){
                if(s.toLowerCase().startsWith(args[0].toLowerCase())) set.add(s);
            }
        }

        if(args.length == 2){
                for(ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()){
                    if(pp.getName().toLowerCase().startsWith(args[1].toLowerCase())) {
                        set.add(pp.getName());
                    }
                }
        }

        return set;
    }
}
