package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class JumpToCommand extends Command {

    public JumpToCommand(String name) {
        super("jumpto");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.jumpto")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sM(Message.JUMPTO_USAGE);
            return;
        }

        try {
            p.getPp().connect(ProxyServer.getInstance().getServerInfo(args[0]));
            p.sM(Message.JUMPTO_SUCCESS.replace("server", ProxyServer.getInstance().getServerInfo(args[0]).getName()));
        } catch (Exception ex) {
            p.sM(Message.JUMPTO_ERROR);
        }

    }

}
