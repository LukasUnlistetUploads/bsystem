package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class MotdCommand extends Command {
    public MotdCommand(String name) {
        super(name);
    }


    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }


        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.motd")) {
            p.sendNoPerm();
        }

        if (args.length < 2) {
            p.sendMessage(Message.MOTD_USAGE);
            TextComponent n1 = new TextComponent(" §e✎");
            n1.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/motd normal 1 " + Conf.getSettingsString("Motd.normal.1").replaceAll("§", "&")));
            n1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Bearbeite diese MoTD").create()));
            TextComponent n2 = new TextComponent(" §e✎");
            n2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/motd normal 2 " + Conf.getSettingsString("Motd.normal.2").replaceAll("§", "&")));
            n2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Bearbeite diese MoTD").create()));
            TextComponent w1 = new TextComponent(" §e✎");
            w1.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/motd wartung 1 " + Conf.getSettingsString("Motd.wartung.1").replaceAll("§", "&")));
            w1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Bearbeite diese MoTD").create()));
            TextComponent w2 = new TextComponent(" §e✎");
            w2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/motd wartung 2 " + Conf.getSettingsString("Motd.wartung.2").replaceAll("§", "&")));
            w2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Bearbeite diese MoTD").create()));
            p.getProxiedPlayer().sendMessage(new TextComponent("§7 Motd 1: §e" + Conf.getSettingsString("Motd.normal.1")), n1);
            p.getProxiedPlayer().sendMessage(new TextComponent("§7 Motd 2: §e" + Conf.getSettingsString("Motd.normal.2")), n2);
            p.getProxiedPlayer().sendMessage(new TextComponent("§7 Wartung-Motd 1: §e" + Conf.getSettingsString("Motd.wartung.1")), w1);
            p.getProxiedPlayer().sendMessage(new TextComponent("§7 Wartung-Motd 2: §e" + Conf.getSettingsString("Motd.wartung.2")), w2);
            return;
        }

        String type = "";

        if (args[0].equalsIgnoreCase("wartung")) {
            type = "wartung";
        } else if (args[0].equalsIgnoreCase("normal")) {
            type = "normal";
        } else {
            p.sendMessage(Message.MOTD_USAGE);
            return;
        }

        String line = "";
        if (args[1].equalsIgnoreCase("1")) {
            line = "1";
        } else if (args[1].equalsIgnoreCase("2")) {
            line = "2";
        } else {
            p.sendMessage(Message.MOTD_USAGE);
            return;
        }

        String msg = "";
        for (int i = 2; i < args.length; i++) {
            msg = msg + args[i] + " ";
        }

        Conf.setSettingsString("Motd." + type + "." + line, msg);
        p.sendMessage(Message.MOTD_SUCCESS);

    }
}
