package de.primeapi.bungeesystem.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ConnectCommand extends Command {
    public ConnectCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (strings.length == 1) {
            ProxiedPlayer p = (ProxiedPlayer) commandSender;
            ProxiedPlayer pp = ProxyServer.getInstance().getPlayer(strings[0]);
            p.connect(pp.getServer().getInfo());
        }


    }
}
