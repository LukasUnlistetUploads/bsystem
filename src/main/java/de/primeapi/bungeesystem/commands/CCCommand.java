package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CCCommand extends Command {

    public CCCommand(String name) {
        super("cc");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.cc")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sM(Message.CLEARCHAT_USAGE);
            return;
        }


        if (args[0].equalsIgnoreCase("local") || args[0].equalsIgnoreCase("l")) {
            for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
                if (all.getPp().getServer().getInfo().equals(p.getPp().getServer().getInfo())) {
                    if (!all.hasPermission("bungee.cc.ignore")) {
                        for (int i = 0; i < 350; i++) {
                            all.getProxiedPlayer().sendMessage(" ");
                        }
                        p.sM(Message.CLEARCHAT_SUCCESS);
                    }

                }

                if (all.hasPermission("bungee.cc.ignore")) {
                    p.sM(Message.CLEARCHAT_NOTIFY_LOCAL.replace("%server%", p.getPp().getServer().getInfo().getName()).replace("%name%", p.getName()));
                }
            }

        } else if (args[0].equalsIgnoreCase("global") || args[0].equalsIgnoreCase("g")) {
            for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
                if (!all.hasPermission("bungee.cc.ignore")) {
                    for (int i = 0; i < 350; i++) {
                        all.getProxiedPlayer().sendMessage(" ");
                    }
                    p.sM(Message.CLEARCHAT_SUCCESS);
                } else {
                    p.sM(Message.CLEARCHAT_NOTIFY_LOCAL.replace("%name%", p.getName()));
                }
            }

        }

    }

}
