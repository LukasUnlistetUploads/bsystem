package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.CoinsAPI;
import de.primeapi.bungeesystem.managers.CommandManager;
import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.LogStats;
import de.primeapi.bungeesystem.mysql.MuteStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.TimeUtils;
import de.primeapi.clansystem.bungee.mysql.SQLPlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.w3c.dom.Text;

public class InfoCommand extends Command {

    public InfoCommand() {
        super("info");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        String pr = Data.pr;
        ProxiedPlayer p = (ProxiedPlayer) sender;
        if (!p.hasPermission("bungee.info")) {
            p.sendMessage(Data.noperm);
            return;
        }

        if (args.length == 0) {
            p.sendMessage(pr + "§7 Benutze: §e/info [Name]");
            return;
        }
        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(pr + "§c Dieser Spieler hat das Netzwerk noch nie betreten");
            return;
        }

        String uuid = PlayerStats.getUUID(args[0]);


        p.sendMessage("§8§m--------§r §dSpielerinformationen §8§m--------");
        p.sendMessage("§8» §d");
        p.sendMessage("§8» §7Name§8: §e" + PlayerStats.getName(uuid));
        p.sendMessage("§8» §7Erster Join§8: §e" + TimeUtils.getDate(PlayerStats.getFirstJoin(args[0])));
        p.sendMessage("§8» §7Spielminuten§8: §e" + TimeUtils.MinutesToString(Long.valueOf(PlayerStats.getOnMins(uuid))));
        if (PrimeAPI.isOnline(args[0])) {
            p.sendMessage("§8» §7Zuletzt gesehen§8: §aOnline");
        } else {
            p.sendMessage("§8» §7Zuletzt gesehen§8: §e" + TimeUtils.getDate(PlayerStats.getLastJoin(args[0])));
        }
        if(BungeeSystem.getInstance().clansystem){
            try {
                SQLPlayer player = new SQLPlayer(p.getUniqueId());
                if(player.getClan() == null){
                    p.sendMessage("§8» §7Clan§8: §eKein Clan");
                }else {
                    TextComponent list = new TextComponent("§8[§eInfo§8]");
                    list.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§aZeige die Mitglieder des Clans an!").create()));
                    list.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/clan list " + player.getClan().getName()));
                    p.sendMessage(new TextComponent("§8» §7Clan§8: §e" + player.getClan().getName() + " "), list);
                    p.sendMessage(" §8» §7Tag§8: §e" + player.getClan().getPrefix());
                    p.sendMessage(" §8» §7Rang§8: §e" + player.getRankAsString());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        if (BanStats.isBanned(uuid)) {

            if (Long.valueOf(BanStats.getTime(uuid)) > System.currentTimeMillis() || Long.valueOf(BanStats.getTime(uuid)) == -1) {
                p.sendMessage("§8» §7Gebannt§8: §cJA");
                p.sendMessage(" §8» §7Gebannt von§8: §e" + BanStats.getAdminName(uuid));
                p.sendMessage(" §8» §7Grund§8: §e" + BanStats.getReason(uuid));
                p.sendMessage(" §8» §7Verbleibende Zeit§8: §e" + TimeUtils.getEnd(Long.valueOf(BanStats.getTime(uuid))));
                if (BanStats.isIPBanned(uuid)) {
                    p.sendMessage(" §8» §7IP-Ban §8: §aJa");
                } else {
                    p.sendMessage(" §8» §7IP-Ban §8: §aNein");
                }
                TextComponent unban = new TextComponent("§8[§cUnBan§8] ");
                unban.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cEntferne den Ban dieses Spielers").create()));

                unban.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, "/unban " + args[0] + " "));
                p.sendMessage(new TextComponent("§8» §7Entbannen§8: "), new TextComponent(unban));
            } else {
                BanStats.setBanned(uuid, false);
                p.sendMessage("§8» §7Gebannt§8: §aNein");
            }
        } else {
            p.sendMessage("§8» §7Gebannt§8: §aNein");
        }
        if (MuteStats.isMuted(uuid)) {

            if (Long.valueOf(MuteStats.getTime(uuid)) > System.currentTimeMillis() || Long.valueOf(MuteStats.getTime(uuid)) == -1) {
                p.sendMessage("§8» §7Gemuted§8: §cJA");
                p.sendMessage(" §8» §7Gemuted von§8: §e" + MuteStats.getAdminName(uuid));
                p.sendMessage(" §8» §7Grund§8: §e" + MuteStats.getReason(uuid));
                p.sendMessage(" §8» §7Verbleibende Zeit§8: §e" + TimeUtils.getEnd(Long.valueOf(MuteStats.getTime(uuid))));
                TextComponent unban = new TextComponent("§8[§cUnMute§8] ");
                unban.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cEntferne den Ban dieses Spielers").create()));

                unban.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, "/unmute " + args[0] + " "));
                p.sendMessage(new TextComponent("§8» §7Entbannen§8: "), new TextComponent(unban));
            } else {
                MuteStats.setMuted(uuid, false);
                p.sendMessage("§8» §7Gemuted§8: §aNein");
            }
        } else {
            p.sendMessage("§8» §7Gemuted§8: §aNein");
        }
        if(CommandManager.registeredCommands.contains(CommandManager.Command.COINS)) p.sendMessage("§8» §7Coins§8: §e" + CoinsAPI.getCoins(uuid));
        TextComponent history = new TextComponent("§8[§9View§8] ");
        history.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cSehe die History des Spielers ein.").create()));

        history.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0]));
        p.sendMessage(new TextComponent("§8» §7History Einträge§8: §e" + LogStats.getBanReasons(uuid).size()));
        p.sendMessage("§8» §7Accounts unter der Selben IP§8:");
        for (String name : PlayerStats.getAlts(PlayerStats.getIP(args[0]))) {
            String uuid2 = PlayerStats.getUUID(name);
            TextComponent info = new TextComponent("§8[§eInfo§8] ");
            info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("§cZeige Informationen des Spielers").create()));
            info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/info " + name));
            if (MuteStats.isMuted(uuid2) && BanStats.isBanned(uuid)) {
                p.sendMessage(new TextComponent(" §8» §7" + name + " §8[§cGemutet§8] §8[§4Gebannt§8] "), info);
            } else if (BanStats.isBanned(uuid2)) {
                p.sendMessage(new TextComponent(" §8» §7" + name + " §8[§4Gebannt§8] "), info);
            } else if (MuteStats.isMuted(uuid2)) {
                p.sendMessage(new TextComponent(" §8» §7" + name + " §8[§cGemutet§8] "), info);
            } else {
                p.sendMessage(new TextComponent(" §8» §7" + name), info);
            }

        }
        p.sendMessage("§8§m--------§r §dSpielerinformationen §8§m--------");


    }
}

