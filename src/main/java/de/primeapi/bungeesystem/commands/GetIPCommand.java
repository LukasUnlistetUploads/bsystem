package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class GetIPCommand extends Command {

    public GetIPCommand(String name) {
        super("getip");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.getip")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sM(Message.GETIP_USAGE);
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sM(Message.GETIP_NOTFOUND);
            return;
        }

        p.sM(Message.GETIP_MESSAGE.replace("player", PlayerStats.getName(PlayerStats.getUUID(args[0]))).replace("adress", PlayerStats.getIP(args[0])));
        p.sM(Message.GETIP_INFO);

    }

}
