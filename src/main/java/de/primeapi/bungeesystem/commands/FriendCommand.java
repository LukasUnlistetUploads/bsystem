package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.FriendStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.List;

public class FriendCommand extends Command {
    private String fpr = Data.pr;

    public FriendCommand(String name) {
        super(name);
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, final String[] args) {
        fpr = Data.pr + " ";

        if (sender instanceof ProxiedPlayer) {

            final PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

            if (args.length == 2) {

                if (args[0].equalsIgnoreCase("add")) {

                    if (p.getName().equalsIgnoreCase(args[1])) {
                        p.sendMessage(Message.FRIEND_ADD_SELF);
                        return;
                    }

                    if (!PlayerStats.exist(args[1].toLowerCase())) {
                        p.sendMessage(Message.FRIEND_NOT_FOUND);
                        return;
                    }


                    if (FriendStats.getFriends(p.getUUID().toString()).contains(PlayerStats.getName(PlayerStats.getUUID(args[1])))) {
                        p.sendMessage(Message.FRIEND_ADD_ALREADY_FRIEND.replace("player", args[1]));
                        return;
                    }

                    if (FriendStats.getFriendAnfrager(p.getUUID().toString(), PlayerStats.getUUID(args[1]))
                            .equalsIgnoreCase(p.getUUID().toString())) {
                        p.sendMessage(Message.FRIEND_ADD_ALREADY_REQUEST.replace("player", args[1]));
                        return;
                    }

                    if (FriendStats.getFriendAnfragen(p.getUUID().toString())
                            .contains(PlayerStats.getName(PlayerStats.getUUID(args[1])))) {
                        p.sendMessage(Message.FRIEND_ADD_ALREADY_GOT.replace("player", args[1]));
                        return;
                    }

                    if (FriendStats.getToggleStatus(PlayerStats.getUUID(args[1]), "friends") == 1) {
                        p.sendMessage(Message.FRIEND_ADD_CANTSEND);
                        return;
                    }

                    FriendStats.addFriend(p.getUUID().toString(), PlayerStats.getUUID(args[1]), p.getUUID().toString(),
                            "ANFRAGE");
                    FriendStats.addFriend(PlayerStats.getUUID(args[1]), p.getUUID().toString(), p.getUUID().toString(),
                            "ANFRAGE");

                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);

                    if (target != null) {
                        target.sendMessage(fpr + "§e" + Methods.getPermissionPrefix(p.getPp()).replace("&", "§") + p.getName()
                                + " §7hat dir eine Anfrage gesendet.");

                        TextComponent annehmen = new TextComponent(fpr + "§8[§aAkzeptieren§8]");
                        annehmen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                new ComponentBuilder("§aKlicke zum annehmen").create()));
                        annehmen.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + p.getName()));

                        TextComponent ablehnen = new TextComponent(fpr + "§8[§cAblehnen§8]");
                        ablehnen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                new ComponentBuilder("§cKlicke zum ablehnen").create()));
                        ablehnen.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + p.getName()));

                        target.sendMessage(annehmen);
                        target.sendMessage(ablehnen);

                        args[1] = Methods.getPermissionPrefix(p.getPp()).replace("&", "§") + target.getName();
                    }

                    p.sendMessage(Message.FRIEND_ADD_SUCCESS.replace("player", args[1]));

                } else if (args[0].equalsIgnoreCase("remove")) {

                    if (p.getName().equalsIgnoreCase(args[1])) {
                        p.sendMessage(Message.FRIEND_SELF);
                        return;
                    }

                    if (!PlayerStats.exist(args[1])) {
                        p.sendMessage(Message.FRIEND_NOT_FOUND);
                        return;
                    }

                    if (!FriendStats.getFriends(p.getUUID().toString()).contains(PlayerStats.getName(PlayerStats.getUUID(args[1])))) {
                        p.sendMessage(Message.FRIEND_REMOVE_NOFRIENDS.replace("player", args[1]));
                        return;
                    }

                    FriendStats.removeFriend(p.getUUID().toString(), PlayerStats.getUUID(args[1]));
                    FriendStats.removeFriend(PlayerStats.getUUID(args[1]), p.getUUID().toString());

                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);

                    if (target != null) {
                        target.sendMessage(Message.FRIEND_REMOVE_NOTIFY.replace("player", p.getName()).getContent());

                        args[1] = Methods.getPermissionPrefix(p.getPp()).replace("&", "§") + target.getName();
                    }

                    p.sendMessage(Message.FRIEND_REMOVE_SUCCESS.replace("player", args[1]));

                } else if (args[0].equalsIgnoreCase("accept")) {

                    if (p.getName().equalsIgnoreCase(args[1])) {
                        p.sendMessage(Message.FRIEND_SELF);
                        return;
                    }

                    if (!PlayerStats.exist(args[1].toLowerCase())) {
                        p.sendMessage(Message.FRIEND_NOT_FOUND);
                        return;
                    }

                    if (FriendStats.getFriends(p.getUUID().toString()).contains(PlayerStats.getName(PlayerStats.getUUID(args[1])))) {
                        p.sendMessage(Message.FRIEND_ADD_ALREADY_FRIEND);
                        return;
                    }

                    if (!FriendStats.getFriendAnfragen(p.getUUID().toString())
                            .contains(PlayerStats.getName(PlayerStats.getUUID(args[1])))) {
                        p.sendMessage(Message.FRIEND_ACCEPT_NOREQUEST.replace("player", args[1]));
                        return;
                    }

                    FriendStats.setFreundStatus(p.getUUID().toString(), PlayerStats.getUUID(args[1]), "FREUND");
                    FriendStats.setFreundStatus(PlayerStats.getUUID(args[1]), p.getUUID().toString(), "FREUND");

                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);

                    if (target != null) {
                        target.sendMessage(Message.FRIEND_ACCEPT_NOTIFY.replace("player", args[1]).getContent());
                    }

                    p.sendMessage(Message.FRIEND_ACCEPT_SUCCESS.replace("player", args[1]));

                } else if (args[0].equalsIgnoreCase("deny")) {

                    if (p.getName().equalsIgnoreCase(args[1])) {
                        p.sendMessage(Message.FRIEND_SELF);
                        return;
                    }

                    if (!PlayerStats.exist(args[1].toLowerCase())) {
                        p.sendMessage(Message.FRIEND_NOT_FOUND);
                        return;
                    }

                    if (FriendStats.getFriends(p.getUUID().toString()).contains(PlayerStats.getName(PlayerStats.getUUID(args[1])))) {
                        p.sendMessage(Message.FRIEND_ADD_ALREADY_FRIEND);
                        return;
                    }

                    if (!FriendStats.getFriendAnfragen(p.getUUID().toString())
                            .contains(PlayerStats.getName(PlayerStats.getUUID(args[1])))) {
                        p.sendMessage(Message.FRIEND_DENY_NOREQUEST.replace("player", args[1]));
                        return;
                    }

                    FriendStats.removeFriend(p.getUUID().toString(), PlayerStats.getUUID(args[1]));
                    FriendStats.removeFriend(PlayerStats.getUUID(args[1]), p.getUUID().toString());

                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);

                    if(target != null){
                        target.sendMessage(Message.FRIEND_DENY_SUCCESS.replace("player", p.getName()).getContent());
                    }

                    p.sendMessage(Message.FRIEND_DENY_SUCCESS.replace("player", args[1]));

                } else if (args[0].equalsIgnoreCase("jump") || args[0].equalsIgnoreCase("join")) {

                    if (p.getName().equalsIgnoreCase(args[1])) {
                        p.sendMessage(Message.FRIEND_SELF);
                        return;
                    }

                    if (!PlayerStats.exist(args[1].toLowerCase())) {
                        p.sendMessage(Message.FRIEND_NOT_FOUND);
                        return;
                    }

                    if (!FriendStats.getFriends(p.getUUID().toString()).contains(args[1])) {
                        p.sendMessage(Message.FRIEND_JUMP_NOFRRIENDS.replace("player", args[1]));
                        return;
                    }

                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[1]);

                    if (target == null) {
                        p.sendMessage(Message.FRIEND_JUMP_NOT_ONLINE.replace("player", args[1]));
                        return;
                    }

                    if (FriendStats.getToggleStatus(target.getUniqueId().toString(), "jump") == 1) {
                        p.sendMessage(Message.FRIEND_JUMP_DENY.replace("player", args[1]));
                        return;
                    }

                    p.sendMessage(Message.FRIEND_JUMP_SUCCESS.replace("server", target.getServer().getInfo().getName()));

                    p.connect(target.getServer().getInfo());

                } else if (args[0].equalsIgnoreCase("list")) {

                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            try {
                                Integer.valueOf(args[1]);
                            } catch (NumberFormatException ex) {
                                p.sendMessage(Message.FRIEND_LIST_NONUMBER);
                                return;
                            }

                            List<String> friendlist = FriendStats.getFriends(p.getUUID().toString());

                            List<String> onlinefriends = new ArrayList<>();
                            List<String> offlinefriends = new ArrayList<>();
                            List<String> friends = new ArrayList<>();

                            for (String st : friendlist) {
                                ProxiedPlayer targetpl = ProxyServer.getInstance().getPlayer(st);

                                if (targetpl != null) {
                                    onlinefriends.add(st);
                                } else {
                                    offlinefriends.add(st);
                                }
                            }

                            for (String online : onlinefriends) {
                                friends.add(online);
                            }

                            for (String offline : offlinefriends) {
                                friends.add(offline);
                            }

                            if (friends.size() <= 0) {
                                p.sendMessage(Message.FRIEND_LIST_NOFRIENDS);
                                return;
                            }

                            int seite = Integer.valueOf(args[1]);

                            int max = (friends.size() / 8) + 1;

                            if (seite > max) {
                                seite = max;
                            } else if (seite <= 1) {
                                seite = 1;
                            }

                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Freundeliste §7§m]--------------");
                            if (friends.size() < (seite * 8)) {
                                for (int i = (seite * 8) - 8; i < friends.size(); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(friends.get(i));

                                    if (target == null) {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §7OFFLINE");
                                    } else {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §a" + target.getServer().getInfo().getName());
                                    }
                                }
                            } else {
                                for (int i = (seite * 8) - 8; i < (seite * 8); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(friends.get(i));

                                    if (target == null) {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §7OFFLINE");
                                    } else {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §a" + target.getServer().getInfo().getName());
                                    }
                                }
                            }
                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Freundeliste §7§m]--------------");

                        }
                    });

                } else if (args[0].equalsIgnoreCase("requests")) {

                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            try {
                                Integer.valueOf(args[1]);
                            } catch (NumberFormatException ex) {
                                p.sendMessage(Message.FRIEND_REQUESTS_NONUMBER);
                                return;
                            }

                            List<String> friends = FriendStats.getFriendAnfragen(p.getUUID().toString());

                            if (friends.size() <= 0) {
                                p.sendMessage(Message.FRIEND_REQUESTS_NOREQUESTS);
                                return;
                            }

                            int seite = Integer.valueOf(args[1]);

                            int max = (friends.size() / 8) + 1;

                            if (seite > max) {
                                seite = max;
                            } else if (seite <= 1) {
                                seite = 1;
                            }

                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Anfragen §7§m]--------------");
                            if (friends.size() < (seite * 8)) {
                                for (int i = (seite * 8) - 8; i < friends.size(); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    TextComponent annehmen = new TextComponent("§7[§aAnnehmen§7]");
                                    annehmen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§aNehme diesen Spieler an.").create()));
                                    annehmen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + friends.get(i)));

                                    TextComponent ablehnen = new TextComponent("§7[§cAblehnen§7]");
                                    ablehnen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§4Lehne diesen Spieler ab.").create()));
                                    ablehnen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + friends.get(i)));

                                    p.getProxiedPlayer().sendMessage(new TextComponent("§7" + rang + friends.get(i)),
                                            new TextComponent("   "), annehmen, new TextComponent("  "), ablehnen);
                                }
                            } else {
                                for (int i = (seite * 8) - 8; i < (seite * 8); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    TextComponent annehmen = new TextComponent("§7[§aAnnehmen§7]");
                                    annehmen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§aNehme diesen Spieler an.").create()));
                                    annehmen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + friends.get(i)));

                                    TextComponent ablehnen = new TextComponent("§7[§cAblehnen§7]");
                                    ablehnen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§4Lehne diesen Spieler ab.").create()));
                                    ablehnen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + friends.get(i)));

                                    p.getProxiedPlayer().sendMessage(new TextComponent("§7" + rang + friends.get(i)),
                                            new TextComponent("   "), annehmen, new TextComponent("  "), ablehnen);
                                }
                            }
                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Anfragen §7§m]--------------");
                        }
                    });
                }
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("help")) {
                    p.sendMessage(Message.FRIEND_HELP_01);
                    p.sendMessage(Message.FRIEND_HELP_02);
                    p.sendMessage(Message.FRIEND_HELP_03);
                    p.sendMessage(Message.FRIEND_HELP_04);
                    p.sendMessage(Message.FRIEND_HELP_05);
                    p.sendMessage(Message.FRIEND_HELP_06);
                    p.sendMessage(Message.FRIEND_HELP_07);
                    p.sendMessage(Message.FRIEND_HELP_08);
                    p.sendMessage(Message.FRIEND_HELP_09);
                    p.sendMessage(Message.FRIEND_HELP_10);
                    p.sendMessage(Message.FRIEND_HELP_11);
                    p.sendMessage(Message.FRIEND_HELP_12);
                    p.sendMessage(Message.FRIEND_HELP_13);
                } else if (args[0].equalsIgnoreCase("list")) {

                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            List<String> friendlist = FriendStats.getFriends(p.getUUID().toString());

                            List<String> onlinefriends = new ArrayList<>();
                            List<String> offlinefriends = new ArrayList<>();
                            List<String> friends = new ArrayList<>();

                            for (String st : friendlist) {
                                ProxiedPlayer targetpl = ProxyServer.getInstance().getPlayer(st);

                                if (targetpl != null) {
                                    onlinefriends.add(st);
                                } else {
                                    offlinefriends.add(st);
                                }
                            }

                            for (String online : onlinefriends) {
                                friends.add(online);
                            }

                            for (String offline : offlinefriends) {
                                friends.add(offline);
                            }

                            if (friends.size() <= 0) {
                                p.sendMessage(Message.FRIEND_LIST_NOFRIENDS);
                                return;
                            }

                            int seite = 1;

                            friends.size();

                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Freundeliste §7§m]--------------");
                            if (friends.size() < (seite * 8)) {
                                for (int i = (seite * 8) - 8; i < friends.size(); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(friends.get(i));

                                    if (target == null) {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §7OFFLINE");
                                    } else {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §a"
                                                + target.getServer().getInfo().getName());
                                    }
                                }
                            } else {
                                for (int i = (seite * 8) - 8; i < (seite * 8); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    ProxiedPlayer target = ProxyServer.getInstance().getPlayer(friends.get(i));

                                    if (target == null) {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §7OFFLINE");
                                    } else {
                                        p.getProxiedPlayer().sendMessage("§7" + rang + friends.get(i) + " §8» §a"
                                                + target.getServer().getInfo().getName());
                                    }
                                }
                            }
                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Freundeliste §7§m]--------------");

                        }
                    });

                } else if (args[0].equalsIgnoreCase("requests")) {

                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            List<String> friends = FriendStats.getFriendAnfragen(p.getUUID().toString());

                            if (friends.size() <= 0) {
                                p.sendMessage(Message.FRIEND_REQUESTS_NOREQUESTS);
                                return;
                            }

                            int seite = 1;

                            friends.size();

                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Anfragen §7§m]--------------");
                            if (friends.size() < (seite * 8)) {
                                for (int i = (seite * 8) - 8; i < friends.size(); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    TextComponent annehmen = new TextComponent("§7[§aAnnehmen§7]");
                                    annehmen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§aNehme diesen Spieler an.").create()));
                                    annehmen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + friends.get(i)));

                                    TextComponent ablehnen = new TextComponent("§7[§cAblehnen§7]");
                                    ablehnen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§4Lehne diesen Spieler ab.").create()));
                                    ablehnen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + friends.get(i)));

                                    p.getProxiedPlayer().sendMessage(new TextComponent("§7" + rang + friends.get(i)),
                                            new TextComponent("   "), annehmen, new TextComponent("  "), ablehnen);
                                }
                            } else {
                                for (int i = (seite * 8) - 8; i < (seite * 8); i++) {

                                    String rang = Methods.getPermissionPrefix(p.getPp()).replace("&", "§");

                                    TextComponent annehmen = new TextComponent("§7[§aAnnehmen§7]");
                                    annehmen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§aNehme diesen Spieler an.").create()));
                                    annehmen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend accept " + friends.get(i)));

                                    TextComponent ablehnen = new TextComponent("§7[§cAblehnen§7]");
                                    ablehnen.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                            new ComponentBuilder("§4Lehne diesen Spieler ab.").create()));
                                    ablehnen.setClickEvent(
                                            new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/friend deny " + friends.get(i)));

                                    p.getProxiedPlayer().sendMessage(new TextComponent("§7" + rang + friends.get(i)),
                                            new TextComponent("   "), annehmen, new TextComponent("  "), ablehnen);
                                }
                            }
                            p.getProxiedPlayer().sendMessage("§7§m--------------[§b Anfragen §7§m]--------------");
                        }
                    });

                } else if (args[0].equalsIgnoreCase("toggle")) {
                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            if (FriendStats.getToggleStatus(p.getUUID().toString(), "friends") == 0) {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "friends", 1);

                                p.sendMessage(Message.FRIEND_TOGGLE_OFF);
                            } else {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "friends", 0);

                                p.sendMessage(Message.FRIEND_TOGGLE_ON);
                            }
                        }
                    });
                } else if (args[0].equalsIgnoreCase("togglemessage")) {
                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            if (FriendStats.getToggleStatus(p.getUUID().toString(), "msg") == 0) {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "msg", 1);

                                p.sendMessage(Message.FRIEND_TOGGLEMESSAGE_1);
                            } else if (FriendStats.getToggleStatus(p.getUUID().toString(), "msg") == 1) {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "msg", 2);

                                p.sendMessage(Message.FRIEND_TOGGLEMESSAGE_2);
                            } else {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "msg", 0);

                                p.sendMessage(Message.FRIEND_TOGGLEMESSAGE_3);
                            }
                        }
                    });
                } else if (args[0].equalsIgnoreCase("togglejump")) {
                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            if (FriendStats.getToggleStatus(p.getUUID().toString(), "jump") == 0) {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "jump", 1);

                                p.sendMessage(Message.FRIEND_TOGGLEJUMP_OFF);
                            } else {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "jump", 0);

                                p.sendMessage(Message.FRIEND_TOGGLEJUMP_ON);
                            }
                        }
                    });
                } else if (args[0].equalsIgnoreCase("togglenotify")) {
                    ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            if (FriendStats.getToggleStatus(p.getUUID().toString(), "online") == 0) {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "online", 1);

                                p.sendMessage(Message.FRIEND_NOTIFY_OFF);
                            } else {
                                FriendStats.setToggleStatus(p.getUUID().toString(), "online", 0);

                                p.sendMessage(Message.FRIEND_NOTIFY_ON);
                            }
                        }
                    });
                } else {
                    p.sendMessage(Message.FRIEND_HELP_01);
                    p.sendMessage(Message.FRIEND_HELP_02);
                    p.sendMessage(Message.FRIEND_HELP_03);
                    p.sendMessage(Message.FRIEND_HELP_04);
                    p.sendMessage(Message.FRIEND_HELP_05);
                    p.sendMessage(Message.FRIEND_HELP_06);
                    p.sendMessage(Message.FRIEND_HELP_07);
                    p.sendMessage(Message.FRIEND_HELP_08);
                    p.sendMessage(Message.FRIEND_HELP_09);
                    p.sendMessage(Message.FRIEND_HELP_10);
                    p.sendMessage(Message.FRIEND_HELP_11);
                    p.sendMessage(Message.FRIEND_HELP_12);
                    p.sendMessage(Message.FRIEND_HELP_13);
                }
            } else {
                p.sendMessage(Message.FRIEND_HELP_01);
                p.sendMessage(Message.FRIEND_HELP_02);
                p.sendMessage(Message.FRIEND_HELP_03);
                p.sendMessage(Message.FRIEND_HELP_04);
                p.sendMessage(Message.FRIEND_HELP_05);
                p.sendMessage(Message.FRIEND_HELP_06);
                p.sendMessage(Message.FRIEND_HELP_07);
                p.sendMessage(Message.FRIEND_HELP_08);
                p.sendMessage(Message.FRIEND_HELP_09);
                p.sendMessage(Message.FRIEND_HELP_10);
                p.sendMessage(Message.FRIEND_HELP_11);
                p.sendMessage(Message.FRIEND_HELP_12);
                p.sendMessage(Message.FRIEND_HELP_13);
            }
        }
    }

}

