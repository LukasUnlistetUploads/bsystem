package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ChatFilterCommand extends Command {

    public ChatFilterCommand() {
        super("chatfilter");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = new PrimePlayer((ProxiedPlayer) sender);

        if (!p.hasPermission("bungee.chatfilter")) {
            p.sendNoPerm();
            return;
        }

        if (args.length < 1) {
            p.getProxiedPlayer().sendMessage("§7 Benutze: /chatfilter blacklist add [Wort]");
            p.getProxiedPlayer().sendMessage("§7 Benutze: /chatfilter blacklist remove [Wort]");
            p.getProxiedPlayer().sendMessage("§7 Benutze: /chatfilter blacklist list [Wort]");
            p.getProxiedPlayer().sendMessage("§7 Benutze: /chatfilter filter add [Wort]");
            p.getProxiedPlayer().sendMessage("§7 Benutze: /chatfilter filter remove [Wort]");
            p.getProxiedPlayer().sendMessage("§7 Benutze: /chatfilter filter list ");
            return;
        }

        if (args[0].equalsIgnoreCase("blacklist")) {
            if (args[1].equalsIgnoreCase("add")) {
                Conf.addblacklist(args[2]);
            }
            if (args[1].equalsIgnoreCase("remove")) {
                Conf.removeblacklist(args[2]);
            }
            if (args[1].equalsIgnoreCase("list")) {
                for (String bl : Conf.getBlacklist()) {
                    p.getProxiedPlayer().sendMessage("§8» §7" + bl);
                }
            }


        }

        if (args[0].equalsIgnoreCase("filter")) {
            if (args[1].equalsIgnoreCase("add")) {
                Conf.addfilter(args[2]);
            }
            if (args[1].equalsIgnoreCase("remove")) {
                Conf.removefilter(args[2]);
            }
            if (args[1].equalsIgnoreCase("list")) {
                for (String bl : Conf.getFilter()) {
                    p.getProxiedPlayer().sendMessage("§8» §7" + bl);
                }
            }


        }


    }
}
