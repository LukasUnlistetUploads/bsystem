package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ForumCommand extends Command {
    public ForumCommand(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        sender.sendMessage("" + Conf.getSettingsString("ForumMSG"));

    }
}
