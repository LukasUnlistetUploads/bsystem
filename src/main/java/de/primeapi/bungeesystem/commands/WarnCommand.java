package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.mysql.WarnStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class WarnCommand extends Command {

    public WarnCommand() {
        super("warn");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.warn")) {
            p.sendNoPerm();
            return;
        }
        if (args.length < 2) {
            p.sendMessage(Message.WARN_USAGE);
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(Message.WARN_NOTFOUND);
            return;
        }


        String uuid = PlayerStats.getUUID(args[0]);
        String msg = "";
        for (int argCo = 1; argCo <= args.length - 1; argCo++) {
            msg = msg + args[argCo] + " ";
        }

        ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
        if (target != null) {
            Title title = ProxyServer.getInstance().createTitle();
            title.title(new TextComponent(Message.WARN_TITLE_1.replace("reason", msg).getContent()));
            title.subTitle(new TextComponent(Message.WARN_TITLE_2.replace("reason", msg).getContent()));
            title.send(target);

            target.sendMessage(Message.WARN_NOTIFY_PLAYER.replace("reason", msg).replace("count", String.valueOf(WarnStats.getWarnReasons(uuid).size() + 1)).getContent());
        }

        WarnStats.addWarnReason(uuid, msg, p.getUUID().toString(), System.currentTimeMillis() + "");


        p.sendMessage(Message.WARN_SUCCESS.replace("player", PlayerStats.getName(uuid)));


        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungee.notify")) {
                if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                    all.sendMessage(Message.WARN_SUCCESS.replace("player", PlayerStats.getName(uuid)).replace("admin", p.getName()).replace("reason", msg).getContent());
                }
            }
        }


    }

}
