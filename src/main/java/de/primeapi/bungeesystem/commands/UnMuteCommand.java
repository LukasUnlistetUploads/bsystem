package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.LogStats;
import de.primeapi.bungeesystem.mysql.MuteStats;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class UnMuteCommand extends Command {

    public UnMuteCommand() {
        super("unmute");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        ProxiedPlayer pp = (ProxiedPlayer) sender;

        PrimePlayer p = new PrimePlayer(pp);

        if (!p.hasPermission("bungee.unmute")) {
            p.sendNoPerm();
            return;
        }

        if (args.length < 2) {
            p.sendMessage(Message.UNMUTE_USAGE);
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(Message.UNMUTE_NOTFOUND);
            return;
        }

        String uuid = PlayerStats.getUUID(args[0]);

        if (!MuteStats.isMuted(uuid)) {
            p.sendMessage(Message.UNMUTE_NOTBANNED);
            return;
        }
        String reason = "";
        for (int argCo = 1; argCo <= args.length - 1; argCo++) {
            reason += args[argCo] + " ";
        }

        MuteStats.setMuted(uuid, false);
        p.setMuted(false);
        LogStats.addLogReason(uuid, reason, p.getUniqeID(), "0", String.valueOf(System.currentTimeMillis()), "Unmute");

        p.sendMessage(Message.UNMUTE_SUCCESS.replace("player", PlayerStats.getName(uuid)));

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungee.notify")) {
                if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                    all.sendMessage(Message.UNMUTE_NOTIFY.replace("player", PlayerStats.getName(uuid)).replace("admin", p.getName()).getContent());
                }
            }
        }


    }
}
