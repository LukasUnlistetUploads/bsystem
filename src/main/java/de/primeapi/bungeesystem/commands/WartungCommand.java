package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class WartungCommand extends Command {
    public WartungCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.wartung")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sendMessage(Message.WARTUNG_USAGE);
            return;
        }

        if (args[0].equalsIgnoreCase("aus")) {
            Conf.setSettingsBoolean("Wartung", false);
            p.sendMessage(Message.WARTUNG_SUCCESS);
            return;
        }

        Conf.setSettingsBoolean("Wartung", true);
        p.sendMessage(Message.WARTUNG_SUCCESS);


    }
}
