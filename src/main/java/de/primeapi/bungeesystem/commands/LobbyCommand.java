package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import jdk.internal.org.jline.terminal.impl.ExecPty;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Command;

import java.lang.reflect.Proxy;
import java.util.Random;

public class LobbyCommand extends Command {
    public LobbyCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        PrimePlayer p = PrimeAPI.getPrimePlayer(commandSender);

        String lobby = Conf.getSettingsString("Lobby.name");
        int amount = Conf.getSettingsInteger("Lobby.amount");

        if(p.getServerInfo().getName().toLowerCase().startsWith(lobby.toLowerCase())){
            p.sendMessage(Message.LOBBY_ALREADY);
            return;
        }

        if(amount <= 1) {
            try {
                p.connect(ProxyServer.getInstance().getServerInfo(lobby));
            }catch (Exception ex){
                p.sendMessage(Message.LOBBY_ERROR.replace("lobby", lobby));
            }
        } else {
            int i = new Random().nextInt(amount) + 1;
            try {
            p.connect(ProxyServer.getInstance().getServerInfo(lobby + i));
            }catch (Exception ex){
                p.sendMessage(Message.LOBBY_ERROR.replace("lobby", lobby));
            }
        }
    }
}
