package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BauServerCommand extends Command {
    public BauServerCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.bauserver")) {
            p.sendNoPerm();
            return;
        }

        ServerInfo serverInfo;
        try {
            serverInfo = ProxyServer.getInstance().getServerInfo(Conf.getSettingsString("BauServerName"));
        } catch (Exception ex) {
            p.sM(Message.BAUSERVER_ERROR);
            return;
        }

        p.getPp().connect(serverInfo);


    }
}
