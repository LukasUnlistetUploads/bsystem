package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.FriendStats;
import de.primeapi.bungeesystem.mysql.OnlineStats;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class    ReplyCommand extends Command {
    public ReplyCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        final PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (args.length >= 1) {

            if (!Data.msg.containsKey(p.getName())) {
                p.sendMessage(Message.MSG_NOTFOUND);
                return;
            }

            String pl = Data.msg.get(p.getName());

            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(pl);

            if (target == null) {
                p.sendMessage(Message.MSG_NOTFOUND);
                return;
            }
            if (target.getName().equalsIgnoreCase(p.getName())) {
                p.sendMessage(Message.MSG_SELF);
                return;
            }

            int status = FriendStats.getToggleStatus(target.getUniqueId().toString(), "msg");

            if (status == 1) {
                p.sendMessage(Message.MSG_DEACTIVATED);
            }
            if (status == 2 && !FriendStats.getFriends(p.getUUID().toString()).contains(args[0].toLowerCase())) {
                p.sendMessage(Message.MSG_DEACTIVATED);
                return;
            }

            String msg = "";

            for (int argCo = 0; argCo <= args.length - 1; argCo++) {
                msg = msg + args[argCo] + " ";
            }

            target.sendMessage(Data.pr + " " + p.getName() + " §8➞ §a" + target.getName() + " §8» §7" + msg);
            p.getProxiedPlayer().sendMessage(Data.pr + " " + p.getName() + " §8➞ §a" + target.getName() + " §8» §7" + msg);
            if(OnlineStats.getAFK(target.getUniqueId())) p.sendMessage(Message.MSG_AFK);

        } else {
            p.sendMessage(Message.MSG_REPLY_USAGE);
        }

    }
}
