package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.LogStats;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

import java.util.HashSet;
import java.util.Set;

public class ABanCommand extends Command implements TabExecutor {

    public ABanCommand() {
        super("aban");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }
        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.aban")) {
            p.sendMessage(Message.NO_PERMS);
            return;
        }

        if (!(args.length >= 3)) {
            p.sendMessage(Message.ABAN_USAGE);
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(Message.ABAN_NOTFOUND);
            return;
        }
        String UUID = PlayerStats.getUUID(args[0]);

        int l = 0;
        try {
            l = Integer.valueOf(args[1].replace("m", "").replace("h", "").replace("d", ""));
        } catch (Exception ex) {
            p.sendMessage(Message.ABAN_NO_NUMBER.replace("%time%", args[1].replace("m", "").replace("h", "").replace("d", "")));
        }
        String type = args[1].replaceAll(String.valueOf(l), "");

        Long time = (long) 0;
        String timel = "";
        if (type.equalsIgnoreCase("m")) {
            time = TimeUtils.toMillies(0, 0, l);
            timel = l + " Minuten";
        } else if (type.equalsIgnoreCase("h")) {
            time = TimeUtils.toMillies(0, l, 0);
            timel = l + " Stunden";
        } else if (type.equalsIgnoreCase("d")) {
            time = TimeUtils.toMillies(l, 0, 0);
            timel = l + " Tage";
        } else if (type.equalsIgnoreCase("P")) {
            time = (long) -1;
            timel = "Permanent";
        } else {
            p.sendMessage(Message.ABAN_NO_VALIDNUMBER);
            return;
        }
        if(time != -1) {
            time = time + System.currentTimeMillis();
        }
        String reason = "";
        for (int argCo = 2; argCo <= args.length - 1; argCo++) {
            reason += args[argCo] + " ";
        }

        BanStats.addBan(UUID, reason, p.getUniqeID().toString(), time);
        LogStats.addLogReason(UUID, reason, p.getUniqeID().toString(), timel, String.valueOf(System.currentTimeMillis()), "Ban");

        p.sendMessage(Message.ABAN_SUCCESS.replace("%player%", PlayerStats.getName(UUID)).replace("%reason%", reason));

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungee.notify")) {
                if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                    all.sendMessage(Message.ABAN_SUCCESS_NOTIFY.replace("%admin%", p.getName()).replace("%player%", PlayerStats.getName(UUID)).replace("%reason%", reason).getContent());
                }
            }
        }


        if (ProxyServer.getInstance().getPlayer(args[0]) != null) {
            ProxyServer.getInstance().getPlayer(args[0]).disconnect(Conf.getSettingsString("messages.ban")
                    .replaceAll("%reason%", reason)
                    .replaceAll("%time%", timel));
        }


    }

    @Override
    public Iterable<String> onTabComplete(CommandSender commandSender, String[] args) {
        Set<String> set = new HashSet<>();

        if(args.length == 1){
            for(ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()){
                if(pp.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                    set.add(pp.getName());
                }
            }
        }



        return set;
    }
}