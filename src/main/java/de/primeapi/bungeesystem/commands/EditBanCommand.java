package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.LogStats;
import de.primeapi.bungeesystem.mysql.NotifyStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class EditBanCommand extends Command {

    public EditBanCommand() {
        super("editban");
    }

    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = new PrimePlayer((ProxiedPlayer) sender);
        if (!p.hasPermission("ban.editban")) {
            p.sendNoPerm();
            return;
        }

        if (!(args.length >= 3)) {
            p.sM(Message.EDITBAN_USAGE);
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sM(Message.EDITBAN_NOTFOUND);
            return;
        }

        String uuid = PlayerStats.getUUID(args[0]);

        if (!BanStats.isBanned(uuid)) {
            p.sM(Message.EDITBAN_NOBANNEND);
            return;
        }
        int l = 0;
        try {
            l = Integer.valueOf(args[1].replace("m", "").replace("h", "").replace("d", ""));
        } catch (Exception ex) {
            p.sM(Message.EDITBAN_NO_NUMBER.replace("time", args[1].replace("m", "").replace("h", "").replace("d", "")));
        }
        String type = args[1].replaceAll(String.valueOf(l), "");

        Long time = (long) 0;
        String timel = "";
        if (type.equalsIgnoreCase("m")) {
            time = TimeUtils.toMillies(0, 0, l);
            timel = l + " Minuten";
        } else if (type.equalsIgnoreCase("h")) {
            time = TimeUtils.toMillies(0, l, 0);
            timel = l + " Stunden";
        } else if (type.equalsIgnoreCase("d")) {
            time = TimeUtils.toMillies(l, 0, 0);
            timel = l + " Tage";
        } else if (type.equalsIgnoreCase("P")) {
            time = (long) -1;
            timel = "Permanent";
        } else {
            p.sM(Message.EDITBAN_NO_VALIDNUMBER);
            return;
        }

        time = time + System.currentTimeMillis();
        String reason = "";
        for (int argCo = 2; argCo <= args.length - 1; argCo++) {
            reason += args[argCo] + " ";
        }

        BanStats.setTime(uuid, time);
        BanStats.setReason(uuid, reason);
        LogStats.addLogReason(uuid, reason, p.getUniqeID(), timel, String.valueOf(System.currentTimeMillis()), "edit");
        p.sM(Message.EDITBAN_SUCCESS.replace("player", PlayerStats.getName(uuid)));

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.hasPermission("bungee.notify")) {
                if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                    all.sendMessage(Data.pr + "§c " + p.getName() + "§7 hat §e" + PlayerStats.getName(uuid) + "§7 abgeändert!");
                    all.sendMessage(Message.EDITBAN_NOTIFY.replace("admin", p.getName()).replace("player", PlayerStats.getName(uuid)).getContent());
                }
            }
        }


    }
}
