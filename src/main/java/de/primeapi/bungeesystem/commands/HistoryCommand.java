package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.mysql.LogStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class HistoryCommand extends Command {

    public HistoryCommand() {
        super("history");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }

        ProxiedPlayer p = (ProxiedPlayer) commandSender;

        if (!p.hasPermission("bungee.hisotry")) {
            p.sendMessage(Data.noperm);
            return;
        }

        if (args.length == 0) {
            p.sendMessage(Data.pr + "§7Benutze: §e/hisotry [Spieler]");
            return;
        }

        if (!PlayerStats.exist(args[0])) {
            p.sendMessage(Data.pr + "§c Dieser Spieler exestiert nicht!");
            return;
        }

        String uuid = PlayerStats.getUUID(args[0]);

        if (LogStats.getLogReasons(uuid).size() == 0) {
            p.sendMessage(Data.pr + "§7Dieser Spieler hat keine Einträge!");
            return;
        }


        if (args.length > 1) {
            if (args[1].equalsIgnoreCase("clear")) {
                if (!p.hasPermission("bungee.hisotry.clear")) {
                    p.sendMessage(Data.noperm);
                    return;
                }

                LogStats.deleteAllLogs(uuid);
                p.sendMessage(Data.pr + "§7 Du hast die History von §e" + args[0] + "§c gelöscht§7!");
                LogStats.addLogReason(uuid, "", p.getUniqueId().toString(), "", String.valueOf(System.currentTimeMillis()), "clear");
                return;
            }
            int i = 0;
            try {
                i = Integer.valueOf(args[1]);
                if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("Ban")) {
                    p.sendMessage("§4§m--------[§r §6Banlog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §4§m]--------");
                    p.sendMessage("§7Bangrund§8: §e" + LogStats.getLogReasons(uuid).get(i));
                    if (PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
                        p.sendMessage("§7Gebannt von§8: §e" + LogStats.getLogAdmins(uuid).get(i));
                    } else {
                        p.sendMessage("§7Gebannt von§8: §e" + PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)));
                    }
                    p.sendMessage("§7Länge des Ban's§8: §e" + LogStats.getLogLongs(uuid).get(i));
                    p.sendMessage("§7Zeitpunkt des Ban's§8: §e" + TimeUtils.getDate(LogStats.getLogTimess(uuid).get(i)));
                    p.sendMessage("§4§m--------[§r §6Banlog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §4§m]--------");

                } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("Mute")) {
                    p.sendMessage("§c§m--------[§r §6Mutelog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §c§m]--------");
                    p.sendMessage("§7Mutegrund§8: §e" + LogStats.getLogReasons(uuid).get(i));
                    if (PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
                        p.sendMessage("§7Gemutet von§8: §e" + LogStats.getLogAdmins(uuid).get(i));
                    } else {
                        p.sendMessage("§7Gemutet von§8: §e" + PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)));
                    }
                    p.sendMessage("§7Länge des Mutes's§8: §e" + LogStats.getLogLongs(uuid).get(i));
                    p.sendMessage("§7Zeitpunkt des Mutes's§8: §e" + TimeUtils.getDate(LogStats.getLogTimess(uuid).get(i)));
                    p.sendMessage("§c§m--------[§r §6Mutelog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §c§m]--------");

                } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("unban")) {
                    p.sendMessage("§a§m--------[§r §6Unbanlog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §a§m]--------");
                    p.sendMessage("§7Grund des Unbans§8: §e" + LogStats.getLogReasons(uuid).get(i));
                    if (PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
                        p.sendMessage("§7Entbannt von§8: §e" + LogStats.getLogAdmins(uuid).get(i));
                    } else {
                        p.sendMessage("§7Entbannt von§8: §e" + PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)));
                    }
                    p.sendMessage("§7Zeitpunkt des Unbans's§8: §e" + TimeUtils.getDate(LogStats.getLogTimess(uuid).get(i)));
                    p.sendMessage("§a§m--------[§r §6Unbanlog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §a§m]--------");

                } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("unmute")) {
                    p.sendMessage("§a§m--------[§r §6Unmutelog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §a§m]--------");
                    p.sendMessage("§7Grund des Unbans§8: §e" + LogStats.getLogReasons(uuid).get(i));
                    if (PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
                        p.sendMessage("§7Entbannt von§8: §e" + LogStats.getLogAdmins(uuid).get(i));
                    } else {
                        p.sendMessage("§7Entbannt von§8: §e" + PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)));
                    }
                    p.sendMessage("§7Zeitpunkt des Unbans's§8: §e" + TimeUtils.getDate(LogStats.getLogTimess(uuid).get(i)));
                    p.sendMessage("§a§m--------[§r §6Unmutelog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §a§m]--------");

                } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("edit")) {
                    p.sendMessage("§2§m--------[§r §6Editbanlog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §2§m]--------");
                    p.sendMessage("§7Neuer Grund§8: §e" + LogStats.getLogReasons(uuid).get(i));
                    if (PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
                        p.sendMessage("§7Geändert von§8: §e" + LogStats.getLogAdmins(uuid).get(i));
                    } else {
                        p.sendMessage("§7Geändert von§8: §e" + PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)));
                    }
                    p.sendMessage("§7Länge des neuen Bans's§8: §e" + LogStats.getLogLongs(uuid).get(i));
                    p.sendMessage("§7Zeitpunkt der Veränderung's§8: §e" + TimeUtils.getDate(LogStats.getLogTimess(uuid).get(i)));
                    p.sendMessage("§2§m--------[§r §6Editbanlog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §2§m]--------");

                } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("clear")) {
                    p.sendMessage("§4§m--------[§r §4Historie clear von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §4§m]--------");
                    p.sendMessage("§cDie History wurde gecleart!");
                    if (PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
                        p.sendMessage("§7Gecleart von§8: §e" + LogStats.getLogAdmins(uuid).get(i));
                    } else {
                        p.sendMessage("§7Gecleart von§8: §e" + PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)));
                    }
                    p.sendMessage("§7Zeitpunkt des Clears's§8: §e" + TimeUtils.getDate(LogStats.getLogTimess(uuid).get(i)));
                    p.sendMessage("§4§m--------[§r §4Historie clear von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §4§m]--------");

                } else {
                    p.sendMessage("§9§m--------[§r §6Kicklog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §9§m]--------");
                    p.sendMessage("§7Kickgrund§8: §e" + LogStats.getLogReasons(uuid).get(i));
                    if (PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
                        p.sendMessage("§7Gekickt von§8: §e" + LogStats.getLogAdmins(uuid).get(i));
                    } else {
                        p.sendMessage("§7Gekickt von§8: §e" + PlayerStats.getName(LogStats.getLogAdmins(uuid).get(i)));
                    }
                    p.sendMessage("§7Zeitpunkt des Kicks's§8: §e" + TimeUtils.getDate(LogStats.getLogTimess(uuid).get(i)));
                    p.sendMessage("§9§m--------[§r §6Kicklog von " + PlayerStats.getName(uuid) + " §8| §6Eintrag " + (i + 1) + " §9§m]--------");
                }

                TextComponent list = new TextComponent("§7§ohier");
                list.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cSehe alle Einträge").create()));
                list.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0]));
                p.sendMessage(new TextComponent(Data.pr + "§7 Klicke "), list, new TextComponent("§7 um die gesammte Liste zu sehen"));
                return;
            } catch (Exception ex) {
                p.sendMessage(Data.pr + "§4 Fehler!");
            }
        }

        int i = 0;

        for (String reason : LogStats.getLogReasons(uuid)) {
            if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("Ban")) {

                TextComponent info = new TextComponent("§8[§6Infos§8]");
                info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cZeige Weitere Informationen zu diesem Eintrag").create()));
                info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0] + " " + i));
                p.sendMessage(new TextComponent(Data.pr + " §4➤ Ban §8| §e" + reason + " "), info);


            } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("Mute")) {

                TextComponent info = new TextComponent("§8[§6Infos§8]");
                info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cZeige Weitere Informationen zu diesem Eintrag").create()));
                info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0] + " " + i));
                p.sendMessage(new TextComponent(Data.pr + " §c➤ Mute §8| §e" + reason + " "), info);


            } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("unban")) {

                TextComponent info = new TextComponent("§8[§6Infos§8]");
                info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cZeige Weitere Informationen zu diesem Eintrag").create()));
                info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0] + " " + i));
                p.sendMessage(new TextComponent(Data.pr + " §a➤ UnBan §8| §e" + reason + " "), info);


            } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("unmute")) {

                TextComponent info = new TextComponent("§8[§6Infos§8]");
                info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cZeige Weitere Informationen zu diesem Eintrag").create()));
                info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0] + " " + i));
                p.sendMessage(new TextComponent(Data.pr + " §a➤ UnMute §8| §e" + reason + " "), info);


            } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("edit")) {

                TextComponent info = new TextComponent("§8[§6Infos§8]");
                info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cZeige Weitere Informationen zu diesem Eintrag").create()));
                info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0] + " " + i));
                p.sendMessage(new TextComponent(Data.pr + " §2➤ Edit §8| §e" + reason + " "), info);


            } else if (LogStats.getLogTypes(uuid).get(i).equalsIgnoreCase("Clear")) {

                TextComponent info = new TextComponent("§8[§6Infos§8]");
                info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cZeige Weitere Informationen zu diesem Eintrag").create()));
                info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0] + " " + i));
                p.sendMessage(new TextComponent(Data.pr + " §4➤ Clear "), info);


            } else {

                TextComponent info = new TextComponent("§8[§6Infos§8]");
                info.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cZeige Weitere Informationen zu diesem Eintrag").create()));
                info.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/history " + args[0] + " " + i));
                p.sendMessage(new TextComponent(Data.pr + " §9➤ Kick §8| §e" + reason + " "), info);


            }
            i++;
        }

//        for(String reason : LogStats.getLogReasons(UUID)) {
//        	
//        	if(LogStats.getLogTypes(UUID).get(i).equalsIgnoreCase("Ban")) {
//            	p.sendMessage("§4§m--------[§r §6Banlog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §4§m]--------");
//            	p.sendMessage("§7Bangrund§8: §e"+LogStats.getLogReasons(UUID).get(i));
//            	if(PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
//            		p.sendMessage("§7Gebannt von§8: §e"+LogStats.getLogAdmins(UUID).get(i));
//            	}else {
//            		p.sendMessage("§7Gebannt von§8: §e"+PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)));
//            	}
//            	p.sendMessage("§7Länge des Ban's§8: §e"+LogStats.getLogLongs(UUID).get(i));
//            	p.sendMessage("§7Zeitpunkt des Ban's§8: §e"+TimeUtils.getDate(LogStats.getLogTimess(UUID).get(i)));
//            	p.sendMessage("§4§m--------[§r §6Banlog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §4§m]--------");
//            	
//        	}else if(LogStats.getLogTypes(UUID).get(i).equalsIgnoreCase("Mute")) {
//            	p.sendMessage("§c§m--------[§r §6Mutelog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §c§m]--------");
//            	p.sendMessage("§7Mutegrund§8: §e"+LogStats.getLogReasons(UUID).get(i));
//            	if(PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
//            		p.sendMessage("§7Gemutet von§8: §e"+LogStats.getLogAdmins(UUID).get(i));
//            	}else {
//            		p.sendMessage("§7Gemutet von§8: §e"+PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)));
//            	}
//            	p.sendMessage("§7Länge des Mutes's§8: §e"+LogStats.getLogLongs(UUID).get(i));
//            	p.sendMessage("§7Zeitpunkt des Mutes's§8: §e"+TimeUtils.getDate(LogStats.getLogTimess(UUID).get(i)));
//            	p.sendMessage("§c§m--------[§r §6Mutelog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §c§m]--------");
//            	
//        	}else if(LogStats.getLogTypes(UUID).get(i).equalsIgnoreCase("unban")) {
//            	p.sendMessage("§a§m--------[§r §6Unbanlog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §a§m]--------");
//            	p.sendMessage("§7Grund des Unbans§8: §e"+LogStats.getLogReasons(UUID).get(i));
//            	if(PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
//            		p.sendMessage("§7Entbannt von§8: §e"+LogStats.getLogAdmins(UUID).get(i));
//            	}else {
//            		p.sendMessage("§7Entbannt von§8: §e"+PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)));
//            	}
//            	p.sendMessage("§7Zeitpunkt des Unbans's§8: §e"+TimeUtils.getDate(LogStats.getLogTimess(UUID).get(i)));
//            	p.sendMessage("§a§m--------[§r §6Unbanlog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §a§m]--------");
//            	
//        	}else if(LogStats.getLogTypes(UUID).get(i).equalsIgnoreCase("unmute")) {
//            	p.sendMessage("§a§m--------[§r §6Unmutelog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §a§m]--------");
//            	p.sendMessage("§7Grund des Unbans§8: §e"+LogStats.getLogReasons(UUID).get(i));
//            	if(PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
//            		p.sendMessage("§7Entbannt von§8: §e"+LogStats.getLogAdmins(UUID).get(i));
//            	}else {
//            		p.sendMessage("§7Entbannt von§8: §e"+PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)));
//            	}
//            	p.sendMessage("§7Zeitpunkt des Unbans's§8: §e"+TimeUtils.getDate(LogStats.getLogTimess(UUID).get(i)));
//            	p.sendMessage("§a§m--------[§r §6Unmutelog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §a§m]--------");
//            	
//        	}else if(LogStats.getLogTypes(UUID).get(i).equalsIgnoreCase("edit")) {
//            	p.sendMessage("§2§m--------[§r §6Editbanlog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §2§m]--------");
//            	p.sendMessage("§7Neuer Grund§8: §e"+LogStats.getLogReasons(UUID).get(i));
//            	if(PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
//            		p.sendMessage("§7Geändert von§8: §e"+LogStats.getLogAdmins(UUID).get(i));
//            	}else {
//            		p.sendMessage("§7Geändert von§8: §e"+PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)));
//            	}
//            	p.sendMessage("§7Länge des neuen Bans's§8: §e"+LogStats.getLogLongs(UUID).get(i));
//            	p.sendMessage("§7Zeitpunkt der Veränderung's§8: §e"+TimeUtils.getDate(LogStats.getLogTimess(UUID).get(i)));
//            	p.sendMessage("§2§m--------[§r §6Editbanlog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §2§m]--------");
//            	
//        	}else if(LogStats.getLogTypes(UUID).get(i).equalsIgnoreCase("clear")) {
//            	p.sendMessage("§4§m--------[§r §4Historie clear von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §4§m]--------");
//            	p.sendMessage("§cDie History wurde gecleart!");
//            	if(PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
//            		p.sendMessage("§7Gecleart von§8: §e"+LogStats.getLogAdmins(UUID).get(i));
//            	}else {
//            		p.sendMessage("§7Gecleart von§8: §e"+PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)));
//            	}
//            	p.sendMessage("§7Zeitpunkt des Clears's§8: §e"+TimeUtils.getDate(LogStats.getLogTimess(UUID).get(i)));
//            	p.sendMessage("§4§m--------[§r §4Historie clear von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §4§m]--------");
//            	
//        	}else {
//            	p.sendMessage("§9§m--------[§r §6Kicklog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §9§m]--------");
//            	p.sendMessage("§7Kickgrund§8: §e"+LogStats.getLogReasons(UUID).get(i));
//            	if(PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)).equalsIgnoreCase("ErrorExeption0043")) {
//            		p.sendMessage("§7Gekickt von§8: §e"+LogStats.getLogAdmins(UUID).get(i));
//            	}else {
//            		p.sendMessage("§7Gekickt von§8: §e"+PlayerStats.getName(LogStats.getLogAdmins(UUID).get(i)));
//            	}
//            	p.sendMessage("§7Zeitpunkt des Kicks's§8: §e"+TimeUtils.getDate(LogStats.getLogTimess(UUID).get(i)));
//            	p.sendMessage("§9§m--------[§r §6Kicklog von "+PlayerStats.getName(UUID)+ " §8| §6Eintrag "+(i+1)+" §9§m]--------");
//        	}
//        	i++;
//        }
        p.sendMessage("");
        p.sendMessage(Data.pr + "§7 Es sind §e" + LogStats.getLogReasons(uuid).size() + "§7 Einträge vorhabden");

    }
}
