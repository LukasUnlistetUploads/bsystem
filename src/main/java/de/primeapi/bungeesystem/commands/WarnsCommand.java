package de.primeapi.bungeesystem.commands;


import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.mysql.WarnStats;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class WarnsCommand extends Command {

    public WarnsCommand() {
        super("warns");
    }

    @SuppressWarnings("deprecation")
    public void execute(CommandSender sender, String[] args) {

        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);


        if (!p.hasPermission("Bungee.warns") || args.length == 0) {
            String uuid = p.getUUID().toString();
            p.sendMessage(Message.WARNS_LIST_HEADER.replace("player", p.getName()));
            int i = 0;
            for (String reason : WarnStats.getWarnReasons(uuid)) {
                p.sendMessage(Message.WARNS_LIST_COUNT.replace("count", (i + 1)));
                p.sendMessage(Message.WARNS_LIST_REASON.replace("reason", WarnStats.getWarnReasons(uuid).get(i)));
                p.sendMessage(Message.WARNS_LIST_TIME.replace("time", TimeUtils.getDate(WarnStats.getWarnLongs(uuid).get(i))));
                p.getProxiedPlayer().sendMessage("");
                i++;
            }
            p.sendMessage(Message.WARNS_LIST_FINALCOUNT.replace("count", WarnStats.getWarnReasons(uuid).size()));
        } else {

            String UUID = PlayerStats.getUUID(args[0]);

            if (args.length > 1) {
                if (args[1].equalsIgnoreCase("clear")) {
                    if (!p.hasPermission("bungee.warns.clear")) {
                        p.sendNoPerm();
                        return;
                    }

                    WarnStats.deleteAllLogs(UUID);
                    p.sendMessage(Message.WARNS_DELETE_SUCCESS.replace("player", args[0]));
                    return;
                }
            }


            if (!PlayerStats.exist(args[0])) {
                p.sendMessage(Message.WARNS_NOTFOUND);
                return;
            }
            p.sendMessage(Message.WARNS_LIST_HEADER.replace("player", p.getName()));
            int i = 0;
            for (String reason : WarnStats.getWarnReasons(UUID)) {
                p.sendMessage(Message.WARNS_LIST_COUNT.replace("count", (i + 1)));
                p.sendMessage(Message.WARNS_LIST_REASON.replace("reason", WarnStats.getWarnReasons(UUID).get(i)));
                if (PlayerStats.getName(WarnStats.getWarnAdmins(UUID).get(i)).equalsIgnoreCase("fehler")) {
                    p.sendMessage(Message.WARNS_LIST_TEAM.replace("team", WarnStats.getWarnAdmins(UUID).get(i)));
                } else {
                    p.sendMessage(Message.WARNS_LIST_TEAM.replace("team", PlayerStats.getName(WarnStats.getWarnAdmins(UUID).get(i))));
                }

                p.sendMessage(Message.WARNS_LIST_TIME.replace("time", TimeUtils.getDate(WarnStats.getWarnLongs(UUID).get(i))));
                p.getProxiedPlayer().sendMessage("");
                i++;
            }
            p.sendMessage(Message.WARNS_LIST_FINALCOUNT.replace("count", WarnStats.getWarnReasons(UUID).size()));

        }


    }
}
