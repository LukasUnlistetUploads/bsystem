package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BCCommand extends Command {

    public BCCommand(String name) {
        super("bc");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {


            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.bc")) {
            p.sendNoPerm();
            return;
        }

        if (args.length == 0) {
            p.sendMessage(Message.BRODCAST_USAGE);
            return;
        }

        String msg = "§b ";
        for (int i = 0; i <= args.length - 1; i++) {
            msg += args[i] + " ";
        }


        msg = msg.replace("&", "§");
        for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
            all.getProxiedPlayer().sendMessage(msg);
        }

    }

}
