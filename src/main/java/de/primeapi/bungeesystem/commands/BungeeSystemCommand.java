package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.managers.messages.MessageManager;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;

public class BungeeSystemCommand extends Command {
    public BungeeSystemCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {

        if(args.length == 0) {
            commandSender.sendMessage(" §7BungeeSystem v" + BungeeSystem.getInstance().getDescription().getVersion() + " published by PrimeAPI");
            commandSender.sendMessage(" §7https://www.primeapi.de");
        }else {
            switch (args[0].toLowerCase()){
                case "rlmessages":
                    if(commandSender.hasPermission("bungee.admin")){
                        commandSender.sendMessage(Message.NO_PERMS.getContent());
                        return;
                    }
                    try {
                        BungeeSystem.messageManager = new MessageManager();
                        commandSender.sendMessage("§aAlle nachrichten wurde neu geladen!");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}
