package de.primeapi.bungeesystem.commands;

import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SendCommand extends Command {

    public SendCommand(String name) {
        super("forcejoin");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            return;
        }

        PrimePlayer p = PrimeAPI.getPrimePlayer(sender);

        if (!p.hasPermission("bungee.forcejoin")) {
            p.sendNoPerm();
            return;
        }

        if (args.length != 2) {
            p.sendMessage(Message.SEND_USAGE);
            return;
        }

        try {
            ServerInfo info = ProxyServer.getInstance().getServerInfo(args[1]);


            if (PrimeAPI.isOnline(args[0])) {
                PrimePlayer t = PrimeAPI.getPrimePlayer(args[0]);
                t.getPp().connect(info);
                p.sendMessage(Message.SEND_NOTIFY.replace("server", info.getName()));
            } else if (args[0].equalsIgnoreCase("all")) {
                for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
                    if (all.getPp().getServer().getInfo().equals(info)) {
                        p.sendMessage(Message.SEND_ALREADY);
                    } else {
                        all.getPp().connect(info);
                        p.sendMessage(Message.SEND_NOTIFY.replace("server", info.getName()));
                    }
                }
            } else if (args[0].equalsIgnoreCase("current")) {
                for (PrimePlayer all : PrimeAPI.getOnlinePrimePlayers()) {
                    if (all.getPp().getServer().getInfo().equals(p.getPp().getServer().getInfo())) {
                        if (all.getPp().getServer().getInfo().equals(info)) {
                            p.sendMessage(Message.SEND_ALREADY);
                        } else {
                            all.getPp().connect(info);
                            p.sendMessage(Message.SEND_NOTIFY.replace("server", info.getName()));
                        }
                    }
                }
            }

        } catch (Exception ex) {
            p.sendMessage(Message.SEND_OFFLINE);
            return;
        }

    }

}
