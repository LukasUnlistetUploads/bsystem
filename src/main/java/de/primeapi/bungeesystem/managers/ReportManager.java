package de.primeapi.bungeesystem.managers;

import java.util.HashMap;

public class ReportManager {



    public static class Report {

        String reporter;
        String reported;
        String reason;
        Long time;

        public Report(String reporter, String reported, String reason) {
            this.reporter = reporter;
            this.reported = reported;
            this.reason = reason;
            time = System.currentTimeMillis();
        }

        public Report(String reporter, String reported, String reason, String time) {
            this.reporter = reporter;
            this.reported = reported;
            this.reason = reason;
            this.time = Long.parseLong(time);
        }

        public String getReporter() {
            return reporter;
        }

        public String getReported() {
            return reported;
        }

        public String getReason() {
            return reason;
        }

        public Long getTime() {
            return time;
        }
    }


}
