package de.primeapi.bungeesystem.managers.messages;

import com.sun.org.apache.xpath.internal.operations.Bool;
import de.primeapi.bungeesystem.commands.FakePlayersCommand;
import de.primeapi.bungeesystem.managers.CoinsAPI;
import de.primeapi.bungeesystem.mysql.MuteStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.mysql.WarnStats;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.TimeUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;

import java.util.Objects;

@AllArgsConstructor
@Getter
public enum Message {
    NO_PERMS("noperms", "§c Du hast dazu keine Berechtigungen!", true),

    ABAN_USAGE("aban.usage", "§7 Benutze: §e/aban [Spieler] [(Zahl)(m/h/d)/P] [Grund...]", true),
    ABAN_NOTFOUND("aban.notfound", "§c Dieser Spieler war noch nie auf dem Netzwerk!", true),
    ABAN_NO_NUMBER("aban.nonumber", "§e %time% §c ist keine Zahl", true),
    ABAN_NO_VALIDNUMBER("aban.novalidtime", "§7 Benutze als 2. Argument einen Zeitstrahl + M,H oder D drangehanagen", true),
    ABAN_SUCCESS("aban.success", "§7 Du hast §aerfolgreich §e%player% §7für §e%reason% §cgebannt§7!", true),
    ABAN_SUCCESS_NOTIFY("aban.notify", "§6 %admin% §7hat &e%player% §7wegen §e%reason% §cgebannt§7!", true),

    AFK_NONAFK("afk.deactivate", "§a Du bist nun nichtmehr als Abwesend makiert!", true),
    AFK_AFK("afk.activate", "§c Du bist nun als Abwesend makiert!", true),

    AMUTE_USAGE("amute.usage", "§7 Benutze: §e/amute [Spieler] [(Zahl)(m/h/d)/P] [Grund...]", true),
    AMUTE_NOTFOUND("amute.notfound", "§c Dieser Spieler war noch nie auf dem Netzwerk!", true),
    AMUTE_NO_NUMBER("amute.nonumber", "§e %time% §c ist keine Zahl", true),
    AMUTE_NO_VALIDNUMBER("amute.novalidtime", "§7 Benutze als 2. Argument einen Zeitstrahl + M,H oder D drangehanagen", true),
    AMUTE_SUCCESS("amute.success", "§7 Du hast §aerfolgreich §e%player% §7für §e%reason% §cgemuted§7!", true),
    AMUTE_SUCCESS_NOTIFY("amute.notify", "§6 %admin% §7hat &e%player% §7wegen §e%reason% §cgemuted§7!", true),
    AMUTE_SUCCESS_NOTIFYPLAYER("amute.notifyplayer", "§c Du wurdest für §e%reason% §4gemuted§c!\n §cZeit: §e%time%", true),


    BAN_USAGE1("ban.usage.1", "§e Mögliche Banngründe:", true),
    BAN_USAGE2("ban.usage.2", "§7 Benutze: §e/ban [Spieler] [ID] <Zusatz>", true),
    BAN_NOTFOUND("ban.notfound", "§c Dieser Spieler exestiert nicht!", true),
    BAN_NOPERMS("ban.noperms",  "§c Du hast keine Rechte für diesen Grund", true),
    BAN_BAN_SUCCESS("ban.ban.success", "§7 Du hast §aerfolgreich §e%player% §7für §e%reason% §cgebannt§7!", true),
    BAN_BAN_SUCCESS_NOTIFY("ban.bannotify", "§6 %admin% §7hat &e%player% §7wegen §e%reason% §cgebannt§7!", true),
    BAN_MUTE_SUCCESS("ban.mute.success", "§7 Du hast §aerfolgreich §e%player% §7für §e%reason% §cgemuted§7!", true),
    BAN_MUTE_SUCCESS_NOTIFY("ban.mute.notify", "§6 %admin% §7hat &e%player% §7wegen §e%reason% §cgemuted§7!", true),
    BAN_MUTE_SUCCESS_NOTIFYPLAYER("ban.mute.notifyplayer", "§c Du wurdest für §e%reason%& §4gemuted§c!\n §cZeit: §e%time%", true),


    BANLIST_NOBANS("banlist.nobans", "§c Es sind keine Spieler gebannt!", true),
    BANLIST_MESSAGE("banlist.message", "§7 Liste aller gebannten Spieler:", true),

    BAUSERVER_ERROR("bauserver.error", "§4 Server wurde nicht gefunden!", false),

    BRODCAST_USAGE("broadcast.usage", "§7 Benutze: §e/bc [Text...]", true),

    CLEARCHAT_USAGE("clearchat.usage", "§7 Benutze: §e/cc [local|global]", true),
    CLEARCHAT_SUCCESS("clearchat.success", "§7 Der Chat wurde geleert!", true),
    CLEARCHAT_NOTIFY_LOCAL("clearchat.notify.local", "§7 Der lokale Chat auf §e%server%§7 wurde von §6%name%§7 geleert!", true),
    CLEARCHAT_NOTIFY_GLOVAL("clearchat.notify.global", "§7 Der globale Chat wurde von §6%name%§7 geleert!", true),

    CHATMUTE_USAGE("chatmute.usage", "§7 Benutze: §e/chatmute [local|global]", true),
    CHATMUTE_SUCESS_LOCAL_DEACTIVATE("chatmute.sucess.local.activate", "§7 Der §eChat §7wurde auf deinem Server §cdeaktiviert§7!", true),
    CHATMUTE_SUCESS_LOCAL_ACTIVATE("chatmute.sucess.local.deactivate", "§7 Der §eChat §7wurde auf deinem Server wieder §aaktiviert§7!", true),
    CHATMUTE_SUCESS_GLOBAL_DEACTIVATE("chatmute.sucess.global.activate", "§7 Der §eChat §7wurde §cdeaktiviert§7!", true),
    CHATMUTE_SUCESS_GLOBAL_ACTIVATE("chatmute.sucess.global.deactivate", "§7 Der §eChat §7wurde wieder §aaktiviert§7!", true),

    COINS_BALANCE("coins.balace", "§7 Du hast §e%amount% Coins§7!", true),

    COINS_USAGE1("coins.usage1", "§7 Benutze: §e/coins pay [Spieler] [Coins]", true),
    COINS_USAGE2("coins.usage2", "§7 Benutze: §e/coins set [Spieler] [Anzahl]", true),
    COINS_USAGE3("coins.usage3", "§7 Benutze: §e/coins add [Spieler] [Anzahl]", true),
    COINS_USAGE4("coins.usage4", "§7 Benutze: §e/coins remove [Spieler] [Anzahl]", true),
    COINS_USAGE5("coins.usage5", "§7 Benutze: §e/coins see [Spieler]", true),

    COINS_NONUMBER("coins.nonumber", "§e %value%§7 ist keine Zahl.", true),
    COINS_MINVALUE("coins.minvalue", "§c Du musst eine Zahl über 1 angeben!", true),
    COINS_NOTFOUND("coins.NOTFOUND", "§7 Dieser Spieler exestiert nicht.", true),
    COINS_NOTIFY("coins.notify", "§6 %name%§7 hat dir §e%amount%§7 Coins gegeben!", true),
    COINS_SUCCESS("coins.success", "§7 Du hast §e%player% §e%coins%§7 Coins gegeben!", true),
    COINS_INFO("coins.info", "§7 Der Spieler §6%player%§7 hat §e %coins% Coins", true),
    COINS_SUCCESS1("coins.success2", "§a Erfolreich!", true),

    EDITBAN_USAGE("editban.usage", "§7 Benutze: §e/editban [Spieler] [Zeit|(d/h/m)] [Neuer Grund]", true),
    EDITBAN_NOTFOUND("editban.notfound", "§7 Dieser Spieler wurde nicht gefunden", true),
    EDITBAN_NOBANNEND("editban.notbannend", "§7 Dieser Spieler ist nicht gebannt", true),
    EDITBAN_NO_NUMBER("editban.nonumber", "§e %time% §c ist keine Zahl", true),
    EDITBAN_NO_VALIDNUMBER("editban.novalidtime", "§7 Benutze als 2. Argument einen Zeitstrahl + M,H oder D drangehanagen", true),
    EDITBAN_SUCCESS("editban.success", "§7 Du hast den Ban von §e%player%§7 erfolgreich abgeändert!", true),
    EDITBAN_NOTIFY("editban.notify", "§c %admin%§7 hat §e%player%§7 abgeändert!", true),

    END_USAGE("end.usage", "§7 Benutze: §e/end [Now/Stop|Time in seconds]", true),
    END_KICKMESSAGE("end.kickmessage", "§c Netzwerk startet neu!", false),
    END_ABORD("end.abord", "§a§lWARNUNG§8: §7 Der Neustart wurde abgebrochen!", false),
    END_COUNT("end.count", "§c§lWARNUNG§8: §7 Der Server restartet in §e%count% Sekunden§7!", false),

    FAKEPLAYERS_USAGE("fakeplayrts.usage", "§7 Benutze: §e/fakeplayers [Anzahl]", true),
    FAKEPLAYERS_COUNT("fakepalyers.count", "§7 Atkuelle FakePlayers: §e%count%", true),
    FAKEPLAYERS_NO_NUMBER("fakepalyers.nonumber", "§c Gib eine gültige Zahl an!", true),
    FAKEPLAYERS_SUCCESS("fakepalyers.success", "§7 Du haste die §eFakeplayers §aerfolgreich §7geändert!", true),

    FOLLOW_USAGE("follow.usage", "§7 Benutze: §e/follow [Spieler]", true),
    FOLLOW_DEACTIVATE("follow.deactivate", "§7 Du §cfolgst §7nun nichtmehr §e%player%", true),
    FOLLOW_ACTIVATE("follow.activate", "§7 Du §cfolgst §7nun §e%player%", true),
    FOLLOW_NOTFOUND("follow.notfound", "§c Der Spieler ist nicht online!", true),
    FOLLOW_NOTIFY("follow.notfound", "§7 Du wurdest auf den Server von §e%player%§7 verschoben. Um dies nichtmehr zu machen tippe §b/follow %player%", true),


    FRIEND_NOT_FOUND("friend.notfound", "§c Dieser Spieler wurde nicht gefunden.", true),
    FRIEND_SELF("friend.self", "§c Du kannst diesen Befehl nicht mit dir selbst ausführen", true),
    FRIEND_ADD_SELF("friend.add.self", "§c Du kannst dir selbst keine Anfrage senden.", true),
    FRIEND_ADD_ALREADY_FRIEND("friend.already", "§c Du bist bereits mit §e%player% §cbefreundet.", true),
    FRIEND_ADD_ALREADY_REQUEST("friend.add.success", "§c Du hast bereits eine Anfrage an §e%player% §cgesendet.", true),
    FRIEND_ADD_ALREADY_GOT("§c Du hast bereits eine Anfrage von §e%player% §c.", true),
    FRIEND_ADD_CANTSEND("§c Du kannst diesem Spieler keine Anfrage senden.", true),
    FRIEND_ADD_SUCCESS("§7 Du hast §e%player% §7eine Freundschaftsanfrage gesendet.", true),

    FRIEND_REMOVE_NOFRIENDS("§c Du bist nicht mit §e%player% §cbefreundet.", true),
    FRIEND_REMOVE_SUCCESS("§c Die Freundschaft mit §e%player% §cwurde aufgelöst.", true),
    FRIEND_REMOVE_NOTIFY("§c Die Freundschaft mit §e%player% §cwurde aufgelöst.", true),

    FRIEND_JUMP_NOFRRIENDS("§c Du bist nicht mit §e%player% §cbefreundet.", true),
    FRIEND_JUMP_SUCCESS("§7 Du bist nun auf §e%server%", true),
    FRIEND_JUMP_NOT_ONLINE("§c Dein Freund §e%player% §c ist nicht online!", true),
    FRIEND_JUMP_DENY("§c Du darfst §e%player% §cnicht nachspringen.", true),

    FRIEND_ACCEPT_NOREQUEST("§c Du hast keine Anfrage von §e%player% §c.", true),
    FRIEND_ACCEPT_SUCCESS("§7 Du bist nun mit §e%player% §7befreundet.", true),
    FRIEND_ACCEPT_NOTIFY("§7 Du bist nun mit §e%player% §7befreundet.", true),

    FRIEND_DENY_NOREQUEST("§c Du hast keine Anfrage von §e%player% §c.", true),
    FRIEND_DENY_SUCCESS("§7 Du hast die Anfrage von §e%player% §7abgelehnt.", true),
    FRIEND_DENY_NOTIFY("§e %player% &7hat deine Anfrage abgelehnt!", true),

    FRIEND_LIST_NONUMBER("§c Du musst eine Zahl angeben.", true),
    FRIEND_LIST_NOFRIENDS("§c Du hast keine Freunde.", true),

    FRIEND_REQUESTS_NONUMBER("§c Du musst eine Zahl angeben.", true),
    FRIEND_REQUESTS_NOREQUESTS("§c Du hast keine Anfragen.", true),

    FRIEND_HELP_01("§7§m --------------[§e Freunde §b<1> §7§m]--------------", true),
    FRIEND_HELP_02("§e /friend add <Name> §8- §7Versende eine Anfrage", true),
    FRIEND_HELP_03("§e /friend remove <Name> §8- §7Entferne einen Freund", true),
    FRIEND_HELP_04("§e /friend accept <Name> §8- §7Nehme eine Anfrage an", true),
    FRIEND_HELP_05("§e /friend deny <Name> §8- §7Lehne eine Anfrage ab", true),
    FRIEND_HELP_06("§e /friend join <Name> §8- §7Joine einem Freund nach", true),
    FRIEND_HELP_07("§e /friend list §8- §7Liste deine Freunde auf", true),
    FRIEND_HELP_08("§e /friend requests §8- §7Liste deine Anfragen auf", true),
    FRIEND_HELP_09("§e /friend toggle §8- §7Anfragen an/aus", true),
    FRIEND_HELP_10("§e /friend togglenotify §8- §7Online/Offline Nachrichten", true),
    FRIEND_HELP_11("§e /friend togglemessage §8- §7Private Nachrichten an/aus", true),
    FRIEND_HELP_12("§e /friend togglejump §8- §7Nachspringen an/aus", true),
    FRIEND_HELP_13("§7§m --------------[§e Freunde §b<1> §7§m]--------------", true),

    FRIEND_TOGGLE_OFF("§7 Du hast deine §cFreundschaftsanfragen §7deaktiviert.", true),
    FRIEND_TOGGLE_ON("§7 Du hast deine §aFreundschaftsanfragen §7aktiviert.", true),

    FRIEND_TOGGLEMESSAGE_1("§7 Du erhälst nun §ckeine privaten Nachrichten §7mehr.", true),
    FRIEND_TOGGLEMESSAGE_2("§D u kannst nur noch §6private Nachrichten §7von §eFreunden §7erhalten.", true),
    FRIEND_TOGGLEMESSAGE_3("§7 Du kannst nun §awieder private Nachrichten §7erhalten.", true),

    FRIEND_TOGGLEJUMP_OFF( "§7 Dir kann nun §cniemand §7mehr §cnachspringen§7.", true),
    FRIEND_TOGGLEJUMP_ON("§7 Deine §eFreunde §7können dir nun §awieder nachspringen§7.", true),

    FRIEND_NOTIFY_OFF("§7 Du erhälst nun §ckeine Join/Quit-Nachrichten §7mehr.", true),
    FRIEND_NOTIFY_ON("§7 Du erhälst nun §awieder Join/Quit-Nachrichten§7.", true),

    FRIEND_NOTIFYS_ONLINE("§7 Dein Freund %player% §7ist nun §aonline!", true),
    FRIEND_NOTIFYS_OFFLINE("§7 Dein Freund %player% §7ist nun §coffline!", true),

    GETIP_USAGE("§7 Benutze: §e/GetIP [Spieler]", true),
    GETIP_NOTFOUND("§7 Dieser Spieler wurde nicht im System gefunden!", true),
    GETIP_MESSAGE("§7 Die IP-Adresse von §e%player%§7 lautet: §e%adress%", true),
    GETIP_INFO("§c Das veröffentlichen dieser IP ist verboten!", true),

    JOINME_WAIT("§c Du musst noch §e%time%§c warten vor deinem nächten JoinMe!", true),
    JOINME_LOBBY("§c Du darfst diesen Command hier nicht ausführen!", true),

    JOINTO_USAGE("§7 Benutze: §e/JoinTo [Spieler]", true),
    JOINTO_NOTFOUND("§c Dieser Spieler ist nicht online", true),
    JOINTO_ALREADY("§c Du befindest dich schon auf dem Server von §e%player", true),
    JOINTO_SUCCESS("§7 Du wurdest §aerfolgreich §7verschoben!", true),

    JUMPTO_USAGE("§7 Benutze: §e/JumpTo [Server]", true),
    JUMPTO_SUCCESS("§7 Du wurdest §aerfolgreich §7auf §e%server%§7 verschoben", true),
    JUMPTO_ERROR("§7 Dieser Server ist §coffline§7!", true),

    KICK_USAGE("§7 Benutze: §e/kick [Spieler] [Grund...]", true),
    KICK_NOTFOUND("§c Dieser Spieler ist nicht online!", true),
    KICK_NOTIFY("§c%admin%§7 hat §e%player%§7 wegen §e%reason%§7 gekickt!", true),
    KICK_SUCCESS("§7 Du hast §e%player%§7 erfolgreich wegen §6%reason%§7 gekickt!", true),

    LOBBY_ALREADY("§c Du bist bereits auf der Lobby!", true),
    LOBBY_ERROR("§c Es ist ein fehler aufgetreten! §%lobby%§c existiert nicht!", true),

    LOGIN_USAGE("§7 Benutze: §e/login [in|out] [all|tc|notify|report|cloud]", true),
    LOGIN_REPORT_ON("§7 Report: §a Eingeloggt", true),
    LOGIN_REPORT_OFF("§7 Report: §c Ausgeloggt", true),
    LOGIN_TEAMCHAT_ON("§7 TeamChat: §a Eingeloggt", true),
    LOGIN_TEAMCHAT_OFF("§7 TeamChat: §c Ausgeloggt", true),
    LOGIN_NOTIFY_ON("§7 Notify: §a Eingeloggt", true),
    LOGIN_NOTIFY_OFF("§7 Notify: §c Ausgeloggt", true),
    LOGIN_LOGIN_ALL_SUCCESS("§7 Du hast dich in alle Systeme eingeloggt!", true),
    LOGIN_LOGIN_REPORT_SUCCESS("§7 Du hast dich in das Report System eingeloggt!", true),
    LOGIN_LOGIN_REPORT_NOTIFY("§6 %player%§7 hat sich in das §4Report§7-§eSystem §aeingeloggt", true),
    LOGIN_LOGIN_TEAMCHAT_SUCCESS("§7 Du hast dich in das TeamChat System eingeloggt!", true),
    LOGIN_LOGIN_TEAMCHATT_NOTIFY("§6 %player%§7 hat sich in das §9TeamChat§7-§eSystem §aeingeloggt", true),
    LOGIN_LOGIN_NOTIFY_SUCCESS("§7 Du hast dich in das Notify System eingeloggt!", true),

    LOGIN_LOGOUT_ALL_SUCCESS("§7 Du hast dich aus allen Systeme ausgeloggt!", true),
    LOGIN_LOGOUT_REPORT_SUCCESS("§7 Du hast dich aus dem Report System ausgeloggt!", true),
    LOGIN_LOGOUT_REPORT_NOTIFY("§6 %player%§7 hat sich aus dem §4Report§7-§eSystem §ausgeloggt", true),
    LOGIN_LOGOUT_TEAMCHAT_SUCCESS("§7 Du hast dich aus dem TeamChat System ausgeloggt!", true),
    LOGIN_LOGOUT_TEAMCHATT_NOTIFY("§6 %player%§7 aus dem in das §9TeamChat§7-§eSystem §aausgeloggt", true),
    LOGIN_LOGOUT_NOTIFY_SUCCESS("§7 Du hast dich aus dem Notify System ausgeloggt!", true),

    MOTD_USAGE("§7 Benutze: §e/Motd <Wartung/normal> <1/2> [Motd...]", true),
    MOTD_SUCCESS("§a Motd erfolgreich aktuellisiert!", true),

    MSG_NOTFOUND("§c Dieser Spieler ist nicht online.", true),
    MSG_SELF("§c Du kannst nicht mit dir selbst interagieren.", true),
    MSG_DEACTIVATED("§c Dieser Spieler hat §eprivate Nachrichten §cdeaktiviert.", true),
    MSG_AFK("§c Der Spieler ist aktuell abwesend!", true),
    MSG_USAGE("§7 Benutze§8: §e/msg <Name> <Nachricht>", true),
    MSG_REPLY_USAGE("§7Benutze§8: §e/r <Nachricht>", true),

    ONLINE_ALL("§7 Spieler insgesammt online: §e%count%", true),
    ONLINE_CURRENT("§7 Spieler auf deinem Server: §e%count%", true),
    ONLINE_TEAM("§7 Teammitglieder online: §e%count%", true),

    ONTIME("§7 Deine Spielzeit: §e%count%", true),
    PING("§7 Dein Ping beträgt§8: §e%ping%ms", true),

    REPORT_NOT_FOUND("§c Dieser Spieler ist nich online!", true),
    REPORT_SELF("§c Du kannst dich nicht selbst reporten", true),
    REPORT_ALREADY("§c Du hast diesen Spieler bereits Reported!", true),
    REPORT_NOTIFY("§c Report§8: §e%player%§3 » §6%reason%", true),
    REPORT_SUCCESS("§7 Du hast §6%player%§7 erfolgreich wegen §e%reason%§7 reportet!", true),
    REPORT_USAGE("§7 Benutze: §e/Report [Spieler] [Grund]", true),
    REPORT_REASONS("§7 Mögliche Gründe: %reasons%", true),

    REPORTS_LOGIN("§c Logge dich zunächst in das Report System ein!", true),
    REPORTS_NOREPORTS("§7 Es sind aktuell keine Reports offen!", true),
    REPORTS_LIST_CAPTION("§7 Liste der Reports: ", true),
    REPORTS_LIST_CONTENT("§c Report§8: §e%player%§3 » §6%reason% §8| §7%reporter% §8[§7%time%§8]", true),
    REPORTS_ACCEPT_SUCCESS("§7 Du hast den Report erfolgreich angenommen!", true),
    REPORTS_ACCEPT_COUNT("§7 Anzahl der Reports: §e%count%", true),
    REPORTS_ACCEPT_LIST_CAPTION("§7 Gründe:", true),
    REPORTS_ACCEPT_LIST_CONTENT("§8 » §7%reason%§8 | §e%reporter%", true),
    REPORTS_NOTREPORTED("§7 Dieser Spieler wurde nicht reportet!", true),
    REPORTS_OFFLINE("§c Dieser Spieler ist bereits offline!", true),

    SEND_USAGE("§7 Benutze: §e/forcejoin [all|current||[PlayerName]] [Server]", true),
    SEND_NOTIFY("§7 Du wurdest auf dem Server §e%server%§7 verschoben.", true),
    SEND_ALREADY("§7 Du wurdest nicht verschoben, da du schon auf diesem Server bist.", true),
    SEND_OFFLINE("§7 Dieser Server ist §coffline", true),

    SETIPBAN_USAGE("§7 Benutze: §e/setipban [Spieler] [Ja/Nein]", true),
    SETIPBAN_NOTFOUND("§7 Dieser Spieler ist nicht in der Datenbank registriert.", true),
    SETIPBAN_NOTBANNED("§7 Dieser Spieler ist nicht gebannt!", true),
    SETIPBAN_ON_SUCCESS("§7 Du hast den IP-Ban aktiviert!", true),
    SETIPBAN_ON_NOTIFY("§c %admin%§7 hat den IP-Ban von §e%player%§a aktiviert§7!", true),
    SETIPBAN_OFF_SUCCESS("§7 DU hast du IP-Ban deaktiviert", true),
    SETIPBAN_OFF_NOTIFY("§c %admin%§7 hat den IP-Ban von §e%player%§a deaktiviert§7!", true),
    SETIPBAN_USAGE2("§7 Gib als 2. Parameter \"JA\" oder \"Nein\" ein.", true),

    SLOTS_COUNT("§7 Aktuelle Slots: §e%count%", true),
    SLOTS_USAGE("§7 Benutze: §e/Slots <Maximale Slots>", true),
    SLOTS_NONUMBER("§e %number%§7 ist keine Zahl!", true),
    SLOTS_SUCCESS("§7 Du hast die §eSlots §aerfolgreich §7geändert!", true),

    TC_LOGIN("§c Logge dich zunächst in das TeamChat System ein!", true),
    TC_USAGE("§7 Benutze: §e/tc [Text...]", true),
    TC_MESSAGE("§9➤ TC §8| §6%name% §8| §9%server% §8➟ §e%message%", true),

    UNBAN_USAGE("§7 Benutze: §e/unban [SpielerName] [Grund...]", true),
    UNBAN_NOTFOUND("§c Dieser Spieler war noch nie auf dem Netzwerk", true),
    UNBAN_NOTBANNED("§7 Dieser Spieler ist nicht gebannt!", true),
    UNBAN_SUCCESS("§7 Du hast erfolgreich §e%player%§a entbannt!", true),
    UNBAN_NOTIFY("§c %admin%§7 hat §e%player%§7 entbannt", true),

    UNMUTE_USAGE("§7 Benutze: §e/unmute [SpielerName] [Grund...]", true),
    UNMUTE_NOTFOUND("§c Dieser Spieler war noch nie auf dem Netzwerk", true),
    UNMUTE_NOTBANNED("§7 Dieser Spieler ist nicht gemuted!", true),
    UNMUTE_SUCCESS("§7 Du hast erfolgreich §e%player%§a entmuted!", true),
    UNMUTE_NOTIFY("§c %admin%§7 hat §e%player%§7 entmuted", true),

    WARN_USAGE("§7 Benutze: §e/warn [Spieler] [Grund...]",true),
    WARN_NOTFOUND("§c Dieser Spieler hat das Netzwerk noch nie betreten",true),
    WARN_TITLE_1("§8» §4§lVerwarnung §8«",true),
    WARN_TITLE_2("§8➥ §c§l%reason%",true),
    WARN_NOTIFY_PLAYER("§c Du wurdest wegen §e%reason% §4verwarnt§c! \n%prefix% §7Du hast jetzt §e%count% §7Verwarnungen",true),
    WARN_SUCCESS("§7 Du hast §e%player%§7 erfolgreich verwarnt.",true),
    WARN_NOTIFY_TEAM("§c %admin%§7 hat §e%player%§7 wegen §e%reason%§7 gewarnt!",true),

    WARNS_LIST_HEADER("§c§m------------[§r §6Warnings von %player% §c§m]------------", true),
    WARNS_LIST_COUNT(" §eWarn %count%§8:", true),
    WARNS_LIST_REASON(" §7Grund§8: §e%reason%", true),
    WARNS_LIST_TEAM(" §7Teammitglied§8: §e%team%", true),
    WARNS_LIST_TIME(" §7Zeitpunkt§8: §e%time%", true),
    WARNS_LIST_FINALCOUNT(" §7Der Spieler hat §e%count%§6 Warns", true),
    WARNS_DELETE_SUCCESS("§7 Du hast die Warns von §e%player%§c gelöscht§7!", true),
    WARNS_NOTFOUND("§7 Dieser Spieler hat das Netzwerk noch nicht betreten", true),


    WARTUNG_USAGE("§7 Benutze: §e/wartung <an/aus>", true),
    WARTUNG_SUCCESS("§7 Du hast die §eMotd §aerfolgreich §7geändert!", true),

    WHEREAMI("§7 Du befindest dich auf §e%server%", true),

    WHEREIS_USAGE("§7 Benutze: §e/whereis [Spieler]", true),
    WHEREIS_NOTFOUND("§c Dieser Spieler ist nich online!", true),
    WHEREIS_MESSAGE("§7 Der Spieler §6%player%§7 befindet sich auf §e%server%§7.", true),

    CHATFILTER_NOTIFY1("§7 Der Spieler §e%player%§7 hat eine Verbotene Nachricht abgeschickt:", true),
    CHATFILTER_NOTIFY2("§8 » §6%message%", false),

    JOIN_WARNSCOUNT("§c Du hast schon §e%count%§c Warns.", true),
    JOIN_TITLE1("§aWilkommen", true),
    JOIN_TITLE2("§eauf Server.de", true),

    CHAT_MUTED("§c Du bist noch für §e%time%§c wegen §e%reason%§c gemuted!", true),
    CHAT_NOSPAMCOMMANDS("§7 Bitte spamme keine Befehle!", true),
    CHAT_NOSPAM("§7 Bitte spamme nicht!", true),
    CHAT_NOREPETATION("§7 Bitte wiederhole dich nicht!", true),
    CHAT_DEACTIVATED("§c Der Chat ist deaktiviert", true),


    AUTH_SECURE_ENTERPIN("§c Bitte verifizeire dich! §7Schreib deinen §eCode §7aus der §6Google Autenticator App §7in den Chat!", true),
    AUTH_ACTIVATE_MESSAGE("§c Du musst das Verify-System aktivieren!\n§7Dein Code: %secret%\n§7Öffne den GoogleAuthenticator auf deinem Handy, und gib deinen Code ein," +
            "oder clicke %link%§7 um einen QR-Code zu öffnen.\n§eGib deinen §6Code §edanach in §6den Chat§e ein!", true),
    AUTH_ACTIVATE_LINK_MESSAGE("§e§ohier", false),
    AUTH_ACTIVATE_LINK_HOVER("§7Öffne einen §6Link §7zu einem §eQR-Code§7!", false),
    AUTH_VERIFY_FAILURE("§7 Der angegebene Code ist §cveraltet §7oder §cungültig§7!", true),
    AUTH_VERIFY_SUCCESS("§7 Du wurdest §eerfolgreich §everifiziert§7!", true),

    VERIFY_USAGE1("§7 Benutze: §e/verify reset <spieler>", true),
    VERIFY_USAGE2("§7 Benutze: §e/verify activate", true),
    VERIFY_NOTFOUND("§c Spieler wurde nicht gefunden", true),
    VERIFY_SUCCESS("§c Verifikation wurde erfolgreich deaktiviert!", true),



    PLACEHOLDER("DO_NOT_REMOVE_THIS", "Do not remove this!", false);

    String path;
    @Setter
    String content;
    Boolean prefix;

    Message(String content, Boolean prefix){
        this.content = content;
        this.prefix = prefix;
        this.path = this.toString().replaceAll("_", ".").toLowerCase();
    }


    public Message replace(String key, String value){
        if(!key.startsWith("%")){
            key = "%" + key + "%";
        }
        String s = getContent().replaceAll(key, value);
        PLACEHOLDER.setContent(s);
        return PLACEHOLDER;
    }

    public Message replace(String key, Object value){
        return replace(key, String.valueOf(value));
    }
}
