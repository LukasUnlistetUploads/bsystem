package de.primeapi.bungeesystem.managers.messages;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import lombok.SneakyThrows;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class MessageManager {


    File file;
    Configuration cfg;


    public MessageManager() throws IOException {
        file = new File("plugins/PrimeBungeeSystem", "messages.yml");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);

        BungeeSystem.getThreadPoolExecutor().submit(() -> {
            int i = 0;
            for (Message message : Message.values()) {
                if (cfg.contains(message.getPath())) {
                    message.setContent(ChatColor.translateAlternateColorCodes('&', cfg.getString(message.getPath())).replaceAll("%prefix%", Data.pr));
                } else {
                    String s = (message.getPrefix() ? "%prefix%" : "") + message.getContent().replaceAll("§", "&");
                    cfg.set(message.getPath(), s);
                    i++;
                    message.setContent(ChatColor.translateAlternateColorCodes('&', s.replaceAll("%prefix%", Data.pr)));
                }
            }
            System.out.println("[BungeeSystem] Es wurde(n) " + i + " neue Nachricht(en) in die messages.yml eingefügt!");
            try {
                ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

}
