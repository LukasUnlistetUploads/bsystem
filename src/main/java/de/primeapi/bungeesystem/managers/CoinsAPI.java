package de.primeapi.bungeesystem.managers;

import de.primeapi.bungeesystem.mysql.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CoinsAPI {


    public static Integer getCoins(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("coins");

            Result.close();
            State.close();

            return Integer.valueOf(s);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static void setCoins(String uuid, Integer coins) {
        MySQL.Update("UPDATE Users SET coins='" + coins + "' WHERE uuid='" + uuid + "'");
    }


    public static void addcoins(String uuid, Integer coins) {
        int coin = CoinsAPI.getCoins(uuid) + coins;
        CoinsAPI.setCoins(uuid, coin);
    }

    public static void removecoins(String uuid, Integer coins) {
        int coin = CoinsAPI.getCoins(uuid) - coins;
        CoinsAPI.setCoins(uuid, coin);
    }
}
