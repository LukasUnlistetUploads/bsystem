package de.primeapi.bungeesystem.managers;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.commands.*;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class CommandManager {

    public static ArrayList<Command> registeredCommands = new ArrayList<>();


    public static void init(){
        File file = new File("plugins/PrimeBungeeSystem", "commands.yml");
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Configuration cfg = null;
        try {
            cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String active = "";
        int activeCount = 0;
        String deactive = "";
        int deactiveCount = 0;

        for(Command c : Command.values()){
            boolean b;

            if(cfg.contains("commands." + c.getName())) {
                b = cfg.getBoolean("commands." + c.getName());
            }else{
                b = true;
                System.out.println("[BungeeSystem] Command '" + c.getName() + "' wurde eingetragen!");
                cfg.set("commands." + c.getName(), true);
            }

            if(b){
                ProxyServer.getInstance().getPluginManager().registerCommand(BungeeSystem.getInstance(), c.getCommand());
                registeredCommands.add(c);
                active += c.getName() + ", ";
                activeCount++;
            }else {
                deactive += c.getName() + ", ";
                deactiveCount++;
            }
        }
        System.out.println("[BungeeSystem] Aktivierte Commands (" + activeCount + "): " + active);
        System.out.println("[BungeeSystem] Deaktivierte Commands (" + deactiveCount + "): " + deactive);


        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    public enum Command{
        ABAN("aban", new ABanCommand()),
        BAN("ban", new BanCommand()),
        UNBAN("unban", new UnBanCommand()),
        HISTORY("history", new HistoryCommand()),
        EDITBAN("editban", new EditBanCommand()),
        BANLIST("banlist", new BanListCommand()),
        KICK("kick", new KickCommand()),

        AMUTE("amute", new AMuteCommand()),
        UNMUTE("unmute", new UnMuteCommand()),

        CHATFILTER("chatfilter", new ChatFilterCommand()),

        WARN("warn", new WarnCommand()),
        WARNS("warns", new WarnsCommand()),

        INFO("info", new InfoCommand()),
        LOGIN("login", new LoginCommand()),

        REPORT("report", new ReportCommand()),
        REPORTS("reports", new ReportsCommand()),

        TC("tc", new TCCommand()),
        BAUSERVER("bauserver", new BauServerCommand("bauserver")),
        BC("bc", new BCCommand("bc")),
        CC("cc", new CCCommand("cc")),
        GETIP("getip", new GetIPCommand("getip")),
        JOINTO("jointo", new JoinToCommand("jointo")),
        JUMPTO("jumpto", new JumpToCommand("jumpto")),
        FORCEJOIN("forcejoin", new SendCommand("forcejoin")),
        WHEREAMI("whereami", new WhereAmICommand("whereami")),
        WHEREIS("whereis", new WhereIsCommand("whereis")),
        CHATMUTE("chatmute", new ChatMuteCommand("chatmute")),
        DC("dc", new DCCommand("dc")),
        DISCORD("discord", new DCCommand("discord")),
        TS("ts", new TSCommand("ts")),
        TEAMSPEAK("teamspeak", new TSCommand("teamspeak")),
        SHOP("shop", new ShopCommand("shop")),
        FORUM("forum", new ForumCommand("forum")),
        APPLY("apply", new ApplyCommand("apply")),

        COINS("coins", new CoinsCommand("coins")),
        JOINME("joinme", new JoinMeCommand("joinme")),
        MOTD("motd", new MotdCommand("motd")),
        WARTUNG("wartung", new WartungCommand("wartung")),
        YT("yt", new YtCommand("yt")),
        YOUTUBER("youtuber", new YtCommand("youtuber")),
        ON("on", new OnlineCommand("on")),
        ONLINE("online", new OnlineCommand("online")),
        SLOTS("slots", new SlotsCommand("slots")),
        FAKEPLAYS("fakeplayers", new FakePlayersCommand("fakeplayers")),
        PING("ping", new PingCommand("ping")),
        FOLLOW("follow", new FollowCommand("follow")),

        FRIEND("friend", new FriendCommand("friend")),
        MSG("msg", new MsgCommand("msg")),
        PN("pn", new MsgCommand("pn")),
        WISPER("wisper", new MsgCommand("wisper")),
        R("r", new ReplyCommand("r")),
        REPLY("reply", new ReplyCommand("reply")),

        LOBBY("lobby", new LobbyCommand("lobby")),
        HUB("hub", new LobbyCommand("hub")),
        L("l", new LobbyCommand("l")),
        AFK("afk", new AFKCommand("afk")),
        ONTIME("ontime", new OnTimeCommand("ontime")),

        SETIPBAN("setipban", new SetIpBanCommand("setipban")),

        VERIFY("verify", new VerifyCommand("verify")),

        END("end", new EndCommand("end"));


        String name;
        net.md_5.bungee.api.plugin.Command command;

        Command(String name, net.md_5.bungee.api.plugin.Command command) {
            this.name = name;
            this.command = command;
        }

        public net.md_5.bungee.api.plugin.Command getCommand() {
            return command;
        }

        public String getName() {
            return name;
        }
    }


}
