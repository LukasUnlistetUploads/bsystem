package de.primeapi.bungeesystem.managers;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.AuthStats;
import de.primeapi.bungeesystem.mysql.OnlineStats;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AuthManager {


    public static List<UUID> authPrision = new ArrayList<>();

    public static void lockPlayer(PrimePlayer p){
        AuthManager.authPrision.add(p.getUUID());
        OnlineStats.updateFrozen(p.getUUID(), true);
        p.sendMessage(Message.AUTH_SECURE_ENTERPIN);
    }

    public static void activatePlayer(PrimePlayer p){
        authPrision.add(p.getUUID());
        GoogleAuthenticator authenticator = new GoogleAuthenticator();
        String secret = authenticator.createCredentials().getKey();
        AuthStats.setKey(p.getUUID(), secret);
        String url = "otpauth://totp/" + Conf.getSettingsString("ServerName") + "Verify:" + p.getName() + "?secret=" + secret + "&issuer=BungeeSystem";

        TextComponent component = new TextComponent(Message.AUTH_ACTIVATE_LINK_MESSAGE.getContent());
        component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Message.AUTH_ACTIVATE_LINK_HOVER.getContent()).create()));
        component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, Conf.getSettingsStringRaw("settings.auth.qrServer").replaceAll("%data%", url)));
        String[] message = Message.AUTH_ACTIVATE_MESSAGE.replace("secret", secret).getContent().split("%link%");
        if(message.length < 2) message[1] = "";

        p.getPp().sendMessage(new TextComponent(message[0]), component, new TextComponent(message[1]));
    }

    public static boolean checkKey(PrimePlayer p, int key){
        GoogleAuthenticator authenticator = new GoogleAuthenticator();
        return authenticator.authorize(AuthStats.getKey(p.getUUID()), key);
    }

}
