package de.primeapi.bungeesystem.main;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;

public class Data {


    public static boolean License;

    public static String pr = "§8[§cBan§8]";
    public static String wpr = pr;
    public static String noperm = pr + "§c Keine Berechtigung!";

    public static boolean luckPerms = false;

    public static HashMap<String, String> msg = new HashMap<>();


    public static ArrayList<ServerInfo> chatmutedServers = new ArrayList<ServerInfo>();
    public static boolean globalchatmute = false;

    public static int fakeplayers = 0;
    public static int slots = Conf.getSettingsInteger("Slots");

    public static HashMap<ProxiedPlayer, ProxiedPlayer> follow = new HashMap<>();
    public static HashMap<ProxiedPlayer, ProxiedPlayer> follow2 = new HashMap<>();


    public static boolean stopping = false;
    public static int stoppingInt = 0;


}
