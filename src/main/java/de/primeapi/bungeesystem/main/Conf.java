package de.primeapi.bungeesystem.main;

import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import lombok.NonNull;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Conf {

    public static void createConf() {
        File ord = new File("plugins/PrimeBungeeSystem");
        if (!ord.exists()) {
            ord.mkdir();
        }


        File file = new File("plugins/PrimeBungeeSystem", "MySQL.yml");
        if (!file.exists()) {
            try {
                System.out.println("[BanSystem] Erstelle MySQL Datei");
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("[BanSystem] Erstellen der MySQL Datei fehlgeschlagen:");
                e.printStackTrace();
            }
        }

        File conf = new File("plugins/PrimeBungeeSystem", "ChatFilter.yml");
        if (!conf.exists()) {
            try {
                System.out.println("[BanSystem] Erstelle Chatfilter Datei");
                conf.createNewFile();
            } catch (IOException e) {
                System.out.println("[BanSystem] Erstellen der Chatfilter Datei fehlgeschlagen:");
                e.printStackTrace();
            }
        }


        File banreasons = new File("plugins/PrimeBungeeSystem", "banreasons.yml");
        if (!banreasons.exists()) {
            try {
                System.out.println("[BanSystem] Erstelle banreasons Datei");
                banreasons.createNewFile();
            } catch (IOException e) {
                System.out.println("[BanSystem] Erstellen der banreasons Datei fehlgeschlagen:");
                e.printStackTrace();
            }
        }


        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(banreasons);
            if (!cfg.contains("reasons")) {

                List<String> reasons = new ArrayList<String>();
                reasons.add("1");
                reasons.add("2");
                reasons.add("3");
                reasons.add("4");
                reasons.add("5");
                reasons.add("6");
                reasons.add("7");
                reasons.add("8");
                reasons.add("9");
                reasons.add("10");
                reasons.add("11");
                reasons.add("12");
                reasons.add("13");
                reasons.add("14");
                reasons.add("15");
                reasons.add("16");
                reasons.add("17");
                reasons.add("18");
                reasons.add("55");
                reasons.add("66");
                reasons.add("77");
                reasons.add("88");
                reasons.add("99");

                cfg.set("reasons", reasons);


                cfg.set("Reason.1.Reason", "Hacking");
                cfg.set("Reason.1.Type", "Ban");
                cfg.set("Reason.1.FirstBan.int", 30);
                cfg.set("Reason.1.FirstBan.s", "d");
                cfg.set("Reason.1.SecondBan.int", -1);
                cfg.set("Reason.1.SecondBan.s", "P");

                cfg.set("Reason.2.Reason", "Bugusing");
                cfg.set("Reason.2.Type", "Ban");
                cfg.set("Reason.2.FirstBan.int", 15);
                cfg.set("Reason.2.FirstBan.s", "d");
                cfg.set("Reason.2.SecondBan.int", 90);
                cfg.set("Reason.2.SecondBan.s", "d");

                cfg.set("Reason.3.Reason", "Teaming");
                cfg.set("Reason.3.Type", "Ban");
                cfg.set("Reason.3.FirstBan.int", 10);
                cfg.set("Reason.3.FirstBan.s", "d");
                cfg.set("Reason.3.SecondBan.int", 20);
                cfg.set("Reason.3.SecondBan.s", "d");

                cfg.set("Reason.4.Reason", "Banumgehung");
                cfg.set("Reason.4.Type", "Ban");
                cfg.set("Reason.4.FirstBan.int", 365);
                cfg.set("Reason.4.FirstBan.s", "d");
                cfg.set("Reason.4.SecondBan.int", -1);
                cfg.set("Reason.4.SecondBan.s", "P");

                cfg.set("Reason.5.Reason", "Rassismus");
                cfg.set("Reason.5.Type", "Ban");
                cfg.set("Reason.5.FirstBan.int", 60);
                cfg.set("Reason.5.FirstBan.s", "d");
                cfg.set("Reason.5.SecondBan.int", 120);
                cfg.set("Reason.5.SecondBan.s", "d");

                cfg.set("Reason.6.Reason", "Reportausnutzung");
                cfg.set("Reason.6.Type", "Ban");
                cfg.set("Reason.6.FirstBan.int", 1);
                cfg.set("Reason.6.FirstBan.s", "d");
                cfg.set("Reason.6.SecondBan.int", 7);
                cfg.set("Reason.6.SecondBan.s", "d");

                cfg.set("Reason.7.Reason", "Beleidigung");
                cfg.set("Reason.7.Type", "Mute");
                cfg.set("Reason.7.FirstBan.int", 2);
                cfg.set("Reason.7.FirstBan.s", "h");
                cfg.set("Reason.7.SecondBan.int", 1);
                cfg.set("Reason.7.SecondBan.s", "d");

                cfg.set("Reason.8.Reason", "Harte Beleidigung");
                cfg.set("Reason.8.Type", "Mute");
                cfg.set("Reason.8.FirstBan.int", 1);
                cfg.set("Reason.8.FirstBan.s", "d");
                cfg.set("Reason.8.SecondBan.int", 5);
                cfg.set("Reason.8.SecondBan.s", "d");

                cfg.set("Reason.9.Reason", "Sehr harte Beleidigung");
                cfg.set("Reason.9.Type", "Mute");
                cfg.set("Reason.9.FirstBan.int", 30);
                cfg.set("Reason.9.FirstBan.s", "d");
                cfg.set("Reason.9.SecondBan.int", 90);
                cfg.set("Reason.9.SecondBan.s", "d");

                cfg.set("Reason.10.Reason", "Extreme Beleidigung");
                cfg.set("Reason.10.Type", "Ban");
                cfg.set("Reason.10.FirstBan.int", 30);
                cfg.set("Reason.10.FirstBan.s", "d");
                cfg.set("Reason.10.SecondBan.int", 365);
                cfg.set("Reason.10.SecondBan.s", "P");

                cfg.set("Reason.11.Reason", "Werbung - SocialMedia");
                cfg.set("Reason.11.Type", "Mute");
                cfg.set("Reason.11.FirstBan.int", 24);
                cfg.set("Reason.11.FirstBan.s", "h");
                cfg.set("Reason.11.SecondBan.int", 3);
                cfg.set("Reason.11.SecondBan.s", "d");

                cfg.set("Reason.12.Reason", "Werbung - FremdServer");
                cfg.set("Reason.12.Type", "Mute");
                cfg.set("Reason.12.FirstBan.int", 30);
                cfg.set("Reason.12.FirstBan.s", "d");
                cfg.set("Reason.12.SecondBan.int", -1);
                cfg.set("Reason.12.SecondBan.s", "P");

                cfg.set("Reason.13.Reason", "Werbung - Sonstiges");
                cfg.set("Reason.13.Type", "Mute");
                cfg.set("Reason.13.FirstBan.int", 5);
                cfg.set("Reason.13.FirstBan.s", "h");
                cfg.set("Reason.13.SecondBan.int", 1);
                cfg.set("Reason.13.SecondBan.s", "d");

                cfg.set("Reason.14.Reason", "Spamming");
                cfg.set("Reason.14.Type", "Mute");
                cfg.set("Reason.14.FirstBan.int", 1);
                cfg.set("Reason.14.FirstBan.s", "h");
                cfg.set("Reason.14.SecondBan.int", 2);
                cfg.set("Reason.14.SecondBan.s", "d");

                cfg.set("Reason.15.Reason", "RandomKilling [TTT]");
                cfg.set("Reason.15.Type", "Ban");
                cfg.set("Reason.15.FirstBan.int", 3);
                cfg.set("Reason.15.FirstBan.s", "d");
                cfg.set("Reason.15.SecondBan.int", 7);
                cfg.set("Reason.15.SecondBan.s", "d");

                cfg.set("Reason.16.Reason", "Trolling");
                cfg.set("Reason.16.Type", "Ban");
                cfg.set("Reason.16.FirstBan.int", 7);
                cfg.set("Reason.16.FirstBan.s", "d");
                cfg.set("Reason.16.SecondBan.int", 17);
                cfg.set("Reason.16.SecondBan.s", "d");

                cfg.set("Reason.17.Reason", "Skin");
                cfg.set("Reason.17.Type", "Ban");
                cfg.set("Reason.17.FirstBan.int", 10);
                cfg.set("Reason.17.FirstBan.s", "d");
                cfg.set("Reason.17.SecondBan.int", 20);
                cfg.set("Reason.17.SecondBan.s", "d");

                cfg.set("Reason.18.Reason", "Name");
                cfg.set("Reason.18.Type", "Ban");
                cfg.set("Reason.18.FirstBan.int", 30);
                cfg.set("Reason.18.FirstBan.s", "d");
                cfg.set("Reason.18.SecondBan.int", 90);
                cfg.set("Reason.18.SecondBan.s", "d");

                cfg.set("Reason.55.Reason", "Auszeit | 7 Tage");
                cfg.set("Reason.55.Type", "Ban");
                cfg.set("Reason.55.FirstBan.int", 7);
                cfg.set("Reason.55.FirstBan.s", "d");
                cfg.set("Reason.55.SecondBan.int", 7);
                cfg.set("Reason.55.SecondBan.s", "d");

                cfg.set("Reason.66.Reason", "Auszeit | 30 Tage");
                cfg.set("Reason.66.Type", "Ban");
                cfg.set("Reason.66.FirstBan.int", 30);
                cfg.set("Reason.66.FirstBan.s", "d");
                cfg.set("Reason.66.SecondBan.int", 30);
                cfg.set("Reason.66.SecondBan.s", "d");

                cfg.set("Reason.77.Reason", "Auszeit | 90 Tage");
                cfg.set("Reason.77.Type", "Ban");
                cfg.set("Reason.77.FirstBan.int", 90);
                cfg.set("Reason.77.FirstBan.s", "d");
                cfg.set("Reason.77.SecondBan.int", 90);
                cfg.set("Reason.77.SecondBan.s", "d");

                cfg.set("Reason.88.Reason", "Auszeit | 365 Tage");
                cfg.set("Reason.88.Type", "Ban");
                cfg.set("Reason.88.FirstBan.int", 365);
                cfg.set("Reason.88.FirstBan.s", "d");
                cfg.set("Reason.88.SecondBan.int", 365);
                cfg.set("Reason.88.SecondBan.s", "d");

                cfg.set("Reason.99.Reason", "Hausverbot");
                cfg.set("Reason.99.Type", "Ban");
                cfg.set("Reason.99.FirstBan.int", -1);
                cfg.set("Reason.99.FirstBan.s", "P");
                cfg.set("Reason.99.SecondBan.int", -1);
                cfg.set("Reason.99.SecondBan.s", "P");

                ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, banreasons);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
        if (!settings.exists()) {
            try {
                System.out.println("[BanSystem] Erstelle Settings Datei");
                settings.createNewFile();
            } catch (IOException e) {
                System.out.println("[BanSystem] Erstellen der Settings Datei fehlgeschlagen:");
                e.printStackTrace();
            }
        }


        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(conf);
            if (!cfg.contains("filter")) {
                List<String> filter = new ArrayList<String>();
                List<String> blacklist = new ArrayList<String>();

                cfg.set("filter", filter);
                cfg.set("blacklist", blacklist);
                ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, conf);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            if (!cfg.contains("host")) {
                cfg.set("host", "HosterDeinesVertrauens");
                cfg.set("dbname", "NameDeinerLieblingsDatenbank");
                cfg.set("dbuser", "DatenbankNutzerName");
                cfg.set("dbpw", "PasswortDesNutzers");
                ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            if (!cfg.contains("ServerName")) {
                cfg.set("ServerName", "ServerName");
                cfg.set("LizenzKey", "RequestOnTheDiscord");
                cfg.set("Prefix", "&8[>] &cServer.de &7[>>]");
                cfg.set("report.keepReportsOnLogout", false);
                cfg.set("report.keepReportsOnRestart", false);
                cfg.set("toggle.motd", true);
                cfg.set("toggle.joinmessage", true);
                cfg.set("toggle.loginStatusOnJoin", true);
                cfg.set("toggle.autobc", true);
                cfg.set("toggle.clearchatOnJoin", true);
                cfg.set("toggle.titleOnJoin", true);
                cfg.set("toggle.warnsOnJoin", true);
                cfg.set("toggle.autpIPBan", true);
                cfg.set("settings.blacklist.reason", "Chatverhalten");
                cfg.set("settings.blacklist.timeInDays", 1);

                cfg.set("settings.auth.use", true);
                cfg.set("settings.auth.permission", "bungee.team");
                cfg.set("settings.auth.force", false);
                cfg.set("settings.auth.onlyOnIpChange", true);
                cfg.set("settings.auth.qrServer", "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=%data%");

                cfg.set("Coins.info", "Wenn du \"pay\" aktivierst, können sich Spieler gegenseitig Geld geben.");
                cfg.set("Coins.startgeld", 1000);
                cfg.set("Coins.pay", false);
                cfg.set("chatmanager.slowchat.use", true);
                cfg.set("chatmanager.slowchat.count", 3);
                cfg.set("chatmanager.slowcommand.use", true);
                cfg.set("chatmanager.slowcommand.count", 3);
                cfg.set("chatmanager.repeatmessage.use", true);
                cfg.set("chatmanager.repeatmessage.count", 300);
                {
                List<String> list = new ArrayList<>();
                list.add("rank.admin:5");
                list.add("rank.youtube:15");
                cfg.set("joinme.cooldown", list);
                }

                {
                    cfg.set("reports.usetemplates", false);
                    String[] array = {"HACKING", "BUGUSING", "BELEIDIGUNGEN"};
                    cfg.set("reports.reasons", Arrays.asList(array));
                }
                cfg.set("Wartung", false);
                cfg.set("Motd.normal.1", "&8[x] &bDeinServer&7.&3de &8[x] &7network &8[>>]&71.8.x - 1.13&8[<<]");
                cfg.set("Motd.normal.2", "&7[>] &e&lNEU: &6&l/coins");
                cfg.set("Motd.wartung.1", "&8[x] &bDeinServer&7.&3de &8[x] &7network &8[>>]&71.8.x - 1.13&8[<<]");
                cfg.set("Motd.wartung.2", "&c[x] &4&lWartungsarbeiten &c[x] §6Status§8: §e50%");
                cfg.set("Slots", 50);
                cfg.set("Lobby.#", "Wenn du nur eine Lobby hast, gebe bei name den exakten Namen der Lobby ein!");
                cfg.set("Lobby.name", "Lobby-");
                cfg.set("Lobby.amount", 2);
                cfg.set("TeamSpeakMSG", "&7Unseren &eTeamSpeak &7erreicht du unter&8:&b Server.de");
                cfg.set("DiscordMSG", "&7Unseren &eDiscord &7erreicht du unter&8:&b discord.gg/fhdsj");
                cfg.set("ShopMSG", "&7Unseren &eShop &7erreicht du unter&8:&b shop.server.de");
                cfg.set("ForumMSG", "&7Unser &eForum &7erreicht du unter&8:&b forum.server.de");
                cfg.set("BauServerName", "BuilderServer");
                cfg.set("TabList.aktive", true);
                List<String> header = new ArrayList<>();
                List<String> footer = new ArrayList<>();
                header.add("&8[>>] &bDeinServer.de &8[<<]");
                header.add("&3%players%&8/&7%max%");
                footer.add("&8[>>] &bForum&8: &3Server.de");
                footer.add("&8[>>] &bTeamSpeak&8: &3Server.de");
                cfg.set("TabList.header", header);
                cfg.set("TabList.footer", footer);
                List<String> dev = new ArrayList<>();
                List<String> sup = new ArrayList<>();
                List<String> builder = new ArrayList<>();
                List<String> ovw = new ArrayList<>();
                List<String> yt = new ArrayList<>();
                List<String> unban = new ArrayList<>();


                yt.add("&8&m-------------[&r &5&lYouTuber &7| &eAnforderungen &8&m]-------------");
                yt.add("");
                yt.add("&8» &eWenn du die Anforderungen erfüllst, melde dich im &6TeamSpeak &8«");
                yt.add("");
                yt.add("&7[>] &e&lPremium&6+");
                yt.add("&8[>] &7Mindestens &e150 &7Abonenten");
                yt.add("&8[>] &7Mindestens &e500 &7Kanalaufrufe");
                yt.add("");
                yt.add("&7[>] &4&lYouTuber");
                yt.add("&8[>] &7Mindestens &e500 &7Abonenten");
                yt.add("&8[>] &7Mindestens &e1.000 &7Kanalaufrufe");
                yt.add("");
                yt.add("&8&m-------------[&r &5&lYouTuber &7| &eRang &8&m]-------------");

                dev.add("&8&m-------------[&r &e&lBewerbung &7| &bDeveloper &8&m]-------------");
                dev.add("");
                dev.add("&8» &eSchreibe deine Bewerbung mit folgendem Inhalt an: &eApply@server.de &8«");
                dev.add("");
                dev.add("&8[>] &bPunkt 1");
                dev.add("&8[>] &bPunkt 2");
                dev.add("&8[>] &bPunkt 2");
                dev.add("&8[>] &b...");
                dev.add("");
                dev.add("&8&m-------------[&r &e&lBewerbung &7| &bDeveloper &8&m]-------------");

                sup.add("&8&m-------------[&r &e&lBewerbung &7| &9Supporter &8&m]-------------");
                sup.add("");
                sup.add("&8» &eSchreibe deine Bewerbung mit folgendem Inhalt an: &eApply@server.de &8«");
                sup.add("");
                sup.add("&8[>] &bPunkt 1");
                sup.add("&8[>] &bPunkt 2");
                sup.add("&8[>] &bPunkt 2");
                sup.add("&8[>] &b...");
                sup.add("");
                sup.add("&8&m-------------[&r &e&lBewerbung &7| &9Supporter &8&m]-------------");

                builder.add("&8&m-------------[&r &e&lBewerbung &7| &aBuilder &8&m]-------------");
                builder.add("");
                builder.add("&8» &eSchreibe deine Bewerbung mit folgendem Inhalt an: &eApply@server.de &8«");
                builder.add("");
                builder.add("&8[>] &bPunkt 1");
                builder.add("&8[>] &bPunkt 2");
                builder.add("&8[>] &bPunkt 2");
                builder.add("&8[>] &b...");
                builder.add("");
                builder.add("&8&m-------------[&r &e&lBewerbung &7| &aBuilder &8&m]-------------");

                ovw.add("&8&m-------------[&r &e&lBewerbung &7| &6Übersicht &8&m]-------------");
                ovw.add("");
                ovw.add("&8» &7Du kannst dich aktuell für folgender Teamprositionen bewerben: &8«");
                ovw.add("§9[>] §eSupporter");
                ovw.add("§b[>] §eDeveloper");
                ovw.add("§a[>] §eBuilder");
                ovw.add("");
                ovw.add("&8» &6Benutze §e/Apply [Rang] §7für weitere Informationen &8«");
                ovw.add("");
                ovw.add("&8&m-------------[&r &e&lBewerbung &7| &6Übersicht &8&m]-------------");

                cfg.set("UnbanbareSpieler", unban);
                cfg.set("YouTuber.msg", yt);
                cfg.set("ApplyMSG.dev", dev);
                cfg.set("ApplyMSG.sup", sup);
                cfg.set("ApplyMSG.build", builder);
                cfg.set("ApplyMSG.overview", ovw);


                List<String> autobc = new ArrayList<>();
                autobc.add("\n[>>]\n&8[>] &cServer.de &7[>>] &7Joine unserem Teamspeak &8[>] &e/ts\n[>>]\n");
                autobc.add("\n[>>]\n&8[>] &cServer.de &7[>>] &7Joine unserem Discord &8[>] &e/discord\n[>>]\n");
                autobc.add("\n[>>]\n&8[>] &cServer.de &7[>>] &7Wir suchen Teammitglieder &8[>] &e/apply\n[>>]\n");
                cfg.set("autobc.list", autobc);
                cfg.set("autobc.timeInMinutes", 30);

                cfg.set("messages.noperms", "&c Keine Berechtigung!");
                cfg.set("messages.ban", "&8[>>] &4BanSystem &8[<<] <br> &cDu wurdest vom &cNetzwerk &4Gebannt&8!<br> &7Grund&8: &e%reason%<br> &7Verbleibende Zeit&8: &e%time%<br> <br> <br> &cDu kannst im &eForum &coder &eTeamspeak &ceinen Entbannungsantrag stellen!");
                cfg.set("messages.ipban", "&8[>>] &4BanSystem &8[<<] <br> &cEin Account mit der selben IP wurde gebannt&8!<br> &7Gebannter Account: &e%account%<br> &7Grund&8: &e%reason%<br> &7Verbleibende Zeit&8: &e%time%<br> <br> <br> &cDu kannst im &eForum &coder &eTeamspeak &ceinen Entbannungsantrag stellen!");
                cfg.set("messages.premiumjoin", "&cDer Server ist voll. Du benötigst einen höheren Rang!");
                cfg.set("messages.wartung", "&8[>>] &c&lWartungen &8[<<]<br> &cEs werden aktuell Wartungsarbeiten durchgeführt!<br><br> &cBei Fragen kannst du dich im &eForum &coder im &eTeamSpeak &cmelden.");
                cfg.set("messages.noperms", "&c Keine Berechtigung!");
                cfg.set("messages.firstjoin", "%prefix% &7Der Spieler &e%spieler% &7ist neu! &8[&a%count%&8]");

                cfg.set("saves.count", 0);


                ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, settings);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void removeblacklist(String word) {
        List<String> list = Conf.getBlacklist();
        list.remove(word);
        Conf.setBlacklist(list);
    }

    public static void addblacklist(String word) {
        List<String> list = Conf.getBlacklist();
        list.add(word);
        Conf.setBlacklist(list);
    }

    public static void addfilter(String word) {
        List<String> list = Conf.getFilter();
        list.add(word);
        Conf.setfilter(list);
    }

    public static void removefilter(String word) {
        List<String> list = Conf.getFilter();
        list.remove(word);
        Conf.setfilter(list);
    }

    public static String getBanString(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "banreasons.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            return cfg.getString(path).replace("&", "§").replace("[x]", "×").replace("[>>]", "»").replace("[>]", "➤").replace("[<<]", "«").replace("[.]", "●");
        } catch (Exception ex) {
            return null;
        }

    }

    public static Integer getBanInt(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "banreasons.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            return cfg.getInt(path);
        } catch (Exception ex) {
            return null;
        }

    }

    public static List<String> getBanList(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "banreasons.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            return cfg.getStringList(path);
        } catch (Exception ex) {
            return null;
        }

    }


    public static String getSettingsString(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            return cfg.getString(path).replace("&", "§").replace("[x]", "×").replace("[>>]", "»").replace("[>]", "➤").replace("[<<]", "«").replace("[.]", "●").replace("<br>", "\n");
        } catch (Exception ex) {
            return "Der Pfad: " + path + " wurde nicht geufunden.";
        }

    }
    public static String getSettingsStringRaw(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            return cfg.getString(path);
        } catch (Exception ex) {
            return "Der Pfad: " + path + " wurde nicht geufunden.";
        }

    }

    public static Boolean getSettingsBoolean(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            return cfg.getBoolean(path);
        } catch (Exception ex) {
            return false;
        }

    }

    public static Long getSettingsLong(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, settings);
            return cfg.getLong(path);
        } catch (Exception ex) {
            return (long) 0;
        }

    }

    public static void setSettingsBoolean(String path, Boolean b) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            cfg.set(path, b);
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, settings);
        } catch (Exception ex) {

        }
    }

    public static void setSettingsString(String path, String s) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            cfg.set(path, s.replace("§", "&").replace("×", "[x]").replace("»", "[>>]").replace("➤", "[>]").replace("«", "[<<]"));
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, settings);
        } catch (Exception ex) {

        }
    }

    public static void setSettingsInt(String path, Integer i) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            cfg.set(path, i);
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, settings);
        } catch (Exception ex) {

        }
    }

    public static void setSettingsLong(String path, Long l) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            cfg.set(path, l);
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, settings);
        } catch (Exception ex) {

        }
    }

    public static Integer getSettingsInteger(String path) {
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            return cfg.getInt(path);
        } catch (Exception ex) {
            return 0;
        }

    }

    public static List<String> getSettingsStringList(String path) {
        List<String> list = new ArrayList<>();
        try {
            File settings = new File("plugins/PrimeBungeeSystem", "settings.yml");
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(settings);
            for (String s : cfg.getStringList(path)) {
                list.add(s.replace("&", "§").replace("[.]", "●").replace("[x]", "×").replace("[>>]", "»").replace("[>]", "➤").replace("[<<]", "«"));
            }
            return list;
        } catch (Exception ex) {
            list.add("Die Arrayliste: " + path + " wurde nicht gefunden!");
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static List<String> getFilter() {
        File file = new File("plugins/PrimeBungeeSystem", "ChatFilter.yml");
        List<String> list = new ArrayList<>();
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            list = (List<String>) cfg.getList("filter");
        } catch (IOException e) {
            e.printStackTrace();

        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public static List<String> getBlacklist() {
        File file = new File("plugins/PrimeBungeeSystem", "ChatFilter.yml");
        List<String> list = new ArrayList<>();
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            list = (List<String>) cfg.getList("blacklist");
        } catch (IOException e) {
            e.printStackTrace();

        }
        return list;
    }

    public static void setBlacklist(List<String> list) {
        File file = new File("plugins/PrimeBungeeSystem", "ChatFilter.yml");
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            cfg.set("blacklist", list);
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, file);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static void setfilter(List<String> list) {
        File file = new File("plugins/PrimeBungeeSystem", "ChatFilter.yml");
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            cfg.set("filter", list);
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, file);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static String getHost() {
        File file = new File("plugins/PrimeBungeeSystem", "MySQL.yml");
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            return (String) cfg.get("host");
        } catch (IOException e) {
            e.printStackTrace();
            return "Fehler: 1";
        }
    }

    public static String getPort() {
        File file = new File("plugins/PrimeBungeeSystem", "MySQL.yml");
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            return (String) cfg.get("port");
        } catch (IOException e) {
            e.printStackTrace();
            return "Fehler: 1";
        }
    }

    public static String getname() {
        File file = new File("plugins/PrimeBungeeSystem", "MySQL.yml");
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            return (String) cfg.get("dbname");
        } catch (IOException e) {
            e.printStackTrace();
            return "Fehler: 1";
        }
    }


    public static String getuser() {
        File file = new File("plugins/PrimeBungeeSystem", "MySQL.yml");
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            return (String) cfg.get("dbuser");
        } catch (IOException e) {
            e.printStackTrace();
            return "Fehler: 1";
        }
    }

    public static String getPW() {
        File file = new File("plugins/PrimeBungeeSystem", "MySQL.yml");
        try {
            Configuration cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            return (String) cfg.get("dbpw");
        } catch (IOException e) {
            e.printStackTrace();
            return "Fehler: 1";
        }
    }


}
