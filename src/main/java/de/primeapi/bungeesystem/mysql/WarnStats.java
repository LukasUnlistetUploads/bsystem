package de.primeapi.bungeesystem.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WarnStats {


    public static void addWarnReason(String uuid, String reason, String admin, String time) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO Warns values(?, ?, ?, ?)");
            State.setString(1, uuid);
            State.setString(2, reason);
            State.setString(3, admin);
            State.setString(4, time);

            State.execute();
            State.close();
        } catch (Exception e) {
        }
    }

    public static void deleteAllLogs(String uuid) {
        MySQL.Update("DELETE FROM Warns WHERE uuid='" + uuid + "'");
    }

    public static List<String> getWarnReasons(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Warns WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("reason"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
        }

        List<String> listn = new ArrayList<String>();

        return listn;
    }


    public static List<String> getWarnAdmins(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Warns WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("admin"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
        }

        List<String> listn = new ArrayList<String>();

        return listn;
    }

    public static List<String> getWarnLongs(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Warns WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("longs"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
        }

        List<String> listn = new ArrayList<String>();

        return listn;
    }

}
