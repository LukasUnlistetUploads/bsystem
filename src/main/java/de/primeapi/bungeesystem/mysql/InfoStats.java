package de.primeapi.bungeesystem.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InfoStats {

    public static void addPlayer(String type, String info1, String info2, String info3) {
        if (!InfoStats.getTypes().contains(type)) {
            try {
                PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO Info values(?, ?, ?, ?)");
                State.setString(1, type);
                State.setString(2, info1);
                State.setString(3, info2);
                State.setString(4, info3);

                State.execute();
                State.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static List<String> getTypes() {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Info?");

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("type"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<String> listn = new ArrayList<String>();

        return listn;
    }


}
