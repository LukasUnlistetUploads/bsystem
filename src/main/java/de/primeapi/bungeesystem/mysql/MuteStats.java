package de.primeapi.bungeesystem.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class MuteStats {


    public static void addPlayer(String uuid) {
        if (!PlayerStats.isexist(uuid)) {
            try {
                PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO Mutes values(?, ?, ?, ?, ?)");
                State.setString(1, uuid);
                State.setString(2, "0");
                State.setString(3, "0");
                State.setString(4, "0");
                State.setString(5, "0");

                State.execute();
                State.close();
            } catch (Exception e) {
            }
        }
    }


    public static boolean isMuted(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Mutes WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("muted");

            Result.close();
            State.close();

            return s.equalsIgnoreCase("1");
        } catch (SQLException e) {
            return false;
        }

    }


//	public static List<String> getBannedUUIDs(){
//		List<String> list = new ArrayList<>();
//        try {
//            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Mutes WHERE banned==;");
//            State.setString(1, "1");
//
//            ResultSet Result = State.executeQuery();
//            
//
//            while (Result.next()) {
//                list.add(Result.getString("uuid"));
//            }
//
//            Result.close();
//            State.close();
//
//
//            
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return false;
//	}

    public static String getAdminEntry(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Mutes WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("admin");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
            return "Error";
        }

    }

    public static String getAdminName(String uuid) {
        String entry = MuteStats.getAdminEntry(uuid);
        if (PlayerStats.getName(entry).equalsIgnoreCase("ErrorExeption0043")) {
            return entry;
        } else {
            return PlayerStats.getName(entry);
        }
    }

    public static String getTime(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Mutes WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("lang");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
        }

        return "Error";
    }

    public static String getmuted1s(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Mutes WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("muted");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
        }

        return "Error";
    }

    public static String getReason(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Mutes WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("reason");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
        }

        return "Error";
    }

    public static void setMuted(String uuid, Boolean banned) {
        String banneds = "0";
        if (banned) {
            banneds = "1";
        } else {
            banneds = "0";
        }
        MySQL.Update("UPDATE Mutes SET muted='" + banneds + "' WHERE uuid='" + uuid + "'");
    }

    public static void setReason(String uuid, String reason) {
        MySQL.Update("UPDATE Mutes SET reason='" + reason + "' WHERE uuid='" + uuid + "'");
    }

    public static void setAdmin(String uuid, String Adminname) {
        MySQL.Update("UPDATE Mutes SET admin='" + Adminname + "' WHERE uuid='" + uuid + "'");
    }

    public static void setTime(String uuid, Long time) {
        MySQL.Update("UPDATE Mutes SET lang='" + time + "' WHERE uuid='" + uuid + "'");
    }

    public static void addMute(String uuid, String reason, String Adminuuid, Long time) {
        setMuted(uuid, true);
        setReason(uuid, reason);
        setAdmin(uuid, Adminuuid);
        setTime(uuid, time);
    }


}
