package de.primeapi.bungeesystem.mysql;


import de.primeapi.bungeesystem.main.Conf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL {

    public static Connection c;

    public static void connect() {
        try {
            c = DriverManager.getConnection("jdbc:mysql://" + Conf.getHost() + ":3306/" + Conf.getname() + "?autoReconnect=true", Conf.getuser(), Conf.getPW());
            System.out.println("[BungeeSystem] MySQL verbindung aufgebaut.");
        } catch (SQLException e) {
            System.out.println("[BungeeSystem] MySQL verbinding fehlgeschlagen. Bitte überpfüfe deine Daten:" + e.getMessage());
        }
    }

    public static void disconnect() {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void Update(String qry) {
        try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate(qry);
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            disconnect();
            connect();
        }
    }

}
