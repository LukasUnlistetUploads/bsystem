package de.primeapi.bungeesystem.mysql;

import de.primeapi.bungeesystem.main.Conf;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NotifyStats {


    public static void instertPlayer(String uuid) {
        if (!NotifyStats.isexist(uuid)) {
            try {
                PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO Notify values(?, ?, ?, ?)");
                State.setString(1, uuid);
                State.setString(2, "0");
                State.setString(3, "0");
                State.setString(4, "0");

                State.execute();
                State.close();
            } catch (Exception e) {
            }
        }
    }


    public static Boolean isexist(String uuid) {

        List<String> list = new ArrayList<String>();
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Notify WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();


            while (Result.next()) {
                list.add(Result.getString("uuid"));
            }

            Result.close();
            State.close();

        } catch (SQLException e) {
        }


        return list.contains(uuid);
    }


    public static Boolean isNotify(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * From Notify WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            if (Result.next()) {
                String s = Result.getString("notify");
                return !s.equalsIgnoreCase("0");
            }
        } catch (SQLException ex) {
        }
        return false;
    }

    public static Boolean isTC(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * From Notify WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            if (Result.next()) {
                String s = Result.getString("tc");
                return !s.equalsIgnoreCase("0");
            }
        } catch (SQLException ex) {
        }
        return false;
    }


    public static Boolean isReport(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * From Notify WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            if (Result.next()) {
                String s = Result.getString("report");
                return !s.equalsIgnoreCase("0");
            }
        } catch (SQLException ex) {
        }
        return false;
    }

    public static void setTC(String uuid, Boolean tc) {
        if (tc) {
            MySQL.Update("UPDATE Notify SET tc='1' WHERE uuid='" + uuid + "'");
        } else {
            MySQL.Update("UPDATE Notify SET tc='0' WHERE uuid='" + uuid + "'");
        }
    }

    public static void setNotify(String uuid, Boolean notify) {
        if (notify) {
            MySQL.Update("UPDATE Notify SET notify='1' WHERE uuid='" + uuid + "'");
        } else {
            MySQL.Update("UPDATE Notify SET notify='0' WHERE uuid='" + uuid + "'");
        }
    }

    public static void setReport(String uuid, Boolean report) {
        if (report) {
            MySQL.Update("UPDATE Notify SET report='1' WHERE uuid='" + uuid + "'");
        } else {
            MySQL.Update("UPDATE Notify SET report='0' WHERE uuid='" + uuid + "'");
        }
    }


}
