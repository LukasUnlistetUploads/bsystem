package de.primeapi.bungeesystem.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PlayerStats {

    public static void addPlayer(String name, String uuid, String IP) {
        if (!PlayerStats.isexist(uuid)) {
            try {
                PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO Users values(?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?)");
                State.setString(1, name.toLowerCase());
                State.setString(2, uuid);
                State.setString(3, IP);
                State.setString(4, "0");
                State.setString(5, "0");
                State.setString(6, "0");
                State.setString(7, "0");
                State.setString(8, "0");
                State.setString(9, "1000");
                State.setString(10, String.valueOf(System.currentTimeMillis()));
                State.setString(11, String.valueOf(System.currentTimeMillis()));
                State.setString(12, "0");

                State.execute();
                State.close();
            } catch (Exception e) {
            }
        }else{
            setName(name, uuid);
        }
    }

    public static Boolean isexist(String uuid) {

        List<String> list = new ArrayList<String>();
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();


            while (Result.next()) {
                list.add(Result.getString("uuid"));
            }

            Result.close();
            State.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return list.contains(uuid);
    }

    public static Boolean exist(String name) {

        List<String> list = new ArrayList<String>();
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE name=?");
            State.setString(1, name);

            ResultSet Result = State.executeQuery();


            while (Result.next()) {
                list.add(Result.getString("name"));
            }

            Result.close();
            State.close();

        } catch (SQLException e) {
        }


        return list.contains(name.toLowerCase());
    }


    public static String getUUID(String playerName) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE name=?;");
            State.setString(1, playerName.toLowerCase());

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("uuid");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
        }

        return "Spieler";
    }

    public static String getOnMins(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("onmins");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
        }

        return "0";
    }


    public static List<String> getAlts(String ip) {
        List<String> list = new ArrayList();
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE ip=?;");
            State.setString(1, ip);

            ResultSet Result = State.executeQuery();
            while (Result.next()) {
                list.add(Result.getString("name"));
            }


            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
        }
        return list;

    }

    public static String getLastJoin(String playerName) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE name=?;");
            State.setString(1, playerName.toLowerCase());

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("LastJoin");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "0";
    }

    public static String getFirstJoin(String playerName) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE name=?;");
            State.setString(1, playerName.toLowerCase());

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("FirstJoin");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
        }

        return "0";
    }

    public static String getIP(String playerName) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE name=?;");
            State.setString(1, playerName.toLowerCase());

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("ip");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
        }

        return "IP";
    }


    public static String getOnTime(String uuid) {
        int min = Integer.valueOf(PlayerStats.getOnMins(uuid));
        int hours = 0;
        while (min >= 60) {
            hours++;
            min = -60;
        }

        return hours + ":" + min;
    }

    public static String getName(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("name");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
            return "ErrorExeption0043";
        }

    }

    //onmins

    public static void setUUID(String playerName, String UUID) {
        MySQL.Update("UPDATE Users SET uuid='" + UUID + "' WHERE name='" + playerName.toLowerCase() + "'");
    }


    public static void updateLastJoin(String name) {
        MySQL.Update("UPDATE Users SET LastJoin='" + System.currentTimeMillis() + "' WHERE name='" + name.toLowerCase() + "'");
    }


    public static void setOnMins(String uuid, String mins) {
        MySQL.Update("UPDATE Users SET onmins='" + mins + "' WHERE uuid='" + uuid + "'");
    }

    public static void setName(String playerName, String uuid) {
        MySQL.Update("UPDATE Users SET name='" + playerName.toLowerCase() + "' WHERE uuid='" + uuid + "'");
    }

    public static void setIP(String playerName, String IP) {
        MySQL.Update("UPDATE Users SET ip='" + IP + "' WHERE name='" + playerName.toLowerCase() + "'");
    }


}
