package de.primeapi.bungeesystem.mysql;

import de.primeapi.bungeesystem.main.Conf;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class BanStats {


    public static boolean isBanned(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("banned");

            Result.close();
            State.close();

            return s.equalsIgnoreCase("1");
        } catch (SQLException e) {
            return false;
        }

    }

    public static boolean isIPBanned(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("ipban");

            Result.close();
            State.close();

            return s.equalsIgnoreCase("1");
        } catch (SQLException e) {
            return false;
        }

    }


    public static List<String> getBannedUUIDs() {
        List<String> list = new ArrayList<>();
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE banned=?;");
            State.setString(1, "1");

            ResultSet Result = State.executeQuery();


            while (Result.next()) {
                list.add(Result.getString("uuid"));
            }

            Result.close();
            State.close();


        } catch (SQLException e) {
        }

        return list;
    }

    public static String getAdminEntry(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("admin");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
            return "Error";
        }

    }

    public static String getAdminName(String uuid) {
        String entry = BanStats.getAdminEntry(uuid);
        if (PlayerStats.getName(entry).equalsIgnoreCase("ErrorExeption0043")) {
            return entry;
        } else {
            return PlayerStats.getName(entry);
        }
    }

    public static String getTime(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("lang");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
            return "Error";
        }

    }

    public static String getbanneds(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("banned");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
            return "Error";
        }

    }

    public static String getReason(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Users WHERE uuid=?;");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("reason");

            Result.close();
            State.close();

            return s;
        } catch (SQLException e) {
            return "Error";
        }

    }

    public static void setBanned(String uuid, Boolean banned) {
        String banneds = "0";
        if (banned) {
            banneds = "1";
        } else {
            banneds = "0";
        }
        MySQL.Update("UPDATE Users SET banned='" + banneds + "' WHERE uuid='" + uuid + "'");
    }

    public static void setIPBanned(String uuid, Boolean banned) {
        String banneds = "0";
        if (banned) {
            banneds = "1";
        } else {
            banneds = "0";
        }
        MySQL.Update("UPDATE Users SET ipban='" + banneds + "' WHERE uuid='" + uuid + "'");
    }

    public static void setReason(String uuid, String reason) {
        MySQL.Update("UPDATE Users SET reason='" + reason + "' WHERE uuid='" + uuid + "'");
    }

    public static void setAdmin(String uuid, String Adminname) {
        MySQL.Update("UPDATE Users SET admin='" + Adminname + "' WHERE uuid='" + uuid + "'");
    }

    public static void setTime(String uuid, Long time) {
        MySQL.Update("UPDATE Users SET lang='" + time + "' WHERE uuid='" + uuid + "'");
    }

    public static void addBan(String uuid, String reason, String Adminuuid, Long time) {
        setBanned(uuid, true);
        setReason(uuid, reason);
        setAdmin(uuid, Adminuuid);
        setTime(uuid, time);
        if(Conf.getSettingsBoolean("toggle.autpIPBan")) setIPBanned(uuid, true);
    }


}
