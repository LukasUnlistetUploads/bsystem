package de.primeapi.bungeesystem.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class AuthStats {

    public static Boolean exists(UUID uuid){
        boolean b = false;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM auth WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            b = rs.next();
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return b;
    }

    public static void insert(UUID uuid){
        if(exists(uuid)) return;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("INSERT INTO auth values (?,?,?)");
            st.setString(1, uuid.toString());
            st.setBoolean(2, false);
            st.setString(3, null);
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void setActive(UUID uuid, boolean b){
        if(!exists(uuid)) insert(uuid);
        try {
            PreparedStatement st = MySQL.c.prepareStatement("UPDATE auth SET active=? WHERE uuid=?");
            st.setBoolean(1, b);
            st.setString(2, uuid.toString());
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public static void setKey(UUID uuid, String secret){
        if(!exists(uuid)) insert(uuid);
        try {
            PreparedStatement st = MySQL.c.prepareStatement("UPDATE auth SET secret=? WHERE uuid=?");
            st.setString(1, secret);
            st.setString(2, uuid.toString());
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Boolean getActive(UUID uuid){
        if(!exists(uuid)) insert(uuid);
        boolean b = false;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM auth WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            if(rs.next()) b = rs.getBoolean("active");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return b;
    }

    public static String getKey(UUID uuid){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM auth WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            if(rs.next()) s = rs.getString("secret");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return s;
    }

}
