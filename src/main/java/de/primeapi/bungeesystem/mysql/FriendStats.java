package de.primeapi.bungeesystem.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class FriendStats {

    public static List<String> getFriends(String UUID1) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Friends WHERE uuid=? AND status=?");
            State.setString(1, UUID1);
            State.setString(2, "FREUND");

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(PlayerStats.getName(Result.getString("uuidfriend")));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> getFriendAnfragen(String UUID1) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Friends WHERE uuid=? AND status=?");
            State.setString(1, UUID1);
            State.setString(2, "ANFRAGE");

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                if (!Result.getString("uuidanfrage").equalsIgnoreCase(UUID1)) {
                    list.add(PlayerStats.getName(Result.getString("uuidanfrage")));
                }
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void createUserToggle(UUID uuid) {
        if(toggleUserExists(uuid)) return;
        try {
            PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO Toggle values(?, 0, 0, 0, 0)");
            State.setString(1, uuid.toString());

            State.execute();
            State.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean toggleUserExists(UUID uuid){

        Boolean b = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM Toggle WHERE uuid=?");
            st.setString(1, uuid.toString());

            ResultSet rs = st.executeQuery();
            b = rs.next();
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return b;
    }

    public static void addFriend(String UUID1, String UUID2, String AnfragerUUID, String Status) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO Friends values(?, ?, ?, ?)");
            State.setString(1, UUID1);
            State.setString(2, UUID2);
            State.setString(3, AnfragerUUID);
            State.setString(4, Status);

            State.execute();
            State.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setFreundStatus(String UUID1, String UUID2, String Status) {
        MySQL.Update(
                "UPDATE Friends SET status='" + Status + "' WHERE uuid='" + UUID1 + "' AND uuidfriend='" + UUID2 + "'");
    }

    public static void removeFriend(String UUID1, String UUID2) {
        MySQL.Update("DELETE FROM Friends WHERE uuid='" + UUID1 + "' AND uuidfriend='" + UUID2 + "'");
    }

    public static String getFriendStatus(String UUID1, String UUID2) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Friends WHERE uuid=? AND uuidfriend=?");
            State.setString(1, UUID1);
            State.setString(2, UUID2);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("status");

            Result.close();
            State.close();

            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "NON";

    }

    public static int getToggleStatus(String UUID, String Toggle) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Toggle WHERE uuid=?");
            State.setString(1, UUID);

            ResultSet Result = State.executeQuery();
            Result.next();

            int i = Result.getInt(Toggle);

            Result.close();
            State.close();

            return i;
        } catch (Exception e) {
        }
        return 0;

    }

    public static void setToggleStatus(String UUID, String Toggle, Integer ID) {
        MySQL.Update("UPDATE Toggle SET " + Toggle + "=" + ID + " WHERE uuid='" + UUID + "'");
    }

    public static String getFriendAnfrager(String UUID1, String UUID2) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM Friends WHERE uuid=? AND uuidfriend=?");
            State.setString(1, UUID1);
            State.setString(2, UUID2);

            ResultSet Result = State.executeQuery();
            Result.next();

            String s = Result.getString("uuidanfrage");

            Result.close();
            State.close();

            return s;
        } catch (Exception e) {
        }
        return "";

    }

}
