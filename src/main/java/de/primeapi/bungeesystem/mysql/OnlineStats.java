package de.primeapi.bungeesystem.mysql;

import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class OnlineStats {

    public static void insert(UUID uuid){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("INSERT INTO Online values(?,?,?,?,?)");
            st.setString(1, uuid.toString());
            st.setString(2, "");
            st.setInt(3, 0);
            st.setString(4, "null");
            st.setBoolean(5, false);
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void flush(){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("DELETE FROM Online");
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void remove(UUID uuid){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("DELETE FROM Online WHERE uuid=?");
            st.setString(1, uuid.toString());
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void updateServer(UUID uuid, String server){
        MySQL.Update("UPDATE Online SET server='" + server + "' WHERE uuid='" + uuid.toString() + "';");
    }

    public static void updateAFK(UUID uuid, Boolean b){
        int i = (b ? 1 : 0);
        MySQL.Update("UPDATE Online SET afk=" + i + " WHERE uuid='" + uuid.toString() + "';");
    }
    public static void updateFrozen(UUID uuid, Boolean b){
        int i = (b ? 1 : 0);
        MySQL.Update("UPDATE Online SET frozen=" + i + " WHERE uuid='" + uuid.toString() + "';");
    }
    public static boolean getAFK(UUID uuid){
        int i = 0;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM Online WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            rs.next();
            i = rs.getInt("afk");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return i == 1;
    }
    public static boolean getFrozen(UUID uuid){
        boolean b = false;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM Online WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            rs.next();
            b = rs.getBoolean("frozen");
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return b;
    }

}
