package de.primeapi.bungeesystem.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LogStats {


    public static void addLogReason(String uuid, String reason, String admin, String longs, String time, String type) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("INSERT INTO History values(?, ?, ?, ?, ?, ?)");
            State.setString(1, uuid);
            State.setString(2, reason);
            State.setString(3, admin);
            State.setString(4, longs);
            State.setString(5, time);
            State.setString(6, type);

            State.execute();
            State.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void deleteAllLogs(String UUID) {
        MySQL.Update("DELETE FROM History WHERE uuid='" + UUID + "'");
    }


    public static List<String> getBanReasons(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM History WHERE uuid=? and type=?");
            State.setString(1, uuid);
            State.setString(2, "Ban");

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("reason"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {

            List<String> listn = new ArrayList<String>();

            return listn;
        }
    }

    public static List<String> getMuteReasons(String uuid) {
        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM History WHERE uuid=? and type=?");
            State.setString(1, uuid);
            State.setString(2, "Mute");

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("reason"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {

            List<String> listn = new ArrayList<String>();

            return listn;
        }
    }

    public static List<String> getLogReasons(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM History WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("reason"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {

            List<String> listn = new ArrayList<String>();

            return listn;
        }
    }

    public static List<String> getLogAdmins(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM History WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("admin"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {
            List<String> listn = new ArrayList<String>();

            return listn;
        }

    }

    public static List<String> getLogLongs(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM History WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("longs"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {

            List<String> listn = new ArrayList<String>();

            return listn;
        }
    }

    public static List<String> getLogTimess(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM History WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("times"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {

            List<String> listn = new ArrayList<String>();

            return listn;
        }
    }

    public static List<String> getLogTypes(String uuid) {

        try {
            PreparedStatement State = MySQL.c.prepareStatement("SELECT * FROM History WHERE uuid=?");
            State.setString(1, uuid);

            ResultSet Result = State.executeQuery();

            List<String> list = new ArrayList<String>();

            while (Result.next()) {
                list.add(Result.getString("type"));
            }

            Result.close();
            State.close();

            return list;
        } catch (SQLException e) {

            List<String> listn = new ArrayList<String>();

            return listn;
        }
    }
}
