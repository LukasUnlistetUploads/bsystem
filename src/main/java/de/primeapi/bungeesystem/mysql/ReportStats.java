package de.primeapi.bungeesystem.mysql;

import de.primeapi.bungeesystem.managers.ReportManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReportStats {

    public static void insertReport(ReportManager.Report report){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("INSERT INTO Reports values(?,?,?,?)");
            st.setString(1, report.getReported());
            st.setString(2, report.getReason());
            st.setString(3, report.getReporter());
            st.setString(4, report.getTime().toString());

            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static boolean hasReported(String reporter, String reported){
        boolean b = false;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM Reports WHERE reported=? AND reporter=?");
            st.setString(1, reported);
            st.setString(2, reporter);
            ResultSet rs = st.executeQuery();
            b = rs.next();
            rs.close();
            st.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return b;
    }

    public static List<ReportManager.Report> getReports(){
        List<ReportManager.Report> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM Reports");
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(new ReportManager.Report(rs.getString("reporter"), rs.getString("reported"), rs.getString("reason"), rs.getString("timestamp")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public static List<ReportManager.Report> getReportsByReporter(String name){
        List<ReportManager.Report> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM Reports WHERE reporter=?");
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(new ReportManager.Report(rs.getString("reporter"), rs.getString("reported"), rs.getString("reason"), rs.getString("timestamp")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public static List<ReportManager.Report> getReportsByReported(String name){
        List<ReportManager.Report> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM Reports WHERE reported=?");
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(new ReportManager.Report(rs.getString("reporter"), rs.getString("reported"), rs.getString("reason"), rs.getString("timestamp")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public static void removeReport(String reported, String reporter){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("DELETE FROM Reports WHERE reported=? AND reporter=?");
            st.setString(1, reported);
            st.setString(2, reporter);
            st.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
