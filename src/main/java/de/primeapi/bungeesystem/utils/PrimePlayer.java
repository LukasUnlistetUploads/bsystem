package de.primeapi.bungeesystem.utils;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.BanStats;
import de.primeapi.bungeesystem.mysql.MuteStats;
import lombok.Getter;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

@Getter
public class PrimePlayer {
    String Name;
    String UniqeID;
    ProxiedPlayer pp;

    public PrimePlayer(ProxiedPlayer pp) {
        this.Name = pp.getName();
        this.UniqeID = pp.getUniqueId().toString();
        this.pp = pp;
    }

    public ProxiedPlayer getProxiedPlayer() {
        return this.pp;
    }


    public void sendNoPerm() {
        sendMessage(Message.NO_PERMS);
    }

    public ServerInfo getServerInfo() {
        return this.pp.getServer().getInfo();
    }

    public boolean hasPermission(String s) {
        return pp.hasPermission(s);
    }


    public String getName() {
        return Name;
    }

    public String getUniqeID() {
        return UniqeID;
    }

    public UUID getUUID(){
        return UUID.fromString(UniqeID);
    }

    public boolean isBanned() {
        return BanStats.isBanned(this.UniqeID);
    }

    public void setBanned(boolean banned) {
        BanStats.setBanned(this.UniqeID, banned);

    }

    public void connect(ServerInfo server) {
        if (!this.pp.getServer().getInfo().equals(server)) {
            this.pp.connect(server);
        }
    }


    public boolean isMuted() {
        return MuteStats.isMuted(this.UniqeID);
    }

    public void setMuted(boolean muted) {
        MuteStats.setMuted(this.UniqeID, muted);
    }

    public void sendMessage(Message message){
        this.pp.sendMessage(new TextComponent(message.getContent()));
    }

    public void sM(Message message){
        sendMessage(message);
    }
}
