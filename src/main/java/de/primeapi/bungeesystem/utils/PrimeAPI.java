package de.primeapi.bungeesystem.utils;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PrimeAPI {


    public static PrimePlayer getPrimePlayer(ProxiedPlayer pp) {
        return new PrimePlayer(pp);
    }

    public static PrimePlayer getPrimePlayer(CommandSender sender) {
        if (!(sender instanceof ProxiedPlayer)) {
            return null;
        }

        return new PrimePlayer((ProxiedPlayer) sender);
    }

    public static Boolean isOnline(String name) {
        return ProxyServer.getInstance().getPlayer(name) != null;
    }

    public static PrimePlayer getPrimePlayer(String name) {
        return new PrimePlayer(ProxyServer.getInstance().getPlayer(name));
    }

    public static PrimePlayer getPrimePlayer(UUID uuid) {
        return new PrimePlayer(ProxyServer.getInstance().getPlayer(uuid));
    }


    public static List<PrimePlayer> getOnlinePrimePlayers() {
        List<PrimePlayer> list = new ArrayList<>();

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            list.add(PrimeAPI.getPrimePlayer(all));
        }
        return list;
    }

    public static List<PrimePlayer> getOnlinePrimePlayers(ServerInfo info) {
        List<PrimePlayer> list = new ArrayList<>();

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            if (all.getServer().getInfo().equals(info)) {
                list.add(PrimeAPI.getPrimePlayer(all));
            }
        }
        return list;
    }


}
