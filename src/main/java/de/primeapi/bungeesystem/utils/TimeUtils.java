package de.primeapi.bungeesystem.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {

    public static Long toMillies(int days, int hours, int mins) {
        //   int day = days * 24 * 60 * 60 * 1000;
        //   int hour = hours * 60 * 60 * 1000;
        //   int min = mins * 60 * 1000;

        //   return (long) (day + hour + min);

        Long min = Long.valueOf(mins) * 60;
        Long hour = Long.valueOf(hours) * (60 * 60);
        Long day = Long.valueOf(days) * (60 * 60 * 24);
        Long time = min + hour + day;
        time = time * 1000;
        return time;

    }


    public static String MinutesToString(Long minutes) {
        String msg = "";

        long seconds = (minutes * 60);


        if (seconds >= 60 * 60 * 24) {
            long days = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            if (days <= 1) {
                msg += days + " Tag ";
            } else {
                msg += days + " Tage ";
            }

        }
        if (seconds >= 60 * 60) {
            long h = seconds / (60 * 60);
            seconds = seconds % (60 * 60);

            if (h <= 1) {
                msg += h + " Stunde ";
            } else {
                msg += h + " Stunden ";
            }

        }
        if (seconds >= 60) {
            long min = seconds / 60;
            seconds = seconds % 60;

            if (min <= 1) {
                msg += min + " Minute ";
            } else {
                msg += min + " Minuten ";
            }
        }

        return msg;
    }

    public static String getEnd(Long end) {

        if(end == -1){
            return "§cPERMANENT";
        }

        String msg = "";

        long now = System.currentTimeMillis();
        long diff = end - now;
        long seconds = (diff / 1000);

        if (seconds >= 60 * 60 * 24) {
            long days = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            if (days <= 1) {
                msg += days + " Tag ";
            } else {
                msg += days + " Tage ";
            }

        }
        if (seconds >= 60 * 60) {
            long h = seconds / (60 * 60);
            seconds = seconds % (60 * 60);

            if (h <= 1) {
                msg += h + " Stunde ";
            } else {
                msg += h + " Stunden ";
            }

        }
        if (seconds >= 60) {
            long min = seconds / 60;
            seconds = seconds % 60;

            if (min <= 1) {
                msg += min + " Minute ";
            } else {
                msg += min + " Minuten ";
            }
        }
        if (seconds > 0) {
            if (seconds <= 1) {
                msg += seconds + " Sekunde";
            } else {
                msg += seconds + " Sekunden";
            }
        }

        return msg;
    }

    public static String getEndd(Long end) {

        if (end == -1) {
            return "§4PERMANENT";
        }
        String msg = "";

        long now = System.currentTimeMillis();
        long diff = end - now;
        long seconds = (diff / 1000);

        if (seconds >= 60 * 60 * 24) {
            long days = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            if (days <= 1) {
                msg = days + " Tag ";
            } else {
                msg = days + " Tage ";
            }

            return msg;

        }
        if (seconds >= 60 * 60) {
            long h = seconds / (60 * 60);
            seconds = seconds % (60 * 60);

            if (h <= 1) {
                msg = h + " Stunde ";
            } else {
                msg = h + " Stunden ";
            }

            return msg;
        }
        if (seconds >= 60) {
            long min = seconds / 60;
            seconds = seconds % 60;

            if (min <= 1) {
                msg = min + " Minute ";
            } else {
                msg = min + " Minuten ";
            }

            return msg;
        }
        if (seconds > 0) {
            if (seconds <= 1) {
                msg = seconds + " Sekunde";
            } else {
                msg = seconds + " Sekunden";
            }
        }

        return msg;


    }

    public static String getDate(String miliseconds) {
        long ms;
        try {
            ms = Long.valueOf(miliseconds);
        } catch (Exception ex) {
            return "Fehler: 1";
        }
        try {
            Date date = new Date(ms);
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            return df.format(date);
        } catch (Exception ex) {
            return "Fehler: 2";
        }
    }

}
