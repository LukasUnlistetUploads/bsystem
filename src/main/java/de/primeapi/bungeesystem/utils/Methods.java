package de.primeapi.bungeesystem.utils;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;

public class Methods {

    public static void updateTabList() {

        if (!Conf.getSettingsBoolean("TabList.aktive")) return;


        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {

            String header = "";
            for (String s : Conf.getSettingsStringList("TabList.header")) {
                if (s.equals(Conf.getSettingsStringList("TabList.header").get(0))) {
                    header += s;
                } else {
                    header += "\n" + s;
                }
            }
            String fooder = "";

            for (String s : Conf.getSettingsStringList("TabList.footer")) {
                if (s.equals(Conf.getSettingsStringList("TabList.footer").get(0))) {
                    fooder += s;
                } else {
                    fooder += "\n" + s;
                }
            }
            try {
            header = header.replaceAll("%players%", "" + (ProxyServer.getInstance().getOnlineCount() + Data.fakeplayers));
            header = header.replaceAll("%max%", String.valueOf(Conf.getSettingsInteger("Slots")));
            header = header.replaceAll("%server%", all.getServer().getInfo().getName());

            }catch (Exception ex){
            }
            try {
            fooder = fooder.replaceAll("%players%", "" + (ProxyServer.getInstance().getOnlineCount() + Data.fakeplayers));
            fooder = fooder.replaceAll("%max%", String.valueOf(Conf.getSettingsInteger("Slots")));
            fooder = fooder.replaceAll("%server%", all.getServer().getInfo().getName());
            }catch (Exception ex){
            }

            all.setTabHeader(new TextComponent(header), new TextComponent(fooder));
        }
    }

    public static void createJoinMe(PrimePlayer p) {
        try {
            (new ImageMessage(ImageIO.read(new URL("https://minotar.net/avatar/" + p.getName() +
                    "/8")), 8, ImageChar.BLOCK.getChar())).appendText("", "", "", "§5" + p.getName() + " §7spielt jetzt auf §c" +
                    p.getServerInfo().getName() + "§7.", "§7Klicke um den §cServer §7zu betreten§7!").sendToPlayer(p.getPp(), p.getPp().getServer());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getPermissionPrefix(ProxiedPlayer p) {
        //Not working here
        return "§e";
    }
}
