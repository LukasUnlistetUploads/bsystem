package de.primeapi.bungeesystem.license;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Conf;
import lombok.Getter;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;

public class AdvancedLicense {

    private String PluginID = "BungeeSystem";

    private String licenseKey;
    private String validationServer = "http://primeapi.de/shop/licence/verify.php";
    private LogType logType = LogType.NORMAL;
    private String securityKey = "YecoF0I6M05thxLeokoHuW8iUhTdIUInjkfF";
    private boolean debug = false;
    private ValidationType cache;

    public AdvancedLicense(String licenseKey, String validationServer) {
        this.licenseKey = licenseKey;
        this.validationServer = validationServer;
    }

    private static String xor(String s1, String s2) {
        String s0 = "";
        for (int i = 0; i < (s1.length() < s2.length() ? s1.length() : s2.length()); i++)
            s0 += Byte.valueOf("" + s1.charAt(i)) ^ Byte.valueOf("" + s2.charAt(i));
        return s0;
    }

    public AdvancedLicense setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
        return this;
    }

    public AdvancedLicense setConsoleLog(LogType logType) {
        this.logType = logType;
        return this;
    }

    public AdvancedLicense debug() {
        debug = true;
        return this;
    }

    public boolean register() {
        ValidationType vt = isValid();
        if (vt.equals(ValidationType.VALID)) {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("LizenzKey ist gültig!");
            System.out.println("[]==========[License-System]==========[]");
            return true;
        } else {
            System.out.println("[]==========[License-System]==========[]");
            System.out.println("Verbinde zum Lizenz Server...");
            System.out.println("Fehler bei der Abfrage!");
            System.out.println("Melde dich auf dem Discord: discord.primeapi.de");
            System.out.println("Fehler: " + vt.toString());
            System.out.println("[]==========[License-System]==========[]");
            return false;
        }
    }


    //
    // Cryptographic
    //

    public boolean isValidSimple() {
        return (isValid() == ValidationType.VALID);
    }

    public String getMotd() {
        if (Conf.getSettingsBoolean("Wartung")) {
            String motd1 = Conf.getSettingsString("Motd.wartung.1");
            String motd2 = Conf.getSettingsString("Motd.wartung.2");
            return motd1 + " | " + motd2;
        } else {
            String motd1 = Conf.getSettingsString("Motd.normal.1");
            String motd2 = Conf.getSettingsString("Motd.normal.2");
            return motd1 + " | " + motd2;
        }
    }


    public ValidationType isValid() {

        if (Objects.nonNull(this.cache)) return this.cache;

        String rand = toBinary(UUID.randomUUID().toString());
        String sKey = toBinary(securityKey);
        String key = toBinary(licenseKey);
        String version = String.valueOf(BungeeSystem.getInstance().getDescription().getVersion());


        try {
            URL url = new URL(validationServer + "?v1=" + xor(rand, sKey) + "&v2=" + xor(rand, key) + "&pl=" + PluginID + "&version=" + version);
            if (debug) System.out.println("RequestURL -> " + url.toString());
            Scanner s = new Scanner(url.openStream());
            if (s.hasNext()) {
                String response = s.next();
                s.close();
                try {
                    return ValidationType.valueOf(response);
                } catch (IllegalArgumentException exc) {
                    String respRand = xor(xor(response, key), sKey);
                    if (rand.substring(0, respRand.length()).equals(respRand)) this.cache = ValidationType.VALID;
                    else this.cache = ValidationType.WRONG_RESPONSE;
                }
            } else {
                s.close();
                return ValidationType.PAGE_ERROR;
            }
        } catch (IOException exc) {
            if (debug) exc.printStackTrace();
            this.cache = ValidationType.URL_ERROR;
        }
        return this.cache;
    }

    //
    // Enums
    //

    private String toBinary(String s) {
        byte[] bytes = s.getBytes();
        StringBuilder binary = new StringBuilder();
        for (byte b : bytes) {
            int val = b;
            for (int i = 0; i < 8; i++) {
                binary.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }
        return binary.toString();
    }

    private void log(int type, String message) {
        if (logType == LogType.NONE || (logType == LogType.LOW && type == 0)) return;
        System.out.println(message);
    }

    //
    // Binary methods
    //

    public enum LogType {
        NORMAL, LOW, NONE
    }

    //
    // Console-Log
    //

    @Getter
    public enum ValidationType {

        KEY_NOT_FOUND(""),
        WRONG_RESPONSE("fa"),
        PAGE_ERROR("fa"),
        URL_ERROR("fa"),
        KEY_OUTDATED("fa"),
        NOT_VALID_IP("fa"),
        INVALID_PLUGIN("fa"),
        VALID("fa");

        final String errorMessage;

        ValidationType(String s) {
            this.errorMessage = s;
        }

    }
}
