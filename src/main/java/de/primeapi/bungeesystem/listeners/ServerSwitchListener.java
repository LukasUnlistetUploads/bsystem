package de.primeapi.bungeesystem.listeners;

import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.AuthManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.OnlineStats;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ServerSwitchListener implements Listener {




    @EventHandler
    public void onSwitch(ServerSwitchEvent e) {
        PrimePlayer p = PrimeAPI.getPrimePlayer(e.getPlayer());

        Methods.updateTabList();
        OnlineStats.updateServer(p.getPp().getUniqueId(), p.getServerInfo().getName());
        if (Data.follow2.containsKey(p.getPp())) {
            PrimePlayer f = PrimeAPI.getPrimePlayer(Data.follow2.get(p.getPp()));
            f.getPp().connect(p.getServerInfo());
        }
    }

}
