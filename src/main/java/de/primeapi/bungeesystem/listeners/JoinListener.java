package de.primeapi.bungeesystem.listeners;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.mysql.*;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.TimeUnit;

public class JoinListener implements Listener {

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onLogin(LoginEvent e) {
        String UUID = e.getConnection().getUniqueId().toString();

        if(!PlayerStats.isexist(UUID) && Conf.getSettingsBoolean("toggle.joinmessage")){
            int i = Conf.getSettingsInteger("saves.count") + 1;
            String msg = Conf.getSettingsString("messages.firstjoin")
                    .replaceAll("%player%", e.getConnection().getName())
                    .replaceAll("%spieler%", e.getConnection().getName())
                    .replaceAll("%count%", String.valueOf(i))
                    .replaceAll("%prefix%", Data.pr);
            Conf.setSettingsInt("saves.count", i);
            ProxyServer.getInstance().getScheduler().schedule(BungeeSystem.getInstance(),() -> {
                for(ProxiedPlayer pp : ProxyServer.getInstance().getPlayers()){
                    pp.sendMessage(new TextComponent(msg));
                }
            }, 1, TimeUnit.SECONDS);
        }

        MuteStats.addPlayer(UUID);
        NotifyStats.instertPlayer(UUID);
        PlayerStats.addPlayer(e.getConnection().getName(), UUID, e.getConnection().getAddress().getAddress().getHostAddress());
        PlayerStats.setName(e.getConnection().getName(), UUID);


        FriendStats.createUserToggle(java.util.UUID.fromString(UUID));


        if (!Data.License) {
            e.setCancelled(true);
            e.setCancelReason("§c§lLicense is not valid! See Log");
            return;
        }


        if (BanStats.isBanned(UUID)) {
            if (Long.valueOf(BanStats.getTime(UUID)) == -1) {
                e.setCancelled(true);
                e.setCancelReason(Conf.getSettingsString("messages.ban")
                .replaceAll("%reason%", BanStats.getReason(UUID))
                .replaceAll("%time%", "§4PERMANENT"));
                return;
            } else {
                if (Long.valueOf(BanStats.getTime(UUID)) <= System.currentTimeMillis()) {
                    e.setCancelled(false);
                    BanStats.setBanned(UUID, false);
                } else {
                    e.setCancelled(true);
                    e.setCancelReason(Conf.getSettingsString("messages.ban")
                            .replaceAll("%reason%", BanStats.getReason(UUID))
                            .replaceAll("%time%", TimeUtils.getEnd(Long.valueOf(BanStats.getTime(UUID)))));
                    return;
                }
            }
        }

        for (String ipname : PlayerStats.getAlts(PlayerStats.getIP(e.getConnection().getName()))) {
            String ipuuid = PlayerStats.getUUID(ipname);
            if (BanStats.isBanned(ipuuid) && BanStats.isIPBanned(ipuuid)) {
                if (Long.valueOf(BanStats.getTime(ipuuid)) == -1) {
                    e.setCancelled(true);
                    e.setCancelReason(Conf.getSettingsString("messages.ipban")
                            .replaceAll("%reason%", BanStats.getReason(UUID))
                            .replaceAll("%time%", "§4PERMANENT")
                            .replaceAll("%account%", PlayerStats.getName(ipuuid)));
                    return;
                } else {
                    if (Long.valueOf(BanStats.getTime(ipuuid)) <= System.currentTimeMillis()) {
                        e.setCancelled(false);
                        BanStats.setBanned(ipuuid, false);
                    } else {
                        e.setCancelled(true);
                        e.setCancelReason(Conf.getSettingsString("messages.ipban")
                                .replaceAll("%reason%", BanStats.getReason(UUID))
                                .replaceAll("%time%", TimeUtils.getEnd(Long.valueOf(BanStats.getTime(ipuuid))))
                                .replaceAll("%account%", PlayerStats.getName(ipuuid)));
                        return;
                    }
                }
            }
        }
    }
}
