package de.primeapi.bungeesystem.listeners;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.AuthManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.*;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import de.primeapi.bungeesystem.utils.TimeUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class ChatListener implements Listener {

    static HashMap<UUID, String> lastMessage = new HashMap<>();
    static ArrayList<UUID> slowChat = new ArrayList<>();
    static ArrayList<UUID> slowCommands = new ArrayList<>();


    @EventHandler
    public void onChat(ChatEvent e) {
        PrimePlayer p = new PrimePlayer((ProxiedPlayer) e.getSender());

        if(AuthManager.authPrision.contains(p.getUUID())){
            e.setCancelled(true);
            int i = 0;
            try {
                i = Integer.parseInt(e.getMessage());
            }catch (Exception ex){
                p.sendMessage(Message.AUTH_VERIFY_FAILURE);
                return;
            }
            if(AuthManager.checkKey(p, i)){
                p.sendMessage(Message.AUTH_VERIFY_SUCCESS);
                AuthManager.authPrision.remove(p.getUUID());

                PlayerStats.setIP(p.getPp().getPendingConnection().getName(), p.getPp().getPendingConnection().getAddress().getAddress().getHostAddress());
                PlayerStats.updateLastJoin(p.getPp().getPendingConnection().getName());
                OnlineStats.updateFrozen(p.getUUID(), false);
                if(!AuthStats.getActive(p.getUUID())) AuthStats.setActive(p.getUUID(), true);
            }else {
                p.sendMessage(Message.AUTH_VERIFY_FAILURE);
                return;
            }


            return;
        }

        String msg = e.getMessage();
        if (p.isMuted()) {
            if (Long.valueOf(MuteStats.getTime(p.getUniqeID())) > System.currentTimeMillis()) {
                if (!msg.startsWith("/") || msg.startsWith("/msg ") || msg.startsWith("/r ")) {
                    e.setCancelled(true);
                    p.sendMessage(Message.CHAT_MUTED.replace("time", TimeUtils.getEnd(Long.valueOf(MuteStats.getTime(p.getUniqeID())))).replace("reason", MuteStats.getReason(p.getUniqeID())));
                    return;
                }
            } else {
                p.setMuted(false);
            }
        }

        if(slowCommands.contains(p.getUUID()) && Conf.getSettingsBoolean("chatmanager.slowcommand.use") && !p.hasPermission("bungeesystem.slowcommand.ignore")){
            p.sendMessage(Message.CHAT_NOSPAMCOMMANDS);
            e.setCancelled(true);
            return;
        }
        if(slowChat.contains(p.getUUID()) && Conf.getSettingsBoolean("chatmanager.slowchat.use") && !p.hasPermission("bungeesystem.slowchat.ignore")){
            p.sendMessage(Message.CHAT_NOSPAM);
            e.setCancelled(true);
            return;
        }

        if(lastMessage.containsKey(p.getUUID()) && Conf.getSettingsBoolean("chatmanager.repeatmessage.use") && !p.hasPermission("bungeesystem.repeatmessage.ignore")){
            p.sendMessage(Message.CHAT_NOREPETATION);
            e.setCancelled(true);
            return;
        }

        if(msg.startsWith("/")){
            slowCommands.add(p.getUUID());
            BungeeSystem.getThreadPoolExecutor().submit(() -> {
                try {
                    Thread.sleep(Conf.getSettingsInteger("chatmanager.slowcommand.count") * 1000L);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                slowCommands.remove(p.getUUID());
            });
        }else {
            slowChat.add(p.getUUID());
            lastMessage.put(p.getUUID(), e.getMessage());
            BungeeSystem.getThreadPoolExecutor().submit(() -> {
                try {
                    Thread.sleep(Conf.getSettingsInteger("chatmanager.slowchat.count") * 1000L);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                slowChat.remove(p.getUUID());
            });
            BungeeSystem.getThreadPoolExecutor().submit(() -> {
                try {
                    Thread.sleep(Conf.getSettingsInteger("chatmanager.repeatmessage.count") * 1000L);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                lastMessage.remove(p.getUUID());
            });
        }
        // /hallo

        //	false				   false					  false

        if (Data.globalchatmute && !p.hasPermission("bungee.chatmute.bypass") && !msg.startsWith("/")) {
            p.sendMessage(Message.CHAT_DEACTIVATED);
            e.setCancelled(true);
        }

        if (Data.chatmutedServers.contains(p.getPp().getServer().getInfo()) && !p.hasPermission("bungee.chatmute.bypass") && !msg.startsWith("/")) {
            p.sendMessage(Message.CHAT_DEACTIVATED);
            e.setCancelled(true);
        }




            boolean bla = false;
            for (String bl : Conf.getBlacklist()) {
                if (!bla) {
                    if (msg.toLowerCase().contains(bl.toLowerCase())) {
                        e.setCancelled(true);
                        bla = true;

                        int time = Conf.getSettingsInteger("settings.blacklist.timeInDays");
                        String reason = Conf.getSettingsString("settings.blacklist.reason");
                        p.sendMessage(Message.AMUTE_SUCCESS_NOTIFYPLAYER.replace("%time%", time + " Tag(e)").replace("%reason%", reason));
                        MuteStats.addMute(p.getUniqeID(), reason, "ChatFilter-System", System.currentTimeMillis() + TimeUtils.toMillies(time, 0, 0));
                        LogStats.addLogReason(p.getUniqeID(), reason, "ChatFilter-System by PrimeAPI", time + " Tage", String.valueOf(System.currentTimeMillis()), "Mute");

                        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                            if (all.hasPermission("bungee.notify")) {
                                if (NotifyStats.isNotify(all.getUniqueId().toString())) {
                                    all.sendMessage(Data.pr + "§e " + p.getName() + "§c wurde von dem automatisierten ChatFilter-System gemutet!");
                                }
                            }
                        }


                    }
                }
            }
            boolean fla = false;
            for (String fl : Conf.getFilter()) {
                if (!fla && !bla) {
                    if (msg.toLowerCase().contains(fl.toLowerCase())) {
                        e.setCancelled(true);
                        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                            PrimePlayer al = new PrimePlayer(all);
                            if (al.hasPermission("bungee.team") && NotifyStats.isReport(al.getUniqeID())) {
                                al.sendMessage(Message.CHATFILTER_NOTIFY1.replace("player", p.getName()));
                                al.sendMessage(Message.CHATFILTER_NOTIFY2.replace("message", msg));
                            }
                        }
                    }
                }
            }
        }

}
