package de.primeapi.bungeesystem.listeners;

import de.primeapi.bungeesystem.BungeeSystem;
import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.AuthManager;
import de.primeapi.bungeesystem.managers.ReportManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.FriendStats;
import de.primeapi.bungeesystem.mysql.OnlineStats;
import de.primeapi.bungeesystem.mysql.PlayerStats;
import de.primeapi.bungeesystem.mysql.ReportStats;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.PrimeAPI;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.TimeUnit;

public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerDisconnectEvent e) {
        PrimePlayer p = PrimeAPI.getPrimePlayer(e.getPlayer());

        PlayerStats.updateLastJoin(p.getName());

        OnlineStats.remove(p.getPp().getUniqueId());

        if (Data.follow.containsKey(p.getPp())) {
            Data.follow2.remove(Data.follow.get(p.getPp()));
            Data.follow.remove(p.getPp());
        }

        AuthManager.authPrision.remove(p.getUUID());

        if (Data.follow2.containsKey(p.getPp())) {
            PrimePlayer f = PrimeAPI.getPrimePlayer(Data.follow2.get(p.getPp()));
            f.sendMessage(Message.FOLLOW_DEACTIVATE.replace("player", p.getName()));
            Data.follow.remove(f.getPp());
            Data.follow2.remove(p.getPp());
        }

        for (String uuid : FriendStats.getFriends(p.getUniqeID())) {
            try {
                ProxiedPlayer pp = ProxyServer.getInstance().getPlayer(uuid);
                pp.sendMessage(Message.FRIEND_NOTIFYS_OFFLINE.replace("player", p.getName()).getContent());
            } catch (Exception ex) {

            }
        }

        if(!Conf.getSettingsBoolean("report.keepReportsOnLogout")){
            for(ReportManager.Report report : ReportStats.getReportsByReported(p.getName())){
                ReportStats.removeReport(report.getReported(), report.getReporter());
            }
        }

        ProxyServer.getInstance().getScheduler().schedule(BungeeSystem.getInstance(), () -> {
            Methods.updateTabList();
        }, 1, TimeUnit.SECONDS);


    }
}