package de.primeapi.bungeesystem.listeners;

import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PingListener implements Listener {

    @EventHandler
    public void onPing(ProxyPingEvent e) {
        ServerPing ping = e.getResponse();

        if (Conf.getSettingsBoolean("Wartung")) {
            String motd1 = Conf.getSettingsString("Motd.wartung.1");
            String motd2 = Conf.getSettingsString("Motd.wartung.2");
            ping.setDescription(motd1 + "\n" + motd2);
            ping.setVersion(new ServerPing.Protocol("§cWartungsarbeiten", 2));
        } else {
            String motd1 = Conf.getSettingsString("Motd.normal.1");
            String motd2 = Conf.getSettingsString("Motd.normal.2");
            ping.setDescription(motd1 + "\n" + motd2);
        }
        ServerPing.Players pls = ping.getPlayers();
        ping.setPlayers(new ServerPing.Players(Conf.getSettingsInteger("Slots"), ProxyServer.getInstance().getOnlineCount() + Data.fakeplayers, pls.getSample()));
        e.setResponse(ping);

    }
}
