package de.primeapi.bungeesystem.listeners;


import de.primeapi.bungeesystem.main.Conf;
import de.primeapi.bungeesystem.main.Data;
import de.primeapi.bungeesystem.managers.AuthManager;
import de.primeapi.bungeesystem.managers.messages.Message;
import de.primeapi.bungeesystem.mysql.*;
import de.primeapi.bungeesystem.utils.Methods;
import de.primeapi.bungeesystem.utils.PrimePlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PostLoginListener implements Listener {

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        final PrimePlayer p = new PrimePlayer(e.getPlayer());


        
        
        OnlineStats.insert(p.getPp().getUniqueId());

        if(Conf.getSettingsBoolean("toggle.clearchatOnJoin")) {
            for (int i = 0; i < 100; i++) {
                p.getProxiedPlayer().sendMessage(" ");
            }
        }

        int max = Conf.getSettingsInteger("Slots");
        int on = ProxyServer.getInstance().getOnlineCount() + Data.fakeplayers;
        if (max <= on && !p.hasPermission("bungee.premiumjoin")) {
            p.getPp().disconnect(Conf.getSettingsString("messages.premiumjoin"));
            return;
        }

        if(Conf.getSettingsBoolean("toggle.titleOnJoin")) {
            Title title = ProxyServer.getInstance().createTitle();
            title.title(new TextComponent(Message.JOIN_TITLE1.getContent()));
            title.subTitle(new TextComponent(Message.JOIN_TITLE2.getContent()));
            title.send(p.getProxiedPlayer());
        }

        for (String uuid : FriendStats.getFriends(p.getUniqeID())) {
            try {
                ProxiedPlayer pp = ProxyServer.getInstance().getPlayer(uuid);
                pp.sendMessage(Message.FRIEND_NOTIFYS_ONLINE.replace("player", p.getName()).getContent());
            } catch (Exception ex) {

            }
        }


        if (Conf.getSettingsBoolean("Wartung") && !p.hasPermission("bungee.wartung.ignore")) {
            p.getPp().disconnect(Conf.getSettingsString("messages.wartung"));
        }

        if (WarnStats.getWarnReasons(p.getUniqeID()).size() > 0 && Conf.getSettingsBoolean("toggle.warnsOnJoin")) {
            p.sendMessage(Message.JOIN_WARNSCOUNT.replace("count", WarnStats.getWarnReasons(p.getUniqeID()).size()));
        }


        if (p.hasPermission("bungee.team") && Conf.getSettingsBoolean("toggle.loginStatusOnJoin")) {
            if (NotifyStats.isReport(p.getUniqeID())) {
                p.sendMessage(Message.LOGIN_REPORT_ON);
            } else {
                p.sendMessage(Message.LOGIN_REPORT_OFF);
            }
            if (NotifyStats.isTC(p.getUniqeID())) {
                p.sendMessage(Message.LOGIN_TEAMCHAT_ON);
            } else {
                p.sendMessage(Message.LOGIN_TEAMCHAT_OFF);
            }
            if (NotifyStats.isNotify(p.getUniqeID())) {
                p.sendMessage(Message.LOGIN_NOTIFY_ON);
            } else {
                p.sendMessage(Message.LOGIN_NOTIFY_OFF);
            }
        }



        if(Conf.getSettingsBoolean("settings.auth.use") && p.hasPermission(Conf.getSettingsString("settings.auth.permission"))){
            if(AuthStats.getActive(p.getUUID())){
                if(Conf.getSettingsBoolean("settings.auth.onlyOnIpChange")){
                    String oldip = PlayerStats.getIP(p.getName());
                    String newIP = p.getPp().getPendingConnection().getAddress().getAddress().getHostAddress();
                    if(!oldip.equalsIgnoreCase(newIP)){
                        AuthManager.lockPlayer(p);
                        return;
                    }
                }else {
                    AuthManager.lockPlayer(p);
                    return;
                }
            }else if(Conf.getSettingsBoolean("settings.auth.force")){
                AuthManager.activatePlayer(p);
                return;
            }
        }

        PlayerStats.setIP(e.getPlayer().getPendingConnection().getName(), e.getPlayer().getPendingConnection().getAddress().getAddress().getHostAddress());
        PlayerStats.updateLastJoin(e.getPlayer().getPendingConnection().getName());


    }

}
